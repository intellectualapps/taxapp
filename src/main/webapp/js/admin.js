const loggedinUser = JSON.parse(localStorage.getItem('loggedInUser'));
const userRole = loggedinUser.role;

let pageNumber = 1;
let pageSize = 10;

function next() {
   $('#tableLoader').addClass('d-flex').show();
   pageNumber = pageNumber+1;
   $('#searchButton').trigger('click');
};

function prev() {
   $('#tableLoader').addClass('d-flex').show();
   pageNumber = pageNumber-1;
   $('#searchButton').trigger('click');
};

$(function(){
    "use strict";

//     /* ----------------------------------------------------------- */
//       /*  Admin Dashboard
//      /* ----------------------------------------------------------- */

if (userRole === 'ADMIN') {
  $('#assignedToCol').hide();
}

$("#usersName").html("Welcome " + loggedinUser.firstName);

var adminUsers = '';
  $.ajax({
    type: 'GET',
    url: 'api/v1/lookup/users?role=admin',
    success: (result) => {
      for (var i in result) {
        adminUsers = result;
      }
    },
    error: () => {}
  });

   let getAllData = function(pageNumber, pageSize){
    $.ajax({
        type: 'GET',
        url: `api/v1/service/tax?page-size=${pageSize}&page-number=${pageNumber}`,
        headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
        success: (result)=>{

            let output="";
            let badge = "";
            let serialNumber  = pageSize * pageNumber-10;
            //if(serialNumber === 0){
                serialNumber = serialNumber+1;
            //}

            for (var i in result.appServiceRequestSummaries){
                let eachValue = result.appServiceRequestSummaries[i];

                if(eachValue.requestStatus === 'PENDING'){
                    badge = 'badge-danger' ;
                } else if(eachValue.requestStatus === 'IN PROCESS'){
                    badge = 'badge-warning' ;
                }else{
                    badge = 'badge-success' ;
                }

                let assignedToUsername = eachValue.assignedTo;

                let adminUsersDropDown = `<select class='form-control search-custom-control mx-2 select-dropdown' onchange='assignUser(this)'><option>Unassigned</option>`;

                for (var i in adminUsers) {
                  if (assignedToUsername === adminUsers[i].username) {
                    adminUsersDropDown += `<option value='${eachValue.id}-${adminUsers[i].username}' selected>${adminUsers[i].firstName} ${adminUsers[i].lastName}</option>`;
                  } else {
                    adminUsersDropDown += `<option value='${eachValue.id}-${adminUsers[i].username}'>${adminUsers[i].firstName} ${adminUsers[i].lastName}</option>`;                    
                  }
                }
                adminUsersDropDown += `</select>`;

              output+= `<tr id="${eachValue.id}">
                            <th scope="row">${serialNumber++}</th>
                            <td class="custom-table-rows">${eachValue.clientName}</td>
                            <td class="custom-table-rows">${eachValue.requestedServiceName}</td>
                            <td class="custom-table-rows">${eachValue.dateRequested}</td>
                            <td><span class="badge badge-pill ${badge}">${eachValue.requestStatus}</span></td>
                        `

            if (userRole === 'ADMIN') {
              output += `</tr>`
            } else {
              output+= `<td>${adminUsersDropDown}</td></tr>`
            }

            }

            $('#tableLoader').removeClass('d-flex').hide();
            $("#adminTable").html(output);
        },
          error: function (request, exception, errorThrown) {
            pageNumber = 1;
            pageSize = 10;
            $('#tableLoader').removeClass('d-flex').hide();
            $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
            
        },
      });

   }

   const fetchServices = () => {
    $.ajax({
      type: 'GET',
      url: `api/v1/lookup/services`,
      headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
      success: (services) => {
        for (let i = 0; i < services.length; i++) {
          let id = services[i].id;
          let description = services[i].description;
          $('#servicesInput').append(`<option value='${id}'>${description}</option>`);
        }
      },
      error: () => {
        $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
      }
    })
   }

  const fetchStatuses = () => {
    $.ajax({
      type: 'GET',
      url: `api/v1/lookup/service-statuses`,
      headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
      success: (statuses) => {
        for (let i = 0; i < statuses.length; i++) {
          let description = statuses[i].description;
          let id = statuses[i].id;
          $('#statusInput').append(`<option value='${id}'>${description}</option>`);
        }
      },
      error: () => {
        $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
      }
    })
   }

   $(document).on('click', '.custom-table-rows', function(){
        let id = $(this).parent().attr('id');
        window.location.href = `details?id=${id}`

   })

   getAllData(pageNumber, pageSize);
   fetchServices();
   fetchStatuses();

   $('#nextBtn').click(function(){
       $('#tableLoader').addClass('d-flex').show();
       pageNumber = pageNumber+1;
       getAllData(pageNumber, pageSize)
   });

   $('#prevBtn').click(function(){
       $('#tableLoader').addClass('d-flex').show();
       pageNumber = pageNumber-1;
       getAllData(pageNumber, pageSize)
   });

  $('#searchButton').click(
    function() {
      $("#adminTable").empty();
      $("#navigateBtn").empty();
      let newButton = "";
      newButton += `<div id="newPrevBtn" class="btn red-btn" onclick="prev()">Prev</div>
                        <div id="newNextBtn" class="btn red-btn" onclick="next()">Next</div>`
      $("#navigateBtn").append(newButton);

      let searchTerm = $('#searchInput').val();
      let serviceId = $('#servicesInput').val();
      let status = $('#statusInput').val();
      
      $.ajax({
        type: 'GET',
        url: `api/v1/service/search?search-term=${searchTerm}&service-id=${serviceId}&status=${status}&page-number=${pageNumber}&page-size=${pageSize}`,
        headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
        success: (result) => {
            let res = result.appServiceRequestSummaries;

            let output="";
            let badge = "";
            let serialNumber  = pageSize * pageNumber-10;
            //if(serialNumber === 0){
                serialNumber = serialNumber+1;
            //}

            for (var i in res){
                let eachValue = res[i];

                if(eachValue.requestStatus === 'PENDING'){
                    badge = 'badge-danger' ;
                } else if(eachValue.requestStatus === 'IN PROCESS'){
                    badge = 'badge-warning' ;
                }else{
                    badge = 'badge-success' ;
                }

                let assignedToUsername = eachValue.assignedTo;                
                
                let adminUsersDropDown = `<select class='form-control search-custom-control mx-2' onchange='assignUser(this)'><option>Unassigned</option>`;
                
                for (var i in adminUsers) {
                  if (assignedToUsername === adminUsers[i].username) {
                    adminUsersDropDown += `<option value='${eachValue.id}-${adminUsers[i].username}' selected>${adminUsers[i].firstName} ${adminUsers[i].lastName}</option>`;
                  } else {
                    adminUsersDropDown += `<option value='${eachValue.id}-${adminUsers[i].username}'>${adminUsers[i].firstName} ${adminUsers[i].lastName}</option>`;                    
                  }
                }
                adminUsersDropDown += `</select>`;

                output+= `<tr id="${eachValue.id}">
                            <th scope="row">${serialNumber++}</th>
                            <td class="custom-table-rows">${eachValue.clientName}</td>
                            <td class="custom-table-rows">${eachValue.requestedServiceName}</td>
                            <td class="custom-table-rows">${eachValue.dateRequested}</td>
                            <td><span class="badge badge-pill ${badge}">${eachValue.requestStatus}</span></td>
                        `
              if (userRole === 'ADMIN') {
                 output+= `</tr>`
              } else {
                 output+= `<td>${adminUsersDropDown}</td></tr>`
              }
            }

            $('#tableLoader').removeClass('d-flex').hide();
            $("#adminTable").html(output);

        },
        error: function (e) {
          console.log(e)
          $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
        },
      });

    });
});


function assignUser(option) {
    let selectedOptionValue = option.value;
    let optionId = selectedOptionValue.split("-");

    let id = optionId[0];
    let username = optionId[1];

    $.ajax({
      type: 'POST',
      url: `api/v1/admin/assign-task/${id}?username=${username}`,
      headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
      success: (response) => { console.log('post:', response) },
      error: (e) => { console.log(e) }
    })
}