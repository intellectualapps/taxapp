$(function(){
    "use strict";

//     /* ----------------------------------------------------------- */
//       /*  User Dashboard
//      /* ----------------------------------------------------------- */

$('#surveyOptions').hide();
let checkIfBusinessExists = function(){

       /* ----------------------------------------------------------- */
    /*  Check if Business Exists
   /* ----------------------------------------------------------- */

   $.ajax({
    type: 'GET',
    url: 'api/v1/business/exists',
    headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
    success: (result)=>{
        if(result.status === true){
            let loggedInUser = localStorage.getItem('loggedInUser');
            localStorage.setItem('user', loggedInUser)
            $('#dashboardOptions').hide();
            $('#surveyOptions').show();
        } else{
            $.toaster({ priority : 'danger', title : 'Error', message : "Sorry, you do not have any registered business, You will be redirected to register one now"});
            setTimeout(function() {
                window.location.href = 'registration-tax-service?fromDashboard=true'
            }, 4000);
        }
    }
  });
}

let checkIfBusinessExistsForIncorporationType = function(){

    /* ----------------------------------------------------------- */
 /*  Check if Business Exists For Incorporation Type
/* ----------------------------------------------------------- */

$.ajax({
 type: 'GET',
 url: 'api/v1/business/incorporation-request/exists',
 headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
 success: (result)=>{
     if(result.status === true){
         let loggedInUser = localStorage.getItem('loggedInUser');
         localStorage.setItem('user', loggedInUser)
         window.location.href = 'incorporation-type';
     } else{
         $.toaster({ priority : 'danger', title : 'Error', message : "Sorry, you do not have any registered business, You will be redirected to register one now"});
         setTimeout(function() {
             window.location.href = 'registration-bus-inc?fromDashboard=true'
         }, 4000);
     }
 }
});
}

var surveyAnswer = "";

   function undoUsedTaxMaster(){
    $('#usedTaxMaster div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
    $('#usedTaxMaster div img').prop("src", "images/hand shake.png").fadeIn(400);
    $('#corresponding-word').empty();
   }

   function undoUsedAnotherCompany(){
    $('#usedAnotherCompany div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
    $('#usedAnotherCompany div img').prop("src", "images/document-icon.png").fadeIn(400);
    $('#corresponding-word').empty();
   }

   function undoNeverFilledTax(){
    $('#neverFilledTax div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
    $('#neverFilledTax div img').prop("src", "images/new-tax-filing-request.png").fadeIn(400);
    $('#corresponding-word').empty();
   }

   $("#usedTaxMaster").click(function(){
    undoNeverFilledTax();
    undoUsedAnotherCompany();
    $('#usedTaxMaster div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
    $('#usedTaxMaster div img').prop("src", "images/check.png").fadeIn(400);
    $('#corresponding-word').append(`<p>Welcome back! Since we know all about you, your 
    tax processing will be easier than ever</p>`).fadeIn(800);
    surveyAnswer = "TAX_MASTER";
    localStorage.setItem('taxStatus', surveyAnswer);
   });

   $("#usedAnotherCompany").click(function(){
    undoUsedTaxMaster();
    undoNeverFilledTax();
    $('#usedAnotherCompany div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
    $('#usedAnotherCompany div img').prop("src", "images/check.png").fadeIn(400);
    $('#corresponding-word').append(`<p>Welcome to Tax Master! We have made it 
    easier than ever for you to process your returns</p>`).fadeIn(800);
    surveyAnswer = "OTHER"
    localStorage.setItem('taxStatus', surveyAnswer);
   });

   $("#neverFilledTax").click(function(){
    undoUsedTaxMaster();
    undoUsedAnotherCompany();
    $('#neverFilledTax div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
    $('#neverFilledTax div img').prop("src", "images/check.png").fadeIn(400);
    $('#corresponding-word').append(`<p>Don't worry - tax master will be 
    with you every step of the way for your ease and convenience</p>`).fadeIn(800);
    surveyAnswer = "NEVER"
    localStorage.setItem('taxStatus', surveyAnswer);
   });

   $('#surveyBack').click(function(){
       $('#dashboardOptions').show();
        $('#surveyOptions').hide();
   })

   $('#surveyNext').click(function(){
    window.location.href = 'tax-service';
    //window.location.href = 'cit-pit-revenue-range';
   })

$("#newTaxService").click(function(){
    checkIfBusinessExists();
  })

  $("#newIncorporationType").click(function(){
    checkIfBusinessExistsForIncorporationType();
  })
    
    
});