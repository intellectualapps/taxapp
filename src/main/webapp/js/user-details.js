$(function(){
    "use strict";

       /* ----------------------------------------------------------- */
    /*  Admin Details summary page
   /* ----------------------------------------------------------- */
    
    var url_string = window.location.href;
	var url = new URL(url_string);
    var id = url.searchParams.get("id");
    let resultId = '';
alert("service request id: " + id);

   $.ajax({
    type: 'GET',
    url: `api/v1/service/request-details?id=${id}`,
    headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
    success: (result)=>{
        console.log(result)
        resultId = result.id;
        let output = "";
        let base = `
        <div class="font-weight-bold">
            <p>Service Requested - <span>${result.requestedServiceName}</span></p>
            <p>Date - <span>${result.dateRequested}</span></p>
        </div>
        <div>
            <p>Company Name - <span>${result.clientName}</span></p>
        </div>
        <div >
            <p>Email Address - <span>${result.username}</span></p>
        </div>`

        var paymentSection = "";

        if (result.serviceCharge) {
            paymentSection = `<div>
                                <p class="text-center font-weight-bold">Payment</p>
                                <p>CIT - <span>${result.serviceCharge.citService}</span></p>
                                <p>Delivery - <span>${result.serviceCharge.deliveryService}</span></p>
                                <p>VAT - <span>${result.serviceCharge.vat}</span></p>
                                <p>VAT Service - <span>${result.serviceCharge.vatService}</span></p>
                                <p>Sub Total - <span>${result.serviceCharge.subTotal}</span></p>
                                <p>Total - <span>${result.serviceCharge.total}</span></p>
                            </div>`
        }

        if(result.incorporationRequest){
            output = `
                    <div >
                        <p>Company Address - <span>${result.incorporationRequest.companyAddress}</span></p>
                    </div>

                    <div >
                        <p>Company Industry - <span>${result.incorporationRequest.companyIndustryName}</span></p>
                    </div>

                    <div >
                        <p>Proposed Company Name One - <span>${result.incorporationRequest.proposedCompanyNameOne}</span></p>
                    </div>
                    <div >
                        <p>Proposed Company Name Two - <span>${result.incorporationRequest.proposedCompanyNameTwo}</span></p>
                    </div>
                    <div >
                        <p>Proposed Company Name Three - <span>${result.incorporationRequest.proposedCompanyNameThree}</span></p>
                    </div>
                    <div >
                        <p>Proposed Company Name Four - <span>${result.incorporationRequest.proposedCompanyNameFour}</span></p>
                    </div>
                `
        } else{
            output = `<div>
                        <p>Contact Name - <span>${result.entity.contactPersonFullName}</span></p>
                    </div>
                    <div >
                        <p>State - <span>${result.entity.stateName}</span></p>
                    </div>

                    <div >
                        <p>Address - <span>${result.entity.address}</span></p>
                    </div>

                    <div >
                        <p>Incorporation Type - <span>${result.entity.typeName}</span></p>
                    </div>

                    <div >
                        <p>Industry - <span>${result.entity.industryName}</span></p>
                    </div>

                    <div >
                        <p>Date of Incorporation - <span>${result.entity.incorporationDate}</span></p>
                    </div>

                    <div >
                        <p>Date Of Commencement - <span>${result.entity.commencementDate}</span></p>
                    </div>

                    <div >
                        <p>Revenue Range - <span>${result.entity.revenueRangeName}</span></p>
                    </div>
                    <div >
                        <p>TIN - <span>${result.entity.tin}</span></p>
                    </div>`
        }
        $('#statusText').text(result.requestStatus);
        $("#adminDetails").html(base + output + paymentSection);
    },
    error: function (request, exception, errorThrown) {
        $('#tableLoader').removeClass('d-flex').hide();
        $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
        
    },
});

$('#completeBadge').click(function(){
    let text = 'COMPLETED';
    $('#statusModal').modal('toggle');
    $.ajax({
        type: 'PUT',
        url: `api/v1/admin/service-status/${resultId}/${text}`,
        headers: {"Authorization": 'bearer ' + localStorage.getItem('authToken')},
        success: (result) => {
            if (result.status === true) {
                $.toaster({priority: 'success', title: 'Success', message: "Request status change successfull"});
                $('#statusText').text(text);
            } else {
                $.toaster({priority: 'danger', title: 'Error', message: "Sorry, something went wrong"});
            }
        }
    });
})

$('#inProcessBadge').click(function(){
    let text = 'IN_PROCESS';
    $('#statusModal').modal('toggle');
    $.ajax({
        type: 'PUT',
        url: `api/v1/admin/service-status/${resultId}/${text}`,
        headers: {"Authorization": 'bearer ' + localStorage.getItem('authToken')},
        success: (result) => {
            if (result.status === true) {
                $.toaster({priority: 'success', title: 'Success', message: "Request status change successfull"});
                $('#statusText').text(text);
            } else {
                $.toaster({priority: 'danger', title: 'Error', message: "Sorry, something went wrong"});
            }
        }
    });
})

$('#onHoldBadge').click(function(){
    let text = 'ON_HOLD';
    $('#statusModal').modal('toggle');
    $.ajax({
        type: 'PUT',
        url: `api/v1/admin/service-status/${resultId}/${text}`,
        headers: {"Authorization": 'bearer ' + localStorage.getItem('authToken')},
        success: (result) => {
            if (result.status === true) {
                $.toaster({priority: 'success', title: 'Success', message: "Request status change successfull"});
                $('#statusText').text(text);
            } else {
                $.toaster({priority: 'danger', title: 'Error', message: "Sorry, something went wrong"});
            }
        }
    });
})

});