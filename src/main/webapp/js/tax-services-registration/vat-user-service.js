$(function(){
    "use strict";

    $('#lastVatFiledDate2').datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true });
    
    $( document ).tooltip();

    var surveyAnswer = "";

    function undofiled(){
     $('#filed div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
     $('#filed div img').prop("src", "images/hand shake.png").fadeIn(400);
     
    }
 
    function undoneverFiled(){
     $('#neverFiled div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
     $('#neverFiled div img').prop("src", "images/document-icon.png").fadeIn(400);
     
    }
 
    function undoupToDate(){
     $('#upToDate div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
     $('#upToDate div img').prop("src", "images/piggy bank.png").fadeIn(400);
     
    }
 
    $("#filed").click(function(){
     undoupToDate();
     undoneverFiled();
     $('#filed div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
     $('#filed div img').prop("src", "images/check.png").fadeIn(400);
     surveyAnswer = 'OTHER';
     localStorage.setItem('vatStatus', surveyAnswer);
    });
 
    $("#neverFiled").click(function(){
     undofiled();
     undoupToDate();
     $('#neverFiled div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
     $('#neverFiled div img').prop("src", "images/check.png").fadeIn(400);
     surveyAnswer = 'NEVER';
     localStorage.setItem('vatStatus', surveyAnswer);
    });
 
    $("#upToDate").click(function(){
     undofiled()
     undoneverFiled();
     $('#upToDate div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
     $('#upToDate div img').prop("src", "images/check.png").fadeIn(400);
     surveyAnswer = 'NA';
     localStorage.setItem('vatStatus', surveyAnswer);
    });

    
    $('#lastVatFiledDate').hide();

   $('#lastVatFiledNext').click(function(){
        let lastVatFiledDate = $('#lastVatFiledDate2').val();
        localStorage.setItem('lastVatFiledDate', lastVatFiledDate);
        registerVatServiceOnServer();
    })

    $('#lastVatFiledBack').click(function(){
        $('#lastVatFiledDate').hide();
        $('#serviceSurvey').show();
    })

    $('#vatServiceNext').click(function(){
        $('#serviceSurvey').hide();
        showVatSurvey();
        
    })

    $('#vatServiceBack').click(function(){
        $('#serviceSurvey').hide();
        window.location.href = 'tax-service';
    })

    function showVatSurvey(){
        var vatStatus = localStorage.getItem('vatStatus');
        if (vatStatus === 'OTHER') {
            $('#lastVatFiledDate').show();
        } else if(vatStatus === 'NA'){
            window.location.href = 'payment-plan?serviceName=Value%20Added%20Tax(VAT)'
        }
        else {
            registerVatServiceOnServer();
        }
    }

   function registerVatServiceOnServer() {
    let taxStatus = localStorage.getItem('taxStatus');
    let vatStatus = localStorage.getItem('vatStatus');
    let taxServicePlan = localStorage.getItem('taxServicePlan');
    let lastVatFiledDate = localStorage.getItem('lastVatFiledDate');
    let business = JSON.parse(localStorage.getItem('business'));

    var parameters = "";
    if(vatStatus === "OTHER"){
        parameters = `entity-id=${business.id}&service-id=${taxServicePlan}&year=0&filed-before-status=${taxStatus}&vat-filed-before-status=${vatStatus}&last-vat-filed-date=${lastVatFiledDate}`
    } else{
        parameters = `entity-id=${business.id}&service-id=${taxServicePlan}&year=0&filed-before-status=${taxStatus}&vat-filed-before-status=${vatStatus}`
    }

    $.ajax({
        type: 'POST',
        url: "api/v1/service/tax?" + parameters,
        headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
        success: (result)=>{    
            window.location.href = `payment-summary?id=${result.id}&cit-service=${result.serviceCharge.citService}&vat-service=${result.serviceCharge.vatService}&delivery-service=${result.serviceCharge.deliveryService}&sub-total=${result.serviceCharge.subTotal}&vat=${result.serviceCharge.vat}&total=${result.serviceCharge.total}&serviceName=${result.serviceName}`
  
        },
        error: function (request, exception, errorThrown) {
            $.toaster({ priority : 'danger', title : 'Error', message: 'Please try again'});
      },
    })
}

})