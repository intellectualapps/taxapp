$(function(){
    "use strict";

       /* ----------------------------------------------------------- */
    /*  Payment summary page
   /* ----------------------------------------------------------- */
    
    $("#spin-page").show();
    var url_string = window.location.href;
	var url = new URL(url_string);
    
    let presentBusiness;
    var amount = url.searchParams.get("amount");
    var frequency = url.searchParams.get("frequency");
    var serviceName = url.searchParams.get("serviceName");
    var incType = url.searchParams.get("incType");
    var business = JSON.parse(localStorage.getItem('business'));
    var proposedBusiness = JSON.parse(localStorage.getItem('proposedBusiness'));

    if(incType){
        presentBusiness = proposedBusiness.busName1;
    } else{
        presentBusiness = business.name;
    }

    let amountName=`<p class="summary-amount"><img src="images/naira.png" alt="Naira" height="45" />101,325.00</p>`;
    let frequencyName=`<p class="my-3">Company Name: Sample Company Ltd.</p> 
                        <p class="my-3">Service: CIT</p>
                        <p class="my-3">CIT: 95,000</p>
                        <p class="my-3">Delivery Charge: 1,500</p>
                        <p class="my-3">Sub-total: 96,500.00</p>
                        <p class="my-3">VAT (5%): 4,825.00</p>
                        <p class="my-3">Total: 101,325.00</p>`;

    $("#plan-list").append(frequencyName);
    $("#money-list").prepend(amountName);
    
    
    $('#firspay').click(function(){        
        $.ajax({
            type: 'PUT',
            url: 'api/v1/admin/payment-confirmation',
            headers: {"Authorization": 'bearer ' + localStorage.getItem('authToken')},
            success: (result) => {
                if (result.status === true) {
                    alert("Payment complete");
                } else {
                    $.toaster({priority: 'danger', title: 'Error', message: "Sorry, something went wrong"});
                }
            }
        });
    })
   
});