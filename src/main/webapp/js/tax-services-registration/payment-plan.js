$(function(){
    "use strict";

    var url_string = window.location.href;
	var url = new URL(url_string);
    let taxServicePlan = localStorage.getItem('taxServicePlan');
    var serviceName = url.searchParams.get("serviceName");

    var businessForm = JSON.parse(localStorage.getItem('business'));

    $('#back-to-tax').click(function(){
        window.location.href = 'tax-service';
    });

    let inputedYear = "";

    if(taxServicePlan!=="cit"){
        $("#years").hide()
    }

     /* ----------------------------------------------------------- */
    /*  Get popup for Number of years
   /* ----------------------------------------------------------- */

   $.ajax({
    type: 'GET',
    url: 'api/v1/lookup/company-income-tax-periods',
    success: (result)=>{
        let output="";

        for (var i in result){
          output+= `<div class="col-md-4 col-6 p-2 year-value" name="${result[i].description}" id="${result[i].id}">
                        <div class="custom-modal-box d-flex justify-content-center align-items-center p-2">
                            <p class="m-0">${result[i].description}</p>
                        </div>
                    </div>`
        }

        $("#yearsList").html(output);

        $( ".year-value" ).click(function() {

            inputedYear = $(this).attr('id');

          $.ajax({
            type: 'GET', 
            url: `api/v1/payment-plan/cit?business-id=${businessForm.id}&year=${inputedYear}`,
            headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
            success: (result)=>{
                
                let output = `<div class="col-md-3 col-6 my-3 rev-range-box">
                          <div class="masthead p-1 top-rev-range-box d-flex justify-content-center align-items-center flex-column">
                              <p class="m-0 p-0 text-center font-weight-bold">${result[0].frequency} Years</p>
                              <hr class="rev-range-hr m-0">
                          </div>
                          <div class="bottom-rev-range-box p-1 d-flex justify-content-center align-items-center flex-column">
                              <p class="font-weight-bold text-center d-flex align-items-center"><img src="images/naira.png" alt="Naira" height="14" />${result[0].amount}</p>
                              <a id="${result[0].serviceId}+${result[0].frequency}" name="${result[0].description}+${result[0].amount}" class="btn red-btn rev-range-button proceedButton">Proceed to payment</a>
                          </div>
                      </div>`
                

                $('#yearsModal').modal('toggle');
                $("#pay-list").html(output);
            }
          });
        });
    }
  });

    

       /* ----------------------------------------------------------- */
    /*  Payment Plan service
   /* ----------------------------------------------------------- */
    
   
    
    $.ajax({
        type: 'GET',
        url: 'api/v1/payment-plan/' + taxServicePlan,
        headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
        success: (paymentPlans)=>{

            let output="";

            for (var i in paymentPlans){
              output+= `<div class="col-md-3 col-6 my-3 rev-range-box">
                      <div class="masthead p-1 top-rev-range-box d-flex justify-content-center align-items-center flex-column">
                          <p class="m-0 p-0 text-center font-weight-bold">${paymentPlans[i].frequency}</p>
                          <hr class="rev-range-hr m-0">
                      </div>
                      <div class="bottom-rev-range-box p-1 d-flex justify-content-center align-items-center flex-column">
                          <p class="font-weight-bold text-center d-flex align-items-center"><img src="images/naira.png" alt="Naira" height="14" />${paymentPlans[i].amount}</p>
                          <a id="${paymentPlans[i].id}" class="btn red-btn rev-range-button proceedButton">Proceed to payment</a>
                      </div>
                  </div>`
            }
    
            $("#pay-list").html(output);
            $("#serviceType").text(serviceName);
        }
      });

      $(document).on("click", ".proceedButton",function(){

        let taxStatus = localStorage.getItem('taxStatus');
        let vatStatus = localStorage.getItem('vatStatus');
        let business = JSON.parse(localStorage.getItem('business'));
        let paymentPlanId = $(this).attr('id');

        var parameters = `entity-id=${business.id}&service-id=${taxServicePlan}&payment-plan-id=${paymentPlanId}&vat-filed-before-status=${vatStatus}`

        $.ajax({
            type: 'POST',
            url: "api/v1/service/tax?" + parameters,
            headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
            success: (serviceSummary)=>{
                window.location.href = `payment-summary?id=${serviceSummary.id}&cit-service=${serviceSummary.serviceCharge.citService}&vat-service=${serviceSummary.serviceCharge.vatService}&delivery-service=${serviceSummary.serviceCharge.deliveryService}&sub-total=${serviceSummary.serviceCharge.subTotal}&vat=${serviceSummary.serviceCharge.vat}&total=${serviceSummary.serviceCharge.total}&serviceName=${serviceSummary.serviceName}`
      
            },
            error: function (request, exception, errorThrown) {
                $.toaster({ priority : 'danger', title : 'Error', message: 'Error. PLease try again'});
          },
        })
      });
      
});