$(function(){
    "use strict";

    $("#check").hide();
    $("#check1").hide();
    $("#check2").hide();
    $("#check3").hide();
    $("#check4").hide();
    $("#check5").hide();
    $("#check6").hide();
    $("#check7").hide();
    $("#check8").hide();
    $("#check9").hide();        

    // var widgetUrl;
    // var widget1Url;
    // var widget2Url;
    // var widget3Url;

    var widget = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },
     (error, result) => {
         if(result.event == "success"){
             alert("cert upload done " + result.info.secure_url);
             console.log(JSON.stringify(result));
            $("#certificateOfIncorporation").remove();
            $("#check").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);
            saveUrl(result.info.secure_url);
         }
    });

    var widget1 = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },
          (error, result) => {          
              if(result.event == "success"){             
                  $("#memorandumOfAssociation").hide();
              $("#check1").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);    
              saveUrl(result.info.secure_url);
            }
     });

    var widget2 = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },
          (error, result) => {          
              if(result.event == "success"){
                $("#articleOfAssociation").hide();
                $("#check2").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);   
                saveUrl(result.info.secure_url);
            }     
        });

    var widget3 = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },      
        (error, result) => {          
            if(result.event == "success"){             
                $("#tinSlip").hide();
                $("#check3").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);          
                saveUrl(result.info.secure_url);
            }     
        });

    var widget4 = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },      
        (error, result) => {          
            if(result.event == "success"){             
                $("#latestTCC").hide();
                $("#check4").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);          
                saveUrl(result.info.secure_url);
            }     
        });

    var widget5 = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },      
        (error, result) => {          
            if(result.event == "success"){             
                $("#companyLetterHead").hide();
                $("#check5").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);          
                saveUrl(result.info.secure_url);
            }     
        });
    var widget6 = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },      
        (error, result) => {          
            if(result.event == "success"){             
                $("#companyUtilityBill").hide();
                $("#check6").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);          
                saveUrl(result.info.secure_url);
            }     
        });

    var widget7 = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },      
        (error, result) => {          
            if(result.event == "success"){
                $("#previousYearStatement").hide();
                $("#check7").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);     
                saveUrl(result.info.secure_url);
            }     
        });
    var widget8 = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },      
        (error, result) => {
            if(result.event == "success"){             
                $("#bankStatement").hide();
                $("#check8").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);
                saveUrl(result.info.secure_url);
            }     
        });
    var widget9 = cloudinary.createUploadWidget({ 
    cloudName: "di9ukqwzp", uploadPreset: "ezmy4hob", multiple: false, sources: ["local"] },      
        (error, result) => {
            if(result.event == "success"){             
                $("#otherDocuments").hide();
                $("#check9").show().append(`<p style="font-size:10px;">${result.info.original_filename}</p>`);  
                saveUrl(result.info.secure_url);
            }     
        });                                            

    
    $("#certificateOfIncorporation").click(function(){
        widget.open();
    });

    $("#memorandumOfAssociation").click(function(){
        widget1.open();
    });

    $("#articleOfAssociation").click(function(){
        widget2.open();
    });

    $("#tinSlip").click(function(){
        widget3.open();
    });

    $("#latestTCC").click(function(){
        widget4.open();
    });    

    $("#companyLetterHead").click(function(){
        widget5.open();
    });

    $("#companyUtilityBill").click(function(){
        widget6.open();
    });

    $("#previousYearStatement").click(function(){
        widget7.open();
    });

    $("#bankStatement").click(function(){
        widget8.open();
    });

    $("#otherDocuments").click(function(){
        widget9.open();
    });    
           

    let submitForm = submitForm.concat([
        {name: "bankStatement", value: widgetUrl},
        {name: "tinDoc", value: widget1Url},
        {name: "taxCert", value: widget2Url},
        {name: "otherDocuments", value: widget3Url}
    ]);

    console.log(submitForm);
          
        //   $.ajax({
        //       type: 'POST',
        //     //   url: "api/v1/user?" + $(form).serialize(),
        //       success: (result)=>{
        
        //       },
        //       error: function (request, exception, errorThrown) {
        //         $("form[name='documentUploadFile'] > span").remove();
        //         $("form[name='documentUploadFile").prepend('<span class=\'error\'><p>'+ request.responseJSON.message +'</p></span>')
        //     },
        //   })
        
        function saveUrl(url) {            
            var id = localStorage.getItem('serviceIdFromPaySummary');
            if (id === null) {
                id = localStorage.getItem('serviceIdFromServiceDetails');
            }
            alert(id + " " + url);
            
            var parameters = `id=${id}&url=${url}`;
            
            $.ajax({
            type: 'POST',
            url: "api/v1/service/tax/document-url?" + parameters,
            headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
            success: (result)=>{
                window.location.href = `payment-summary?id=${result.id}&cit-service=${result.serviceCharge.citService}&vat-service=${result.serviceCharge.vatService}&delivery-service=${result.serviceCharge.deliveryService}&sub-total=${result.serviceCharge.subTotal}&vat=${result.serviceCharge.vat}&total=${result.serviceCharge.total}&serviceName=${result.serviceName}`
      
            },
            error: function (request, exception, errorThrown) {
                $.toaster({ priority : 'danger', title : 'Error', message: 'PLease try again'});
          },
        })
        }
});

