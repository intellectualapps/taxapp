$(function(){
    "use strict";

       /* ----------------------------------------------------------- */
    /*  Payment summary page
   /* ----------------------------------------------------------- */
    
    $("#spin-page").show();
    var url_string = window.location.href;
	var url = new URL(url_string);
    let presentBusiness;
    var amount = url.searchParams.get("amount");
    var frequency = url.searchParams.get("frequency");
    var serviceName = url.searchParams.get("serviceName");
    var incType = url.searchParams.get("incType");
    var plan = url.searchParams.get("plan");
    var business = JSON.parse(localStorage.getItem('business'));
    var proposedBusiness = JSON.parse(localStorage.getItem('proposedBusiness'));
    
    var id = url.searchParams.get("id");
    localStorage.setItem('serviceIdFromPaySummary', id);
    var serviceName = url.searchParams.get("serviceName");
    var citService = url.searchParams.get("cit-service");
    var vatService = url.searchParams.get("vat-service");
    var deliveryService = url.searchParams.get("delivery-service");
    var subTotal = url.searchParams.get("sub-total");
    var vat = url.searchParams.get("vat");
    var total = url.searchParams.get("total");
    var discountAmount = url.searchParams.get("discount-amount");
    
    if(incType){
        presentBusiness = proposedBusiness.proposedCompanyNameOne;
    } else{
        presentBusiness = business.name;
    }
    let year = "";
    if(plan === "cit"){
        year = "year(s)"
    }
        
    let amountName=`<p class="summary-amount"><img src="images/naira.png" alt="Naira" height="45" />${amount}</p>`;
    let frequencyName=`<p class="my-1">Company Name: ${presentBusiness}</p> 
                        <p class="my-1">Service: ${serviceName}</p>
                        <p class="my-1">Payment plan: ${frequency} ${year}</p>`;

    $("#plan-list").append(frequencyName);
    $("#money-list").prepend(amountName);

    let text = '';
    if (serviceName === "Company Income Tax") {
        text = 'CIT Service';
    } else if (serviceName === "Personal Income Tax") {
        text = 'PIT Service';
    } else if (serviceName === "Personal Income Tax (PAYE)") {
        text = 'PAYE Service';
    }
    
    let companyAndServiceDetails = `<div class="summary-details">
                            <p class="my-1">Company Name:</p>
                            <p class="my-1">${business.name}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">Service:</p>
                            <p class="my-1">${serviceName}</p> 
                        </div>`;
    
    let serviceSummary = `<div class="summary-details">
                            <p class="my-1">${text}:</p>
                            <p class="my-1">N${citService}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">VAT Service:</p>
                            <p class="my-1">N${vatService}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">Sub-Total:</p>
                            <p class="my-1">N${subTotal}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">VAT 5%:</p>
                            <p class="my-1">N${vat}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">Delivery Service:</p>
                            <p class="my-1">N${deliveryService}</p> 
                        </div>`;

    let payeSummary = `<div class="summary-details">
                            <p class="my-1">${text}:</p>
                            <p class="my-1">N${citService}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">Sub-Total:</p>
                            <p class="my-1">N${subTotal}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">VAT 5%:</p>
                            <p class="my-1">N${vat}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">Delivery Service:</p>
                            <p class="my-1">N${deliveryService}</p> 
                        </div>`;                        

    let vatServiceSummary = `<div class="summary-details">
                            <p class="my-1">VAT Service:</p>
                            <p class="my-1">N${vatService}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">Sub-Total:</p>
                            <p class="my-1">N${subTotal}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">VAT 5%:</p>
                            <p class="my-1">N${vat}</p> 
                        </div>
                        <div class="summary-details">
                            <p class="my-1">Delivery Service:</p>
                            <p class="my-1">N${deliveryService}</p> 
                        </div>`;
        
         let serviceTotal = "";
         let preDiscountTotal = "";
         let discountAmountHtml = "";
         
        if (discountAmount === 0 || discountAmount === "undefined" || discountAmount === null) {
    serviceTotal = `<div class="summary-amount row">
                            <p class="my-1 col-md-6 col-12 text-center">Total Amount:</p>
                            <p class="my-1 col-md-6 col-12 text-center">N${total}</p> 
                        </div>`;
    } else {
        var totalLocal = url.searchParams.get("total");
        var discountAmountLocal = url.searchParams.get("discount-amount");
        var preDiscountTotalAmount = url.searchParams.get("pre-discount-total");
        preDiscountTotal = `<div class="summary-details row">
                            <p class="my-1">Pre-discount Total:</p>
                            <p class="my-1"><b>N${preDiscountTotalAmount}</b></p> 
                        </div>`;
        discountAmountHtml = `<div class="summary-details row">
                            <p class="my-1">Discount:</p>
                            <p class="my-1">N${discountAmountLocal}</p> 
                        </div>`;
        serviceTotal = `<div class="summary-amount row">
                            <p class="my-1 col-md-6 col-12 text-center">Total Amount:</p>
                            <p class="my-1 col-md-6 col-12 text-center">N${total}</p> 
                        </div>`;
    }
    
    $("#company-service-details").append(companyAndServiceDetails);

    if (vatService === 'undefined') {
        $('#service-summary').append(payeSummary);        
    } else {
        if (citService === 'undefined') {
            $('#service-summary').append(vatServiceSummary);
        } else {
            $("#service-summary").append(serviceSummary);        
        }        
    }

    $("#pre-discount-total").append(preDiscountTotal);
    $("#discount-amount").append(discountAmountHtml);
    $("#service-total").append(serviceTotal);
    
    $("#applyDiscountBtn").click(function() {
        //alert("clicked");
        //var parameters = localStorage.getItem('taxServiceRequestParams');
        //alert("p; " + parameters);
        var discountCode = $("#discountInput").val();
        var discountUrl = "api/v1/service/tax/" + id + "/discount?discount-code=" + discountCode;        
        $.ajax({
            type: 'PUT',
            url: discountUrl,
            headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
            success: (result)=>{                
                window.location.href = `payment-summary?id=${result.id}&cit-service=${result.serviceCharge.citService}&vat-service=${result.serviceCharge.vatService}&delivery-service=${result.serviceCharge.deliveryService}&sub-total=${result.serviceCharge.subTotal}&vat=${result.serviceCharge.vat}&total=${result.serviceCharge.total}&serviceName=${result.serviceName}&discount-amount=${result.serviceCharge.discountAmount}&pre-discount-total=${result.serviceCharge.preDiscountTotal}`
      
            },
            error: function (request, exception, errorThrown) {
                //alert("Something went wrong, please try again");
                //$.toaster({ priority : 'danger', title : 'Error', message: 'PLease try again'});
          },
        })
    });
   
});