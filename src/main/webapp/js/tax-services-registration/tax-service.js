$(function(){
    "use strict"; 
      

    $('#lastVatFiledDate2').datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true });
    
    $( document ).tooltip();

   $('#vatYearPage').hide();
   $('#certificateExpiry').hide();
   $('#serviceSurvey').hide();
   $('#revenueRange').hide();
   $('#lastVatFiledDate').hide();

    $('#citTab').click(function(){
        localStorage.setItem('taxServicePlan', 'cit');
        loadAndShowRevenueRange();
        $('#certificateExpiry').hide();
        $('#taxServicesContainer').hide();
        $('#vatButtons').hide();
        $('#surveyButtons').show();
    })

    $('#pitTab').click(function(){
        localStorage.setItem('taxServicePlan', 'pit');
        loadAndShowRevenueRange();
        $('#certificateExpiry').hide();
        $('#taxServicesContainer').hide();
        $('#vatButtons').hide();
        $('#surveyButtons').show();
    });

    $('#payeTab').click(function() {
      localStorage.setItem('taxServicePlan', 'paye');
      var serviceName = 'Personal Income Tax(PAYE)';
      window.location.href='payment-plan?serviceName=' + serviceName;
    });

    $('#vatTab').click(function(){
        localStorage.setItem('taxServicePlan', 'vat');
        window.location.href='vat';
    });

    $('#clearanceCertificateNextBtn').click(function(){    
        let tccExpiryYear = $('#taxClearanceExpiryYear').val();
        localStorage.setItem('tccExpiryYear', tccExpiryYear);
        $('#serviceSurvey').show();
        $('#certificateExpiry').hide();
    })

    $('#vatServiceNext').click(function(){
        $('#serviceSurvey').hide();
        $('#certificateExpiry').hide();
        $('#taxServicesContainer').hide();
    })

    $('#vatServiceBack').click(function(){
        $('#certificateExpiry').show();
        $('#taxServicesContainer').hide();
    })

    var surveyAnswer = "";

    function undofiled(){
     $('#filed div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
     $('#filed div img').prop("src", "images/hand shake.png").fadeIn(400);
     
    }
 
    function undoneverFiled(){
     $('#neverFiled div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
     $('#neverFiled div img').prop("src", "images/document-icon.png").fadeIn(400);
     
    }
 
    function undoupToDate(){
     $('#upToDate div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
     $('#upToDate div img').prop("src", "images/piggy bank.png").fadeIn(400);
     
    }
 
    $("#filed").click(function(){
     undoupToDate();
     undoneverFiled();
     $('#filed div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
     $('#filed div img').prop("src", "images/check.png").fadeIn(400);
     surveyAnswer = 'OTHER';
     localStorage.setItem('vatStatus', surveyAnswer);
    });
 
    $("#neverFiled").click(function(){
     undofiled();
     undoupToDate();
     $('#neverFiled div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
     $('#neverFiled div img').prop("src", "images/check.png").fadeIn(400);
     surveyAnswer = 'NEVER';
     localStorage.setItem('vatStatus', surveyAnswer);
    });
 
    $("#upToDate").click(function(){
     undofiled()
     undoneverFiled();
     $('#upToDate div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
     $('#upToDate div img').prop("src", "images/check.png").fadeIn(400);
     surveyAnswer = 'NA';
     localStorage.setItem('vatStatus', surveyAnswer);
    });

    $('#surveyNext').click(function(){
        $('#serviceSurvey').hide();
        showVatSurvey();
        
    })

    $('#surveyBack').click(function(){
        $('#serviceSurvey').hide();
        $('#certificateExpiry').show();
    })
    
    $('#lastVatFiledNext').click(function(){
        let lastVatFiledDate = $('#lastVatFiledDate2').val();
        localStorage.setItem('lastVatFiledDate', lastVatFiledDate);
        registerTaxServiceOnServer();
    })

    $('#lastVatFiledBack').click(function(){
        $('#lastVatFiledDate').hide();
        $('#serviceSurvey').show();
    })
    
    function registerTaxServiceOnServer() {
        let taxStatus = localStorage.getItem('taxStatus');
        let vatStatus = localStorage.getItem('vatStatus');
        let tccExpiryYear = localStorage.getItem('tccExpiryYear');
        let taxServicePlan = localStorage.getItem('taxServicePlan');
        let taxServiceRevenueRange = localStorage.getItem('taxServiceRevenueRange');
        let lastVatFiledDate = localStorage.getItem('lastVatFiledDate');
        let business = JSON.parse(localStorage.getItem('business'));
        
        if (lastVatFiledDate === null || lastVatFiledDate === 'null') {
          lastVatFiledDate = '';
        }

        if (tccExpiryYear === null || tccExpiryYear === 'null') {
          tccExpiryYear = '';
        }

        var parameters = "";
        if(taxServicePlan==="cit"){
            parameters = `entity-id=${business.id}&service-id=${taxServicePlan}&revenue-range-id=${taxServiceRevenueRange}&year=0&filed-before-status=${taxStatus}&cit-vat-filed-before-status=${vatStatus}&tcc-expiry-year=${tccExpiryYear}&last-vat-filed-date=${lastVatFiledDate}`
        }else if (taxServicePlan === "pit") {
            parameters = `entity-id=${business.id}&service-id=${taxServicePlan}&revenue-range-id=${taxServiceRevenueRange}&year=0&filed-before-status=${taxStatus}&cit-vat-filed-before-status=${vatStatus}&tcc-expiry-year=${tccExpiryYear}&last-vat-filed-date=${lastVatFiledDate}`
        } else{
            parameters = `entity-id=${business.id}&service-id=${taxServicePlan}&payment-plan-id=`
        }
        
        $.ajax({
            type: 'POST',
            url: "api/v1/service/tax?" + parameters,
            headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
            success: (result)=>{
                localStorage.setItem('taxServiceRequestParams', parameters);
                window.location.href = `payment-summary?id=${result.id}&cit-service=${result.serviceCharge.citService}&vat-service=${result.serviceCharge.vatService}&delivery-service=${result.serviceCharge.deliveryService}&sub-total=${result.serviceCharge.subTotal}&vat=${result.serviceCharge.vat}&total=${result.serviceCharge.total}&serviceName=${result.serviceName}&discount-amount=${result.serviceCharge.discountAmount}&pre-discount-total=${result.serviceCharge.preDiscountTotal}`
      
            },
            error: function (request, exception, errorThrown) {
                $.toaster({ priority : 'danger', title : 'Error', message: 'PLease try again'});
          },
        })
    }
    
    
    function loadAndShowRevenueRange() {
        $('#revenueRange').show();
    
    /* ----------------------------------------------------------- */
    /*  Get Popup for Revenue Ranges
   /* ----------------------------------------------------------- */

   $.ajax({
    type: 'GET',
    url: 'api/v1/lookup/revenue-ranges',
    success: (result)=>{
        let output="";
        let count=1;
        
        for (var i in result){
          output+= `<div class="col-md-4 col-6 my-3 rev-range-box">
                      <div class="masthead-rev-range p-1 top-rev-range-box d-flex justify-content-center align-items-center flex-column">
                          <p class="m-0 p-0 text-center">Revenue Range ${count++}</p>
                          <hr class="rev-range-hr m-0">
                      </div>
                      <div class="bottom-rev-range-box p-1 d-flex justify-content-center align-items-center flex-column">
                          <p class="font-weight-bold text-center">${result[i].description}</p>
                          <button id="${result[i].id}" name="${result[i].description}" class="btn red-btn rev-range-button">Select Option</button>
                      </div>
                  </div>`
        }

        $("#revenueRangeList").html(output);

        $( ".rev-range-button" ).click(function() {

          $("#revRange").val($(this).attr('id'));
          $("#selectedRevenueRange").text($(this).attr('name'));
          $('#revenueRangeModal').modal('toggle');
          localStorage.setItem('taxServiceRevenueRange', $(this).attr('id'));
          $('#revenueRange').hide();
          showTccExpiry();

        });
    }
  });
    }
    
    function showTccExpiry() {
        var taxStatus = localStorage.getItem('taxStatus');
        if (taxStatus === 'OTHER' || taxStatus === 'TAX_MASTER') {
            $('#certificateExpiry').show();
        } else {
            $('#certificateExpiry').hide();
            $('#serviceSurvey').show();
        }
    }

    function showVatSurvey(){
        var vatStatus = localStorage.getItem('vatStatus');
        if (vatStatus === 'OTHER') {
            $('#lastVatFiledDate').show();
        } else {
            registerTaxServiceOnServer();
        }
    }
   
});
