$(function(){
    "use strict";

    $('#spin-page').hide();
    $('#spin-page-2').hide();
    $('#spin-page-3').hide();

    let grossAnnualIncome = 0;

    let taxReliefDividendPercentage = 0.9;
    let taxReliefInterestPercentage = 0.9;

    let dividendSolution = 0;
    let interestSolution = 0;

    let statutoryReliefs = 0;
    let statutoryReliefsPercentage = 0.2;
    let statutoryReliefsAddition = 200000;

    let totalNonTaxableIncome = 0;
    let totalTaxableIncome = 0;

    const rates = [7, 11, 15, 19, 21, 24];
    const steps = [300000, 300000, 500000, 500000, 1600000, 3200000]


    /* ----------------------------------------------------------- */
    /*  Functions Used In the App
   /* ----------------------------------------------------------- */

    let addGrossAnnualIncome = function(){
        grossAnnualIncome = totalEarnedIncome + totalUnearnedIncome;
        $('#grossAnnualIncome').text(modifyNumbers(grossAnnualIncome));

        statutoryReliefs = (grossAnnualIncome*statutoryReliefsPercentage) + statutoryReliefsAddition;
        $('#statutoryReliefs').text(modifyNumbers(statutoryReliefs));
    }

    let calcWithHoldingTaxForUnquotedDividend = function(value){
        let withholdingTaxValue = (0.10 * value)/0.90;

        return withholdingTaxValue;
    }

    let calcWithHoldingTax = function(value){

        let withholdingTaxValue = (0.05 * value)/0.95;

        return withholdingTaxValue;
    }

    let modifyNumbers = function(number){

        let numberToTwoDecimalPoints = number.toFixed(2);
        let numberToLocalString = Number(numberToTwoDecimalPoints).toLocaleString();

        return numberToLocalString;
    }

    let calcTotalAnnualTaxPayable = function(amount, index){
        if (rates.length> index){
            let tax, remainder;
            if (amount>steps[index] && index != rates.length-1){
                tax = rates[index]*steps[index]/100;
            }else{
                tax = rates[index]*amount/100;
            }
    
            remainder = amount - steps[index]
            if (remainder>0){
                return tax+ taxer(remainder, index+1)
            }else{
                return tax
            }
        }else{
            return 0;
        }
    }

    /* ----------------------------------------------------------- */
    /*  Earned Income
   /* ----------------------------------------------------------- */
    
   let totalEarnedIncome = 0;
    $('#calculate-earned-income').click(function(){
        $('#earned-income-collapse').collapse("toggle");
        $('#total-earned-income').empty();
        $('#spin-page').show();

        let salariesAndWages = Number($('#salaries-and-wages').val() ? $('#salaries-and-wages').val() : 0);
        let bonus = Number($('#bonus').val() ? $('#bonus').val() : 0);
        let allowances = Number($('#allowances').val() ? $('#allowances').val() : 0);
        let gratuity =  Number($('#gratuity').val() ? $('#gratuity').val() : 0);
        let compensations =  Number($('#compensations').val() ? $('#compensations').val() : 0);
        let benefits =  Number($('#benefits').val() ? $('#benefits').val() : 0);
        let pensionIncome = Number($('#pension-income').val() ? $('#pension-income').val() : 0);
        let premium = Number($('#premium').val() ? $('#premium').val() : 0);
        let otherEploymentIncome = Number($('#other-employment-income').val() ? $('#other-employment-income').val() : 0);

        totalEarnedIncome = Number(salariesAndWages + bonus + allowances + gratuity + compensations
                            + benefits + pensionIncome + premium + otherEploymentIncome);
        
        $('body,html').animate({
            scrollTop: 0
        }, 2000);
        setTimeout(function() {
            $('#spin-page').hide();
            $('#total-earned-income').text(modifyNumbers(totalEarnedIncome));
            addGrossAnnualIncome();
        }, 2000);

        $('#totalTaxableIncome').text(" ");
        $('#totalAnnualTaxPayable').text(" ");
        $('#withholdingtax').text(" ");
        $('#annualTaxDueValue').text(" ");
        $('#totalMonthlyTax').text(" ");
    });

    /* ----------------------------------------------------------- */
    /*  UnEarned Income
   /* ----------------------------------------------------------- */
    
   let rent = 0;
   let fees = 0;
   let totalUnearnedIncome = 0;
   let commission = 0;
   let interestFromUnquotedCompanies = 0;
   let dividendFromUnquotedCompanies = 0;
   let interestFromQuotedCompanies = 0;
   let dividendFromQuotedCompanies = 0;
   let otherIncome = 0;

    $('#calculate-unearned-income').click(function(){
        $('#unearned-income-collapse').collapse("toggle");
        $('#total-unearned-income').empty();
        $('#spin-page-2').show();

        rent = Number($('#rent').val() ? $('#rent').val() : 0);
        fees = Number($('#fees').val() ? $('#fees').val() : 0);
        commission = Number($('#commission').val() ? $('#commission').val() : 0);
        interestFromUnquotedCompanies =  Number($('#interestFromUnquotedCompanies').val() ? $('#interestFromUnquotedCompanies').val() : 0);
        dividendFromUnquotedCompanies =  Number($('#dividendFromUnquotedCompanies').val() ? $('#dividendFromUnquotedCompanies').val() : 0);
        interestFromQuotedCompanies =  Number($('#interestFromQuotedCompanies').val() ? $('#interestFromQuotedCompanies').val() : 0);
        dividendFromQuotedCompanies = Number($('#dividendFromQuotedCompanies').val() ? $('#dividendFromQuotedCompanies').val() : 0);
        otherIncome = Number($('#otherIncome').val() ? $('#otherIncome').val() : 0);

        dividendSolution = dividendFromQuotedCompanies/taxReliefDividendPercentage;
        interestSolution = interestFromQuotedCompanies/taxReliefInterestPercentage;


        $('#dividends').text(modifyNumbers(dividendSolution));
        $('#interest').text(modifyNumbers(interestSolution));

        totalUnearnedIncome = rent + fees + commission + interestFromUnquotedCompanies + dividendFromUnquotedCompanies
                            + interestFromQuotedCompanies + dividendFromQuotedCompanies + otherIncome;
        
        $('body,html').animate({
            scrollTop: 0
        }, 2000);
        setTimeout(function() {
            $('#spin-page-2').hide();
            $('#total-unearned-income').text(modifyNumbers(totalUnearnedIncome));
            addGrossAnnualIncome();
        }, 2000);

        $('#totalTaxableIncome').text(" ");
        $('#totalAnnualTaxPayable').text(" ");
        $('#withholdingtax').text(" ");
        $('#annualTaxDueValue').text(" ");
        $('#totalMonthlyTax').text(" ");
    });


    /* ----------------------------------------------------------- */
    /*  Deductions
   /* ----------------------------------------------------------- */
   
   let deductions = 0;
    $('#calculate-deductions').click(function(){
        $('#deductions').collapse("toggle");
        $('#deductionsValue').empty();
        $('#spin-page-3').show();

        let pensionContribution = Number($('#pensionContribution').val() ? $('#pensionContribution').val() : 0);
        let nhisContribution = Number($('#nhisContribution').val() ? $('#nhisContribution').val() : 0);
        let nhfContribution = Number($('#nhfContribution').val() ? $('#nhfContribution').val() : 0);
        let gratuity =  Number($('#gratuity').val() ? $('#gratuity').val() : 0);
        let lifeAssurancePayment =  Number($('#lifeAssurancePayment').val() ? $('#lifeAssurancePayment').val() : 0);

        deductions = pensionContribution + nhisContribution + nhfContribution + gratuity + lifeAssurancePayment
                            + dividendSolution + interestSolution;
        
        $('body,html').animate({
            scrollTop: 0
        }, 2000);
        setTimeout(function() {
            $('#spin-page-3').hide();
            $('#deductionsValue').text(modifyNumbers(deductions));

            totalNonTaxableIncome = (statutoryReliefs + deductions);
            $('#totalNonTaxableIncome').text(modifyNumbers(totalNonTaxableIncome));

        }, 2000);

        $('#totalTaxableIncome').text(" ");
        $('#totalAnnualTaxPayable').text(" ");
        $('#withholdingtax').text(" ");
        $('#annualTaxDueValue').text(" ");
        $('#totalMonthlyTax').text(" ");
    });

      /* ----------------------------------------------------------- */
    /*  Taxable Income
   /* ----------------------------------------------------------- */

    $('#calculatePersonalIncomeTax').click(function(){

        /* ----------------------------------------------------------- */
        /*  Total Taxable Income
        /* ----------------------------------------------------------- */

        totalTaxableIncome = (grossAnnualIncome-totalNonTaxableIncome);
        $('#totalTaxableIncome').text(modifyNumbers(totalTaxableIncome));

        /* ----------------------------------------------------------- */
        /*  Total Annual Tax Payable
        /* ----------------------------------------------------------- */

        let totalAnnualTaxPayable = calcTotalAnnualTaxPayable(totalTaxableIncome, 0);
        $('#totalAnnualTaxPayable').text(modifyNumbers(totalAnnualTaxPayable));

        /* ----------------------------------------------------------- */
        /*  Total Witholding Tax
        /* ----------------------------------------------------------- */


        let withHoldingTaxForRent = calcWithHoldingTax(rent);
        let withHoldingTaxForfees = calcWithHoldingTax(fees);
        let withHoldingTaxForCommision = calcWithHoldingTax(commission);
        let withHoldingTaxForinterestFromUnquotedCompanies = calcWithHoldingTax(interestFromUnquotedCompanies);
        let withHoldingTaxFordividendFromUnquotedCompanies = calcWithHoldingTaxForUnquotedDividend(dividendFromUnquotedCompanies);

        let finalWithholdingTax = withHoldingTaxForRent + withHoldingTaxForfees + withHoldingTaxForCommision 
                                    + withHoldingTaxForinterestFromUnquotedCompanies + withHoldingTaxFordividendFromUnquotedCompanies;
        $('#withholdingtax').text(modifyNumbers(finalWithholdingTax));

        /* ----------------------------------------------------------- */
        /*  Annual Tax Due
        /* ----------------------------------------------------------- */

        let annualTaxDueValue = totalAnnualTaxPayable - finalWithholdingTax;
        $('#annualTaxDueValue').text(modifyNumbers(annualTaxDueValue));

        /* ----------------------------------------------------------- */
        /*  Total Monthly Tax
        /* ----------------------------------------------------------- */

        let totalMonthlyTax = (annualTaxDueValue/12);
        $('#totalMonthlyTax').text(modifyNumbers(totalMonthlyTax));
        
    });


    /* ----------------------------------------------------------- */
    /*  Company Income Tax Calculator
   /* ----------------------------------------------------------- */
    let turnover = 0;
    let costOfSales = 0;
    let grossProfitValue = 0;
    let generalAdminExpenses = 0;
    let netProfitBeforeTaxValue = 0;
    let chargeableProfitValue = 0;
    let disallowedExpenses = 0;
    let nonTaxableIncome = 0;
    let lossBroughtFoward = 0;
    let balanceAdjustments = 0;
    let capitalAllowance = 0;
    let paidUpCapital = 0;


   let calcGrossProfit = function(){
       grossProfitValue = turnover-costOfSales;
       $("#grossProfit").text(modifyNumbers(grossProfitValue));
   }

   let calcNetProfitBeforeTax = function(){
        netProfitBeforeTaxValue = grossProfitValue - generalAdminExpenses;
        $("#netProfitBeforeTax").text(modifyNumbers(netProfitBeforeTaxValue));
   }

   let calcChargeableProfit = function(){
        chargeableProfitValue = netProfitBeforeTaxValue + disallowedExpenses - nonTaxableIncome - lossBroughtFoward
                                - balanceAdjustments - capitalAllowance;

        $("#chargeableProfit").text(modifyNumbers(chargeableProfitValue));
   }

   $("#turnover").change(function(){
    turnover = Number($(this).val());
    calcGrossProfit();
  });


  $("#costOfSales").change(function(){
    costOfSales = Number($(this).val());
    calcGrossProfit();
  });

  $("#generalAdminExpenses").change(function(){
    generalAdminExpenses = Number($(this).val());
    calcNetProfitBeforeTax();
  });

  $("#disallowedExpenses").change(function(){
    disallowedExpenses = Number($(this).val());
    calcChargeableProfit();
  });

  $("#nonTaxableIncome").change(function(){
    nonTaxableIncome = Number($(this).val());
    calcChargeableProfit();
  });

  $("#lossBroughtFoward").change(function(){
    lossBroughtFoward = Number($(this).val());
    calcChargeableProfit();
  });

  $("#balanceAdjustments").change(function(){
    balanceAdjustments = Number($(this).val());
    calcChargeableProfit();
  });

  $("#capitalAllowance").change(function(){
    capitalAllowance = Number($(this).val());
    calcChargeableProfit();
  });

  $("#paidUpCapital").change(function(){
    paidUpCapital = Number($(this).val());
    calcChargeableProfit();
  });


    $('#calculateCompanyIncomeTax').click(function(){
        let companyIncomeTaxValue = chargeableProfitValue * 0.3;
        $("#companyIncomeTax").text(modifyNumbers(companyIncomeTaxValue));

        let educationTaxValue = 0.2 * (netProfitBeforeTaxValue + disallowedExpenses - nonTaxableIncome - lossBroughtFoward);
        $("#educationTax").text(modifyNumbers(educationTaxValue));

        let informationTechnologyDevelopmentLevyValue = 0.1 * netProfitBeforeTaxValue;
        $("#informationTechnologyDevelopmentLevy").text(modifyNumbers(informationTechnologyDevelopmentLevyValue));
    });

});