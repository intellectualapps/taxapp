$(function(){
    "use strict";
    $("#spin1").hide();
    
    var loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));

   $.ajax({
    type: 'GET',
    url: `api/v1/user/${loggedInUser.username}`,
    headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
    success: (user)=>{
        console.log(user);
        
        $("#fullName").val(user.firstName + " " + user.lastName);
        $("#email").val(user.email);
        $("#phone").val(user.phoneNumber);
       
                            
        
    },
    error: function (request, exception, errorThrown) {
        
        
    },
});

    $("#updateProfileBtn").click(function () {
        $("#spin1").show();

        var fullName = $("#fullName").val();
        var nameParts = fullName.split(" ");
        var firstName = nameParts[0];
        var lastName = nameParts[1];

        var email = $("#email").val();
        var phone = $("#phone").val();

        $.ajax({
            type: 'PUT',
            url: `api/v1/user/${loggedInUser.username}?first-name=${firstName}&last-name=${lastName}&email=${email}&phone-number=${phone}`,
            headers: {"Authorization": 'bearer ' + localStorage.getItem('authToken')},
            success: (user) => {
                $("#spin1").hide();
                $.toaster({ priority : 'info', title : 'Profile update', message : "Profile updated successfully"});
            },
            error: function (request, exception, errorThrown) {
                $("#spin1").hide();
                $.toaster({ priority : 'danger', title : 'Profile update', message : "Internal Server Error"});

            },
        });
    });
    
$("#updateProfileBackBtn").click(function () {
    window.location.href = 'dashboard';
});

});