$(function(){
    "use strict";

       /* ----------------------------------------------------------- */
    /*  Admin Details summary page
   /* ----------------------------------------------------------- */
    
    var url_string = window.location.href;
	var url = new URL(url_string);
    var id = url.searchParams.get("id");
    let resultId = '';

    var loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
    
    if (loggedInUser.role === "USER") {
        $("#statusText").removeAttr("data-target");
        $("#notesDiv").html("");
    }

   $.ajax({
    type: 'GET',
    url: `api/v1/service/request-details?id=${id}`,
    headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
    success: (result)=>{
        console.log(result)
        resultId = result.id;
        let output = "";
        let base = `
        <div class="font-weight-bold">
            <p>Service Requested - <span>${result.requestedServiceName}</span></p>
            <p>Date - <span>${result.dateRequested}</span></p>
        </div>
        <div>
            <p>Company Name - <span>${result.clientName}</span></p>
        </div>
        <div >
            <p>Email Address - <span>${result.username}</span></p>
        </div>`

        var paymentSection = "";

        if (result.serviceCharge) {
            paymentSection = `<div>
                                <p class="text-center font-weight-bold">Payment</p>
                                <p>CIT - <span>${result.serviceCharge.citService}</span></p>
                                <p>Delivery - <span>${result.serviceCharge.deliveryService}</span></p>
                                <p>VAT - <span>${result.serviceCharge.vat}</span></p>
                                <p>VAT Service - <span>${result.serviceCharge.vatService}</span></p>
                                <p>Sub Total - <span>${result.serviceCharge.subTotal}</span></p>
                                <p>Total - <span>${result.serviceCharge.total}</span></p>
                            </div>`
        }

        if(result.incorporationRequest){
            output = `
                    <div >
                        <p>Company Address - <span>${result.incorporationRequest.companyAddress}</span></p>
                    </div>

                    <div >
                        <p>Company Industry - <span>${result.incorporationRequest.companyIndustryName}</span></p>
                    </div>

                    <div >
                        <p>Proposed Company Name One - <span>${result.incorporationRequest.proposedCompanyNameOne}</span></p>
                    </div>
                    <div >
                        <p>Proposed Company Name Two - <span>${result.incorporationRequest.proposedCompanyNameTwo}</span></p>
                    </div>
                    <div >
                        <p>Proposed Company Name Three - <span>${result.incorporationRequest.proposedCompanyNameThree}</span></p>
                    </div>
                    <div >
                        <p>Proposed Company Name Four - <span>${result.incorporationRequest.proposedCompanyNameFour}</span></p>
                    </div>
                `
        } else{
            output = `<div>
                        <p>Contact Name - <span>${result.entity.contactPersonFullName}</span></p>
                    </div>
                    <div >
                        <p>State - <span>${result.entity.stateName}</span></p>
                    </div>

                    <div >
                        <p>Address - <span>${result.entity.address}</span></p>
                    </div>

                    <div >
                        <p>Incorporation Type - <span>${result.entity.typeName}</span></p>
                    </div>

                    <div >
                        <p>Industry - <span>${result.entity.industryName}</span></p>
                    </div>

                    <div >
                        <p>Date of Incorporation - <span>${result.entity.incorporationDate}</span></p>
                    </div>

                    <div >
                        <p>Date Of Commencement - <span>${result.entity.commencementDate}</span></p>
                    </div>

                    <div >
                        <p>Revenue Range - <span>${result.entity.revenueRangeName}</span></p>
                    </div>
                    <div >
                        <p>TIN - <span>${result.entity.tin}</span></p>
                    </div>`
        }
        $('#statusText').text(result.requestStatus); 
        $("#adminDetails").html(base + output + paymentSection);
        
        var rawDocumentUrls = result.documentUrls;
        var documentUrls = null;
        if (rawDocumentUrls == null) {
             documentUrls = "";
        } else {
             documentUrls = rawDocumentUrls.split(',');
        }
        
        var documentDivs = "";
        for (var i = 0; i < documentUrls.length; i++) {            
            documentDivs = documentDivs +  "<div class='col-4 col-md-3 my-3 '><a href='"+documentUrls[i]+"' target='_blank'><img src='"+documentUrls[i]+"' width=100 /></a></div>" ;
        }
        
        $("#documentDiv").html(documentDivs);
                            
        
    },
    error: function (request, exception, errorThrown) {
        $('#tableLoader').removeClass('d-flex').hide();
        $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
        
    },
});

if (loggedInUser.role === "SUPER_ADMIN" || loggedInUser.role === "ADMIN") {
     
var pageSize = 20;
var pageNumber = 1;
$.ajax({
    type: 'GET',
    url: `api/v1/service/note/${id}?page-size=${pageSize}&page-number=${pageNumber}`,
    headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
    success: (result)=>{
        console.log(result)
        let output="";
            let badge = "";
            let serialNumber  = pageSize * pageNumber-20;
            //if(serialNumber === 0){
                serialNumber = serialNumber+1;
            //}

            for (var i in result.notes){
                let note = result.notes[i];
              
              output+= `<tr id="${note.id}">
                            <th scope="row">${serialNumber++}</th>
                            <td class="custom-table-rows">${note.text}</td>
                            <td class="custom-table-rows">${note.dateAdded}</td>
                            <td class="custom-table-rows">${note.username}</td>
                        
                        </tr>`
            }

            $('#tableLoader').removeClass('d-flex').hide();
            $("#noteTableDetails").html(output);
    },
    error: function (request, exception, errorThrown) {
        $('#tableLoader').removeClass('d-flex').hide();
        $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
        
    },
});

$('#noteSubmitBtn').click(function(){
    var noteText = $('#noteInput').val();
    if (noteText === "") {
        alert("Please provide a note");
    }
    $.ajax({
        type: 'POST',
        url: `api/v1/service/note?user-tax-service-id=${id}&note-text=${noteText}`,
        headers: {"Authorization": 'bearer ' + localStorage.getItem('authToken')},
        success: (result) => {
            if (result.dateAdded == null) {
                $.toaster({priority: 'danger', title: 'Error', message: "Sorry, something went wrong"});
            } else {
                $.toaster({priority: 'success', title: 'Success', message: "Note added successfully"});
                window.location.href = `details?id=${id}`
            }
        }
    });
})
} else {
    $("#notes").html("");
}

$('#completeBadge').click(function(){
    let text = 'COMPLETED';
    $('#statusModal').modal('toggle');
    $.ajax({
        type: 'PUT',
        url: `api/v1/admin/service-status/${resultId}/${text}`,
        headers: {"Authorization": 'bearer ' + localStorage.getItem('authToken')},
        success: (result) => {
            if (result.status === true) {
                $.toaster({priority: 'success', title: 'Success', message: "Request status change successfull"});
                $('#statusText').text(text);
            } else {
                $.toaster({priority: 'danger', title: 'Error', message: "Sorry, something went wrong"});
            }
        }
    });
})

$('#inProcessBadge').click(function(){
    let text = 'IN_PROCESS';
    $('#statusModal').modal('toggle');
    $.ajax({
        type: 'PUT',
        url: `api/v1/admin/service-status/${resultId}/${text}`,
        headers: {"Authorization": 'bearer ' + localStorage.getItem('authToken')},
        success: (result) => {
            if (result.status === true) {
                $.toaster({priority: 'success', title: 'Success', message: "Request status change successfull"});
                $('#statusText').text(text);
            } else {
                $.toaster({priority: 'danger', title: 'Error', message: "Sorry, something went wrong"});
            }
        }
    });
})

$('#onHoldBadge').click(function(){
    let text = 'ON_HOLD';
    $('#statusModal').modal('toggle');
    $.ajax({
        type: 'PUT',
        url: `api/v1/admin/service-status/${resultId}/${text}`,
        headers: {"Authorization": 'bearer ' + localStorage.getItem('authToken')},
        success: (result) => {
            if (result.status === true) {
                $.toaster({priority: 'success', title: 'Success', message: "Request status change successfull"});
                $('#statusText').text(text);
            } else {
                $.toaster({priority: 'danger', title: 'Error', message: "Sorry, something went wrong"});
            }
        }
    });
})

$('#adminDetailsBack').click(function(){
        if (loggedInUser.role === "USER") {
            window.location.href = 'request-history';
        } else if (loggedInUser.role === "SUPER_ADMIN") {
            window.location.href = `admin?t=${loggedInUser.authToken}`;
        } else if (loggedInUser.role === "ADMIN") {
            window.location.href = `admin?t=${loggedInUser.authToken}`;
        }
})

$('#uploadDocuments').click(function(){
        localStorage.setItem('serviceIdFromPaySummary', resultId);
            window.location.href = 'document-upload';
        
})



});