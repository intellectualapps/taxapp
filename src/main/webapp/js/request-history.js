
  let pageNumber = 1;
   let pageSize = 10;

function next() {
   $('#tableLoader').addClass('d-flex').show();
   pageNumber = pageNumber+1;
   $('#searchButton').trigger('click');
};

function prev() {
   $('#tableLoader').addClass('d-flex').show();
   pageNumber = pageNumber-1;
   $('#searchButton').trigger('click');
};


$(function(){
    "use strict";

       /* ----------------------------------------------------------- */
    /*  Request History page
   /* ----------------------------------------------------------- */

 

   let getAllData = function(pageNumber, pageSize){

    $.ajax({
        type: 'GET',
        url: `api/v1/service/user?page-number=${pageNumber}&page-size=${pageSize}`,
        headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
        success: (result)=>{
            let output="";
            let badge = "";
            let serialNumber  = pageSize * pageNumber-10;
            //if(serialNumber === 0){
                serialNumber = serialNumber+1;
            //}
    
            for (var i in result.appServiceRequestSummaries){
                let eachValue = result.appServiceRequestSummaries[i];
              output+= `<tr id="${eachValue.id}" >
                            <th scope="row">${serialNumber++}</th>
                            <td class="custom-table-rows">${eachValue.clientName}</td>
                            <td class="custom-table-rows">${eachValue.requestedServiceName}</td>
                            <td class="custom-table-rows">${eachValue.dateRequested}</td>`
    
                if(eachValue.requestStatus === 'PENDING'){
                    badge = 'badge-danger' ;
                } else if(eachValue.requestStatus === 'IN PROCESS'){
                    badge = 'badge-warning' ;
                }else{
                    badge = 'badge-success' ;
                }
             output+= `<td><span class="badge badge-pill ${badge}">${eachValue.requestStatus}</span></td></tr>`
            }
            $('#tableLoader').removeClass('d-flex').hide();
            $("#historyTable").html(output);
        },
          error: function (request, exception, errorThrown) {
            pageNumber = 1;
            pageSize = 10;
            $('#tableLoader').removeClass('d-flex').hide();
            $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
            
        },
      });

   }
   
    const fetchServices = () => {
    $.ajax({
      type: 'GET',
      url: `api/v1/lookup/services`,
      headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
      success: (services) => {
        for (let i = 0; i < services.length; i++) {
          let id = services[i].id;
          let description = services[i].description;
          $('#servicesInput').append(`<option value='${id}'>${description}</option>`);
        }
      },
      error: () => {
        $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
      }
    })
   }

  const fetchStatuses = () => {
    $.ajax({
      type: 'GET',
      url: `api/v1/lookup/service-statuses`,
      headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
      success: (statuses) => {
        for (let i = 0; i < statuses.length; i++) {
          let description = statuses[i].description;
          let id = statuses[i].id;
          $('#statusInput').append(`<option value='${id}'>${description}</option>`);
        }
      },
      error: () => {
        $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
      }
    })
   }

   getAllData(pageNumber, pageSize);
   fetchServices();
   fetchStatuses();

   $('#nextBtn').click(function(){
       $('#tableLoader').addClass('d-flex').show();
       pageNumber = pageNumber+1;
       getAllData(pageNumber, pageSize)
   });

   $('#prevBtn').click(function(){
       $('#tableLoader').addClass('d-flex').show();
       pageNumber = pageNumber-1;
       getAllData(pageNumber, pageSize)
   });
    
    
    $('#searchButton').click(
    function() {
      $("#historyTable").empty();
      $("#navigateBtn").empty();
      let newButton = "";
      newButton += `<div id="newPrevBtn" class="btn red-btn" onclick="prev()">Prev</div>
                        <div id="newNextBtn" class="btn red-btn" onclick="next()">Next</div>`
      $("#navigateBtn").append(newButton);

      let searchTerm = $('#searchInput').val();
      let serviceId = $('#servicesInput').val();
      let status = $('#statusInput').val();
      console.log("about to search");
      $.ajax({
        type: 'GET',
        url: `api/v1/service/search?search-term=${searchTerm}&service-id=${serviceId}&status=${status}&page-number=${pageNumber}&page-size=${pageSize}`,
        headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
        success: (result) => {
            let res = result.appServiceRequestSummaries;
            console.log("result: " + res);
            let output="";
            let badge = "";
            let serialNumber  = pageSize * pageNumber-10;
            //if(serialNumber === 0){
                serialNumber = serialNumber+1;
            //}

            for (var i in res){
                let eachValue = res[i];
                            output+= `<tr class="custom-table-rows">
                            <th scope="row">${serialNumber++}</th>
                            <td>${eachValue.clientName}</td>
                            <td>${eachValue.requestedServiceName}</td>
                            <td>${eachValue.dateRequested}</td>`
    
                if(eachValue.requestStatus === 'PENDING'){
                    badge = 'badge-danger' ;
                } else if(eachValue.requestStatus === 'IN PROCESS'){
                    badge = 'badge-warning' ;
                }else{
                    badge = 'badge-success' ;
                }
             output+= `<td><span class="badge badge-pill ${badge}">${eachValue.requestStatus}</span></td></tr>`
            }

            $('#tableLoader').removeClass('d-flex').hide();
            $("#historyTable").html(output);

        },
        error: function (e) {
          console.log(e)
          $.toaster({ priority : 'danger', title : 'Error', message : "Error: Try Again"});
        },
      });

    });
   
   $(document).on('click', '.custom-table-rows', function(){
        let id = $(this).parent().attr('id');
        window.location.href = `details?id=${id}`

   })
   
});