$(function(){
    "use strict";

//     /* ----------------------------------------------------------- */
//       /*  Login Page
//      /* ----------------------------------------------------------- */
    
    $("#spin1").hide();

    $("form[name='loginForm']").validate({
        rules: {
          id:{
            required: true,
            email: true
          },
          password: {
            required: true
          }
        },
        messages: {
          id: "*Your Email is required",
          password: "*Your password is required"
        },
        submitHandler: function(form) {
          $("#spin1").show();
          
          $.ajax({
              type: 'POST',
              url: "api/v1/user/authenticate?auth-type=email&" + $(form).serialize(),
              success: (result)=>{
                  localStorage.setItem('loggedInUser', JSON.stringify(result));
                  localStorage.setItem('authToken', result.authToken);

                  var businessData = result.businesses[0];

                  localStorage.setItem('business', JSON.stringify(businessData));

                  if (result.role === "USER") {
                      window.location.href = 'dashboard';
                  } else if (result.role === "SUPER_ADMIN") {
                      window.location.href = `admin?t=${result.authToken}`;
                  } else if (result.role === "ADMIN") {
                      window.location.href = `admin?t=${result.authToken}`;
                  }

              },
              error: function (request, exception, errorThrown) {
                $("#spin1").hide();
                if(request.responseJSON.message){
                  $.toaster({ priority : 'danger', title : 'Error', message : request.responseJSON.message});
                } else {
                  $.toaster({ priority : 'danger', title : 'Error', message : "Internal Server Error"});
                }
            },
          })
        }
      });
});