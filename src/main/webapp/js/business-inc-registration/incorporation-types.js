$(function(){
    "use strict";
    

       /* ----------------------------------------------------------- */
    /*  Incorporation type service
   /* ----------------------------------------------------------- */
    
    $("#spin-page").show();
    
    $.ajax({
        type: 'GET',
        url: 'api/v1/payment-plan/incorporation-plans',
        headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
        success: (result)=>{
        
            let output="";
            let paymentPlans = result.paymentPlans;
            let services = result.services;

            for (var i = 0; i < paymentPlans.length; i++) { 
                for (var j = 0; j < services.length; j++) { 
                    if (paymentPlans[i].serviceId === services[j].id) {
                         output+=`<div class="col-md-4 col-12 my-3" onclick="window.location.href='payment-summary?id=${paymentPlans[i].id}&frequency=${paymentPlans[i].frequency}&amount=${paymentPlans[i].amount}&serviceName=${services[j].description}&incType=true';">
                            <div class="tax-service-body-content d-flex justify-content-center align-items-center flex-column">
                                <img src="images/white-hand-money.png" alt="Survey Page Logo" height="80">
                                <p class="my-3">${services[j].description}</p>
                                <p class="font-weight-bold">(${paymentPlans[i].frequency} @ ${paymentPlans[i].amount})</p>
                            </div>
                        </div>`
                    }
                }
            }
    
            $("#item-list").html(output);

            $("#spin-page").hide();
        }
      });
      
});