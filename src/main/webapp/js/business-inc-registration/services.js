$(function(){
    "use strict";
  
    $("#step2").hide();
    $("#step3").hide();
     $("#spin1").hide();
     $("#spin2").hide();

  var url_string = window.location.href;
	var url = new URL(url_string);
    
  var fromDashboard = url.searchParams.get("fromDashboard");

  if(fromDashboard){
    $("#step1").hide();
    $("#step2").hide();
    $("#step3").show();
  }

     /* ----------------------------------------------------------- */
    /*  Survey Page Logic
   /* ----------------------------------------------------------- */

   var surveyAnswer = "";

   function undoUsedTaxMaster(){
    $('#usedTaxMaster div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
    $('#usedTaxMaster div img').prop("src", "images/hand shake.png").fadeIn(400);
    $('#corresponding-word').empty();
   }

   function undoUsedAnotherCompany(){
    $('#usedAnotherCompany div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
    $('#usedAnotherCompany div img').prop("src", "images/document-icon.png").fadeIn(400);
    $('#corresponding-word').empty();
   }

   function undoNeverFilledTax(){
    $('#neverFilledTax div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
    $('#neverFilledTax div img').prop("src", "images/new-tax-filing-request.png").fadeIn(400);
    $('#corresponding-word').empty();
   }

   $("#usedTaxMaster").click(function(){
    undoNeverFilledTax();
    undoUsedAnotherCompany();
    $('#usedTaxMaster div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
    $('#usedTaxMaster div img').prop("src", "images/check.png").fadeIn(400);
    $('#corresponding-word').append(`<p>Welcome back! Since we know all about you, your company registration 
    will be easier than ever.</p>`).fadeIn(800);
    surveyAnswer = "1";
   });

   $("#usedAnotherCompany").click(function(){
    undoUsedTaxMaster();
    undoNeverFilledTax();
    $('#usedAnotherCompany div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
    $('#usedAnotherCompany div img').prop("src", "images/check.png").fadeIn(400);
    $('#corresponding-word').append(`<p>Welcome to tax master! we have made it easier than ever for you to 
    register your company.</p>`).fadeIn(800);
    surveyAnswer = "2"
   });

   $("#neverFilledTax").click(function(){
    undoUsedTaxMaster();
    undoUsedAnotherCompany();
    $('#neverFilledTax div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
    $('#neverFilledTax div img').prop("src", "images/check.png").fadeIn(400);
    $('#corresponding-word').append(`<p>Welcome to tax master! we have made it easier than ever for you to 
    register your company</p>`).fadeIn(800);
    surveyAnswer = "3"
   });

   $("#step2Back").click(function(){
    $('#step2, #step1').toggle();
   });

   $("#step2Next").click(function(){
    $('#step2, #step3').toggle();
   });

   $("#step3Back").click(function(){
    $('#step3, #step2').toggle();
   });


    /* ----------------------------------------------------------- */
    /*  Get dropdown for Industries
   /* ----------------------------------------------------------- */

   $.ajax({
    type: 'GET',
    url: 'api/v1/lookup/industries',
    success: (result)=>{
        let output="";

        for (var i in result){
          output+= `<div class="col-md-4 col-6 p-2 industry-value" name="${result[i].description}" id="${result[i].id}">
                        <div class="custom-modal-box d-flex justify-content-center align-items-center p-2">
                            <p class="m-0">${result[i].description}</p>
                        </div>
                    </div>`
        }

        $("#indutriesList").html(output);

        $( ".industry-value" ).click(function() {

          $("#busIndustry").val($(this).attr('id'));
          $("#selectedIndustry").text($(this).attr('name'));
          $('#industriesModal').modal('toggle');

        });
    }
  });
  
      /* ----------------------------------------------------------- */
      /*  Business Incorporation registration Step 1
     /* ----------------------------------------------------------- */
  
  $("form[name='step1Form']").validate({
      rules: {
        fullNameInput:{
          required: true,
          minlength: 1
        },
        emailInput: {
          required: true,
          email: true
        },
        confirmEmailInput: {
          required: true,
          equalTo: emailInput
        },
        phoneInput: {
          number: true,
          required: true,
          minlength: 11
        },
        terms: {
          required: true
        }
      },
      messages: {
        fullNameInput: "*Your full name is required",
        emailInput: "*Your email address is required",
        confirmEmailInput : "*Make sure the emails are equal",
        phoneInput: {
          number: "*Please input a valid phone number",
          required: "*Your phone number is required",
          minlength: "*Please input a valid phone number"
        },
        terms: "*You must agree to the terms"
      },
      submitHandler: function(form) {
        $("#spin1").show();
        $("form[name='step1Form'] > span").remove();
        $('input[name="emailInput"]').prop('name', "id")
        $('input[name="fullNameInput"]').prop('name', "full-name")
        $('input[name="phoneInput"]').prop('name', "phone-number")
        
        $.ajax({
            type: 'POST',
            url: "api/v1/user?" + $(form).serialize(),
            success: (result)=>{
                localStorage.setItem('user', JSON.stringify(result))
                localStorage.setItem('authToken', result.authToken);
                $("form[name='step1Form'] > span").remove()
                $('#step1, #step2').toggle();
                $('#step1Title, #step2Title').toggle();      
            },
            error: function (request, exception, errorThrown) {
              $("#spin1").hide();
              if(request.responseJSON.message){
                $.toaster({ priority : 'danger', title : 'Error', message : request.responseJSON.message});
              } else {
                $.toaster({ priority : 'danger', title : 'Error', message : "Internal Server Error"});
              }
          },
        })
      }
    });
  
//     /* ----------------------------------------------------------- */
//       /*  Business Incorporation registration Step 2
//      /* ----------------------------------------------------------- */
  
    $("form[name='step3Form']").validate({
      rules: {
        busName1:{
          required: true
        },
        busName2:{
            required: true
        },
        busName3:{
            required: true
        },
        busName4:{
            required: true
        },
        busAddress: {
            required: true
        }
      },
      messages: {
        busName1: "*This field is required",
        busName2: "*This field is required",
        busName3: "*This field is required",
        busName4: "*This field is required",
        busAddress: "*This field is required"
      },
      submitHandler: function(form) {
        $("#spin2").show();

        $('input[name="busName1"]').prop('name', "proposed-company-name-one")
        $('input[name="busName2"]').prop('name', "proposed-company-name-two")
        $('input[name="busName3"]').prop('name', "proposed-company-name-three")
        $('input[name="busName4"]').prop('name', "proposed-company-name-four")
        $('textarea[name="busAddress"]').prop('name', "company-address")
        
        $.ajax({
            type: 'POST',
            url: "api/v1/business/incorporation-request?" + $(form).serialize(),
            headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
            success: (result)=>{
              localStorage.setItem('proposedBusiness', JSON.stringify(result))
               window.location.href = 'bus-account-creation-confirmation';
      
            },
            error: function (request, exception, errorThrown) {
              $("form[name='step3Form'] > span").remove();
              $("#spin2").hide();
              if(request.responseJSON.message){
                $.toaster({ priority : 'danger', title : 'Error', message : request.responseJSON.message});
              } else {
                $.toaster({ priority : 'danger', title : 'Error', message : "Internal Server Error"});
              }
          },
        })
      }
    });
   
   });