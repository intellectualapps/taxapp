$(function(){
  "use strict";

   $("#commencementDate").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true });
   $("#incorporationDate").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true });

   $( document ).tooltip();
   
   $("#step1").hide();
   $("#step2").hide();
   //$("#step3").hide();

  var url_string = window.location.href;
	var url = new URL(url_string);
    
  var fromDashboard = url.searchParams.get("fromDashboard");

  if(fromDashboard){
    $("#step1").hide();
    $("#step2").hide();
    $("#step3").show();
  }

   /* ----------------------------------------------------------- */
    /*  Survey Page Logic
   /* ----------------------------------------------------------- */

   var surveyAnswer = "";

   function undoUsedTaxMaster(){
    $('#usedTaxMaster div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
    $('#usedTaxMaster div img').prop("src", "images/hand shake.png").fadeIn(400);
    $('#corresponding-word').empty();
   }

   function undoUsedAnotherCompany(){
    $('#usedAnotherCompany div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
    $('#usedAnotherCompany div img').prop("src", "images/document-icon.png").fadeIn(400);
    $('#corresponding-word').empty();
   }

   function undoNeverFilledTax(){
    $('#neverFilledTax div').removeClass('survey-body-content-clicked').addClass('survey-body-content');
    $('#neverFilledTax div img').prop("src", "images/new-tax-filing-request.png").fadeIn(400);
    $('#corresponding-word').empty();
   }

   $("#usedTaxMaster").click(function(){
    undoNeverFilledTax();
    undoUsedAnotherCompany();
    $('#usedTaxMaster div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
    $('#usedTaxMaster div img').prop("src", "images/check.png").fadeIn(400);
    $('#corresponding-word').append(`<p>Welcome back! Since we know all about you, your 
    tax processing will be easier than ever</p>`).fadeIn(800);
    surveyAnswer = "TAX_MASTER";
    localStorage.setItem('taxStatus', surveyAnswer);
   });

   $("#usedAnotherCompany").click(function(){
    undoUsedTaxMaster();
    undoNeverFilledTax();
    $('#usedAnotherCompany div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
    $('#usedAnotherCompany div img').prop("src", "images/check.png").fadeIn(400);
    $('#corresponding-word').append(`<p>Welcome to Tax Master! We have made it 
    easier than ever for you to process your returns</p>`).fadeIn(800);
    surveyAnswer = "OTHER"
    localStorage.setItem('taxStatus', surveyAnswer);
   });

   $("#neverFilledTax").click(function(){
    undoUsedTaxMaster();
    undoUsedAnotherCompany();
    $('#neverFilledTax div').removeClass('survey-body-content').addClass('survey-body-content-clicked').fadeIn(400);
    $('#neverFilledTax div img').prop("src", "images/check.png").fadeIn(400);
    $('#corresponding-word').append(`<p>Don't worry - tax master will be 
    with you every step of the way for your ease and convenience</p>`).fadeIn(800);
    surveyAnswer = "NEVER"
    localStorage.setItem('taxStatus', surveyAnswer);
   });

   $("#step2Back").click(function(){
    $('#step2, #step1').toggle();
   });

   $("#step2Next").click(function(){
    $('#step2, #step3').toggle();
   });

   $("#revenueRangeBack").click(function(){
       window.location.href = 'dashboard';
   });

   $("#revenueRangeNext").click(function(){
       window.location.href = 'tax-service';
   });

   $("#spin1").hide();
   $("#spin2").hide();


   /* ----------------------------------------------------------- */
    /*  Get popup for Industries
   /* ----------------------------------------------------------- */

   $.ajax({
    type: 'GET',
    url: 'api/v1/lookup/industries',
    success: (result)=>{
        let output="";

        for (var i in result){
          output+= `<div class="col-md-4 col-6 p-2 industry-value" name="${result[i].description}" id="${result[i].id}">
                        <div class="custom-modal-box d-flex justify-content-center align-items-center p-2">
                            <p class="m-0">${result[i].description}</p>
                        </div>
                    </div>`
        }

        $("#indutriesList").html(output);

        $( ".industry-value" ).click(function() {

          $("#busIndustry").val($(this).attr('id'));
          $("#selectedIndustry").text($(this).attr('name'));
          $('#industriesModal').modal('toggle');

        });
    }
  });


   /* ----------------------------------------------------------- */
    /*  Get Popup for Revenue Ranges
   /* ----------------------------------------------------------- */

   $.ajax({
    type: 'GET',
    url: 'api/v1/lookup/revenue-ranges',
    success: (result)=>{
        let output="";
        let count=1;
        
        for (var i in result){
          output+= `<div class="col-md-4 col-6 my-3 rev-range-box">
                      <div class="masthead-rev-range p-1 top-rev-range-box d-flex justify-content-center align-items-center flex-column">
                          <p class="m-0 p-0 text-center">Revenue Range ${count++}</p>
                          <hr class="rev-range-hr m-0">
                      </div>
                      <div class="bottom-rev-range-box p-1 d-flex justify-content-center align-items-center flex-column">
                          <p class="font-weight-bold text-center">${result[i].description}</p>
                          <button id="${result[i].id}" name="${result[i].description}" class="btn red-btn rev-range-button">Select Option</button>
                      </div>
                  </div>`
        }

        $("#revenueRangeList").html(output);

        $( ".rev-range-button" ).click(function() {

          $("#revRange").val($(this).attr('id'));
          $("#selectedRevenueRange").text($(this).attr('name'));
          $('#revenueRangeModal').modal('toggle');

        });
    }
  });

   /* ----------------------------------------------------------- */
    /*  Get Popup for States
   /* ----------------------------------------------------------- */

   $.ajax({
    type: 'GET',
    url: 'api/v1/lookup/states',
    success: (result)=>{
      let output="";

      for (var i in result){
        output+= `<div class="col-md-4 col-6 p-2 states-value" name="${result[i].state}" id="${result[i].id}">
                      <div class="custom-modal-box d-flex justify-content-center align-items-center p-2">
                          <p class="m-0">${result[i].state}</p>
                      </div>
                  </div>`
      }

      $("#statesList").html(output);

      $( ".states-value" ).click(function() {

        $("#state").val($(this).attr('id'));
        $("#selectedState").text($(this).attr('name'));
        $('#statesModal').modal('toggle');

      });

  }
});

   /* ----------------------------------------------------------- */
    /*  Get popup for Incorporation Types
   /* ----------------------------------------------------------- */

   $.ajax({
    type: 'GET',
    url: 'api/v1/lookup/business-types',
    success: (result)=>{
        let output="";

        for (var i in result){
          output+= `<div class="col-md-4 col-6 p-2 incorporation-value" name="${result[i].description}" id="${result[i].id}">
                        <div class="custom-modal-box d-flex justify-content-center align-items-center p-2">
                            <p class="m-0">${result[i].description}</p>
                        </div>
                    </div>`
        }

        $("#incorporationTypeList").html(output);

        $( ".incorporation-value" ).click(function() {

          $("#busType").val($(this).attr('id'));
          $("#selectedIncorporation").text($(this).attr('name'));
          $('#incorporationTypeModal').modal('toggle');

        });

    }
  });

    /* ----------------------------------------------------------- */
    /*  Post Tax Services registration Step 1
   /* ----------------------------------------------------------- */

   $('#id-name--1').change(function() {

      if (this.checked == true) {
        $('#tin').prop('disabled', true);
      } else {
        $('#tin').prop('disabled', false);
      }
  });

  $('#id-name--2').change(function() {

      if (this.checked == true) {
        $('.notPIT').fadeOut(1000);
        $('#detailsHeader').text('Business Details').fadeIn(1500)
        $('label[for="revRange"]').text('Annual Income Range');
        $('[name="busName"], [name="business-type-id"], [name="business-industry-id"], [name="incorporation-date"], [name="commencement-date"]').prop('disabled', true);
      } else {
        $('.notPIT').fadeIn(1000);
        $('#detailsHeader').text('Company Details').fadeIn(1500);
        $('label[for="revRange"]').text('Annual Revenue Range');
        $('[name="busName"], [name="business-type-id"], [name="business-industry-id"], [name="incorporation-date"], [name="commencement-date"]').prop('disabled', false);
      }
  });

  $("form[name='step1Form']").validate({
    rules: {
      fullNameInput:{
        required: true,
        minlength: 1
      },
      emailInput: {
        required: true,
        email: true
      },
      confirmEmailInput: {
        required: true,
        equalTo: emailInput
      },
      phoneInput: {
        number: true,
        required: true,
        minlength: 11
      },
      terms: {
        required: true
      }
    },
    messages: {
      fullNameInput: "*Your full name is required",
      emailInput: "*Your email address is required",
      confirmEmailInput : "*Make sure the emails are equal",
      phoneInput: {
        number: "*Please input a valid phone number",
        required: "*Your phone number is required",
        minlength: "*Please input a valid phone number"
      },
      terms: "*You must agree to the terms"
    },
    submitHandler: function(form) {
      $("#spin1").show();
      let userName =  $("input[name='fullNameInput']").val();
      $("form[name='step1Form'] > span").remove();
      $('input[name="emailInput"]').prop('name', "id")
      $('input[name="fullNameInput"]').prop('name', "full-name")
      $('input[name="phoneInput"]').prop('name', "phone-number")
      
      $.ajax({
          type: 'POST',
          url: "api/v1/user?" + $(form).serialize(),
          success: (result)=>{
              localStorage.setItem('user', JSON.stringify(result))
              localStorage.setItem('authToken', result.authToken);
              $("form[name='step1Form'] > span").remove()
              $('#step1, #step2').toggle();
              $('#step1Title, #step2Title').toggle();
    
          },
          error: function (request, exception, errorThrown) {
            $("#spin1").hide();
            if(request.responseJSON.message){
              $.toaster({ priority : 'danger', title : 'Error', message : request.responseJSON.message});
            } else {
              $.toaster({ priority : 'danger', title : 'Error', message : "Internal Server Error"});
            }
        },
      })
    }
  });

  /* ----------------------------------------------------------- */
    /*  Post Tax Services registration Step 2
   /* ----------------------------------------------------------- */

  $("form[name='step3Form']").validate({
    rules: {
      busName:{
        required: true,
      },
      busAddress: {
        required: true
      },
      tin: {
        minlength: 10
      }
    },
    messages: {
      busName: "*Your business name is required",
      busAddress: "*Your business address is required",
      tin: "*Your TIN must be 10 digits or more"
    },
    submitHandler: function(form) {
      $("#spin2").show();
      let businessName =  $("input[name='busName']").val();
      $('input[name="busName"]').prop('name', "business-name")
      $('textarea[name="busAddress"]').prop('name', "business-address") 
      
      $.ajax({
          type: 'POST',
          url: "api/v1/business/tax?" + $(form).serialize(),
          headers: {"Authorization": 'bearer '+localStorage.getItem('authToken')},
          success: (result)=>{
            console.log(result)
            localStorage.setItem('business', JSON.stringify(result))
             window.location.href = 'tax-account-creation-confirmation';
    
          },
          error: function (request, exception, errorThrown) {
            $("form[name='step3Form'] > span").remove();
            $("#spin2").hide();
            if(request.responseJSON.message){
              $.toaster({ priority : 'danger', title : 'Error', message : request.responseJSON.message});
            } else {
              $.toaster({ priority : 'danger', title : 'Error', message : "Internal Server Error"});
            }
        },
      })
    }
  });
 
 });