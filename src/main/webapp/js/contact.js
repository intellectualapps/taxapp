$(function(){
    "use strict";

    $('#characterLeft').text('1000 characters left');
    $('#message').keydown(function () {
        var max = 1000;
        var len = $(this).val().length;
        if (len >= max) {
            $('#characterLeft').text('You have reached the limit');
            $('#characterLeft').addClass('red');
            $('#btnSubmit').addClass('disabled');            
        } 
        else {
            var ch = max - len;
            $('#characterLeft').text(ch + ' characters left');
            $('#btnSubmit').removeClass('disabled');
            $('#characterLeft').removeClass('red');            
        }
    });  
    
    $.ajax({
        type: 'GET',
        url: '/ta/api/v1/lookup/contact-form-subject',
        success: (result)=>{
            let output="";
    
            for (var i in result){
              output+= `<div class="col-md-4 col-6 p-2 incorporation-value" name="${result[i].description}" id="${result[i].id}">
                            <div class="custom-modal-box d-flex justify-content-center align-items-center p-2">
                                <p class="m-0">${result[i].description}</p>
                            </div>
                        </div>`
            }
    
            $("#subjectList").html(output);
    
            $( ".incorporation-value" ).click(function() {
    
              $("#subject").val($(this).attr('name'));
              $("#selectedSubject").text($(this).attr('name'));
              $('#subjectModal').modal('toggle');
    
            });
    
        }
      });
    
    $("#submit").click(function() {
        var name = $("#name").val();
        var email = $("#email").val();
        var subject = $("#subject").val();
        var message = $("#message").val();
        $.ajax({
        type: 'POST',
        url: '/ta/api/v1/user/contact?name=' + name + '&email=' + email + '&subject=' + subject + '&message=' + message,        
        success: (result)=>{
            alert("Your message has been sent. Thank you for reaching out to us.");
        }
      });

    })
   
});