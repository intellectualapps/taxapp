$(function(){
    "use strict";

//     /* ----------------------------------------------------------- */
//       /*  Login Page
//      /* ----------------------------------------------------------- */
    
    //$("#spin1").hide();

    
    $("#resetOtpBtn").click(function(){
        var emailAddress = $("#emailInput").val();      
          $.ajax({
              type: 'POST',
              url: "api/v1/user/reset-otp/"+emailAddress,
              success: (result)=>{
                  //do nothing
              },
              error: function (request, exception, errorThrown) {
                //$("#spin1").hide();
                if(request.responseJSON.message){
                  $.toaster({ priority : 'danger', title : 'Error', message : request.responseJSON.message});
                } else {
                  $.toaster({ priority : 'danger', title : 'Error', message : "Something went wrong. Please try again."});
                }
            },
          })
        
      });
});