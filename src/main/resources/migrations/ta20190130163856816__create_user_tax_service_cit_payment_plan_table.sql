CREATE TABLE `user_tax_service_cit_payment_plan` (
  `user_tax_service_id` VARCHAR(45) NOT NULL,
  `revenue_range` VARCHAR(45) NULL,
  `year` VARCHAR(45) NULL,
  PRIMARY KEY (`user_tax_service_id`),
  INDEX `fk_cit_revenue_range_idx` (`revenue_range` ASC),
  INDEX `fk_year_idx` (`year` ASC),
  CONSTRAINT `fk_user_tax_service`
    FOREIGN KEY (`user_tax_service_id`)
    REFERENCES `user_tax_service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
