CREATE TABLE `cit_payment_plan` (
  `revenue_range_id` VARCHAR(45) NOT NULL,
  `year` VARCHAR(45) NOT NULL,
  `fee` VARCHAR(45) NULL,
  PRIMARY KEY (`revenue_range_id`, `year`),
  CONSTRAINT `fk_cit_payment_plan_id_revenue_range_id`
    FOREIGN KEY (`revenue_range_id`)
    REFERENCES `revenue_range` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
