CREATE TABLE `service_charge` (
  `id` VARCHAR(45) NOT NULL,
  `cit_service` DOUBLE NULL,
  `vat_service` DOUBLE NULL,
  `delivery_service` DOUBLE NULL,
  `sub_total` DOUBLE NULL,
  `vat` DOUBLE NULL,
  PRIMARY KEY (`id`));