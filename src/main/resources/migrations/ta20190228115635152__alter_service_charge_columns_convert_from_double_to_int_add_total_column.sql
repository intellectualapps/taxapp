ALTER TABLE `service_charge` 
CHANGE COLUMN `cit_service` `cit_service` INT NULL DEFAULT NULL ,
CHANGE COLUMN `vat_service` `vat_service` INT NULL DEFAULT NULL ,
CHANGE COLUMN `delivery_service` `delivery_service` INT NULL DEFAULT NULL ,
CHANGE COLUMN `sub_total` `sub_total` INT NULL DEFAULT NULL ,
ADD COLUMN `total` DOUBLE NULL DEFAULT NULL AFTER `vat`;