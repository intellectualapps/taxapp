ALTER TABLE `user_tax_service` 
ADD COLUMN `filed_before` ENUM('NEVER', 'OTHER', 'TAX_MASTER', 'NA') NULL DEFAULT 'NA' AFTER `non_cit_payment_plan`,
ADD COLUMN `cit_vat_status_filed_before` ENUM('NEVER', 'OTHER', 'TAX_MASTER', 'NA') NULL DEFAULT 'NA' AFTER `filed_before`,
ADD COLUMN `last_tcc_expiry_date` DATETIME NULL DEFAULT NULL AFTER `cit_vat_status_filed_before`,
ADD COLUMN `last_vat_filed_date` DATETIME NULL DEFAULT NULL AFTER `last_tcc_expiry_date`;