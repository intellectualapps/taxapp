CREATE TABLE `message` (
  `id` VARCHAR(120) NOT NULL,
  `description` VARCHAR(120) NULL,
  `body` VARCHAR(3000) NULL,
  PRIMARY KEY (`id`));