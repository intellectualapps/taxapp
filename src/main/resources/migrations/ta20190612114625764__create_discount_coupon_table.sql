CREATE TABLE `discount_coupon` (
  `discount_code` VARCHAR(45) NOT NULL,
  `percentage` VARCHAR(45) NULL,
  `expiry_date` DATETIME NULL,
  PRIMARY KEY (`discount_code`));