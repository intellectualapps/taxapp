INSERT INTO `industry` (`id`, `description`) VALUES ('indnw171', 'Agriculture');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw172', 'Automobile + Car dealer');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw173', 'Aviation/Shipping');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw174', 'Breweries and Bottling');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw175', 'Building and Construction');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw176', 'Chemicals, Paints and Allied Products');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw177', 'Communication');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw178', 'Electronics');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw179', 'Financial Services');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw180', 'Food and Catering Services');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw181', 'Health Care');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw182', 'Hotel & Hospitality');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw183', 'Information & Communication Technology');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw184', 'Insurance');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw185', 'Legal Services');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw186', 'Mining');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw187', 'Manufacturing');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw188', 'Oil and Gas');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw189', 'Pension Managers and Pension Related Companies');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw190', 'Petro-Chemical, Trading and Refining');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw191', 'Pharmaceuticals, Soaps and Toiletries');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw192', 'Professional Services');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw193', 'Publishing, Printing and Packaging');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw194', 'Real Estate Properties and Investments');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw195', 'Restaurant & Bar');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw196', 'Retail Services');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw197', 'Telecommunication and Internet Service Providers');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw198', 'Textiles and Fashion');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw199', 'Transport and Haulage');
INSERT INTO `industry` (`id`, `description`) VALUES ('indnw200', 'Other');