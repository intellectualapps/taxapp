ALTER TABLE `business` 
ADD COLUMN `state_id` VARCHAR(45) NULL AFTER `revenue_range_id`,
ADD INDEX `business_sate_id_state_id_idx` (`state_id` ASC);

ALTER TABLE `business` 
ADD CONSTRAINT `business_state_id_state_id`
  FOREIGN KEY (`state_id`)
  REFERENCES `state` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
