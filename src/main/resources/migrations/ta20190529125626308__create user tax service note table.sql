CREATE TABLE `user_tax_service_note` (
  `id` VARCHAR(45) NOT NULL,
  `user_tax_service_id` VARCHAR(45) NULL,
  `text` VARCHAR(1000) NULL,
  `username` VARCHAR(45) NULL,
  `date_added` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));