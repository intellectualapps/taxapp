ALTER TABLE `user_tax_service` 
ADD COLUMN `non_cit_payment_plan` VARCHAR(45) NULL AFTER `status`,
ADD INDEX `fk_user_tax_service_pay_plan_idx` (`non_cit_payment_plan` ASC);
ALTER TABLE `user_tax_service` 
ADD CONSTRAINT `fk_user_tax_service_pay_plan`
  FOREIGN KEY (`non_cit_payment_plan`)
  REFERENCES `payment_plan` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;