INSERT INTO `service` (`id`, `description`, `type`) VALUES ('cit', 'Company Income Tax', 'TAX');
INSERT INTO `service` (`id`, `description`, `type`) VALUES ('vat', 'Value Added Tax', 'TAX');
INSERT INTO `service` (`id`, `description`, `type`) VALUES ('pit', 'Personal Income Tax', 'TAX');
INSERT INTO `service` (`id`, `description`, `type`) VALUES ('paye', 'Personal Income Tax', 'TAX');
INSERT INTO `service` (`id`, `description`, `type`) VALUES ('tin', 'Obtain a TIN', 'TAX');