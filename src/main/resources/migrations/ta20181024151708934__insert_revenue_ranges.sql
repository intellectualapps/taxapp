INSERT INTO `revenue_range` (`id`, `description`) VALUES ('a', 'N0 - N999,999');
INSERT INTO `revenue_range` (`id`, `description`) VALUES ('b', 'N1M - N4.99M');
INSERT INTO `revenue_range` (`id`, `description`) VALUES ('c', 'N5M - N19.99M');
INSERT INTO `revenue_range` (`id`, `description`) VALUES ('d', 'N20M - N49.99M');
INSERT INTO `revenue_range` (`id`, `description`) VALUES ('e', 'N50M - N99.99M');
INSERT INTO `revenue_range` (`id`, `description`) VALUES ('f', 'N100M and above');