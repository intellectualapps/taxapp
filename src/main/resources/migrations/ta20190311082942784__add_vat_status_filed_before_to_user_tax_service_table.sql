ALTER TABLE `user_tax_service` 
ADD COLUMN `vat_status_filed_before` VARCHAR(45) NULL DEFAULT NULL AFTER `last_vat_filed_date`;