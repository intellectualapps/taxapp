INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp1', 'cit', 'Annually', '50000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp2', 'vat', 'Annually', '49000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp3', 'vat', 'Monthly', '5000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp4', 'vat', 'Quarterly', '13000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp5', 'vat', 'Bi-annually', '25000');

INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp6', 'pit', 'Annually', '49000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp7', 'pit', 'Monthly', '5000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp8', 'pit', 'Quarterly', '13000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp9', 'pit', 'Bi-annually', '25000');

INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp10', 'paye', 'Annually', '49000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp11', 'paye', 'Monthly', '5000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp12', 'paye', 'Quarterly', '13000');
INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp13', 'paye', 'Bi-annually', '25000');

INSERT INTO `payment_plan` (`id`, `service_id`, `frequency`, `amount`) VALUES ('pp14', 'tin', 'One-off', '10000');
