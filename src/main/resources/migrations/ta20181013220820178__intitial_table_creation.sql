

CREATE TABLE `user` (
  `username` varchar(100) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `photo_url` varchar(2000) DEFAULT NULL,
  `creation_date` date NOT NULL,
  `facebook_email` varchar(255) DEFAULT NULL,
  `twitter_email` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `verified` int(2) DEFAULT NULL,
  `mem_hash` varchar(255) DEFAULT NULL,
  `modification_date` date DEFAULT NULL,
  `referrer` varchar(255) DEFAULT NULL,
  `oauth_uid` varchar(100) DEFAULT NULL,
  `oauth_provider` varchar(100) DEFAULT NULL,
  `twitter_id` varchar(100) DEFAULT NULL,
  `facebook_id` varchar(100) DEFAULT NULL,
  `validated` int(2) DEFAULT NULL,
  `optout` int(2) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ;

CREATE TABLE `otp` (
  `username` VARCHAR(100) NOT NULL,
  `otp` VARCHAR(45) NULL,
  `date_created` DATETIME NULL,
  PRIMARY KEY (`username`),
  CONSTRAINT `otp_username_user_username`
    FOREIGN KEY (`username`)
    REFERENCES `user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    CREATE TABLE `service` (
  `id` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  `type` ENUM('TAX', 'INCORPORATION') NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `payment_plan` (
  `id` VARCHAR(45) NOT NULL,
  `service_id` VARCHAR(45) NULL,
  `frequency` VARCHAR(45) NULL,
  `amount` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `payment_plan_service_id_service_id_idx` (`service_id` ASC),
  CONSTRAINT `payment_plan_service_id_service_id`
    FOREIGN KEY (`service_id`)
    REFERENCES `service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
 CREATE TABLE `business_type` (
  `id` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `industry` (
  `id` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `incorporation_request` (
  `request_id` VARCHAR(45) NOT NULL,
  `username` VARCHAR(100) NULL,
  `proposed_name_1` VARCHAR(100) NULL,
  `proposed_name_2` VARCHAR(100) NULL,
  `proposed_name_3` VARCHAR(100) NULL,
  `proposed_name_4` VARCHAR(100) NULL,
  `proposed_address` VARCHAR(500) NULL,
  `service_id` VARCHAR(45) NULL,
  `industry_id` VARCHAR(45) NULL,
  `request_date` DATETIME NULL,
  `document_urls` VARCHAR(1000) NULL,
  `status` ENUM('PENDING', 'PROCESSING', 'DONE', 'HOLD') NULL,
  PRIMARY KEY (`request_id`),
  INDEX `ir_username_user_username_idx` (`username` ASC),
  INDEX `ir_service_id_service_id_idx` (`service_id` ASC),
  INDEX `ir_industry_id_industry_id_idx` (`industry_id` ASC),
  CONSTRAINT `ir_username_user_username`
    FOREIGN KEY (`username`)
    REFERENCES `user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ir_service_id_service_id`
    FOREIGN KEY (`service_id`)
    REFERENCES `service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ir_industry_id_industry_id`
    FOREIGN KEY (`industry_id`)
    REFERENCES `industry` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    CREATE TABLE `revenue_range` (
  `id` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `business` (
  `id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `address` VARCHAR(500) NULL,
  `type_id` VARCHAR(45) NULL,
  `industry_id` VARCHAR(45) NULL,
  `incorporation_date` VARCHAR(45) NULL,
  `tin` VARCHAR(45) NULL,
  `commencement_date` DATETIME NULL,
  `revenue_range_id` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `business_type_id_idx` (`type_id` ASC),
  INDEX `business_industry_id_industry_id_idx` (`industry_id` ASC),
  INDEX `business_revenue_range_revenue_range_idx` (`revenue_range_id` ASC),
  CONSTRAINT `business_type_id_business_type_d`
    FOREIGN KEY (`type_id`)
    REFERENCES `business_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `business_industry_id_industry_id`
    FOREIGN KEY (`industry_id`)
    REFERENCES `industry` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `business_revenue_range_revenue_range`
    FOREIGN KEY (`revenue_range_id`)
    REFERENCES `revenue_range` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    CREATE TABLE `user_tax_service` (
  `id` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) NULL,
  `business_id` VARCHAR(45) NULL,
  `service_id` VARCHAR(45) NULL,
  `date_requested` DATETIME NULL,
  `accounting_year` INT NULL,
  `from_date` DATETIME NULL,
  `to_date` DATETIME NULL,
  `document_urls` VARCHAR(1000) NULL,
  `status` ENUM('PENDING', 'PROCESSING', 'DONE', 'HOLD') NULL,
  PRIMARY KEY (`id`),
  INDEX `uts_username_user_username_idx` (`username` ASC),
  INDEX `uts_service_id_service_id_idx` (`service_id` ASC),
  INDEX `uts_business_id_business_id_idx` (`business_id` ASC),
  CONSTRAINT `uts_username_user_username`
    FOREIGN KEY (`username`)
    REFERENCES `user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `uts_business_id_business_id`
    FOREIGN KEY (`business_id`)
    REFERENCES `business` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `uts_service_id_service_id`
    FOREIGN KEY (`service_id`)
    REFERENCES `service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    CREATE TABLE `user_business` (
  `username` VARCHAR(45) NOT NULL,
  `business_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`username`, `business_id`),
  INDEX `user_business_business_id_business_id_idx` (`business_id` ASC),
  CONSTRAINT `user_business_username_user_username`
    FOREIGN KEY (`username`)
    REFERENCES `user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `user_business_business_id_business_id`
    FOREIGN KEY (`business_id`)
    REFERENCES `business` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);






