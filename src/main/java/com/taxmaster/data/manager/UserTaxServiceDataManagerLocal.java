/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.UserTaxService;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface UserTaxServiceDataManagerLocal {
    
    UserTaxService create(UserTaxService userTaxService) throws EJBTransactionRolledbackException;

    UserTaxService update(UserTaxService userTaxService);

    void delete(UserTaxService userTaxService);
    
    List<UserTaxService> getUserTaxServicesByUsername(String username);  

    List<UserTaxService> getAllTaxServicesExcludingCompletedStatus();      

    UserTaxService get(String id);
    
    List<UserTaxService> searchUserTaxServices(String username, String serviceId, String status);
    
}
