/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.Service;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author RAYNOLD
 */

@Local
public interface ServiceDataManagerLocal {
    
    List<Service> getServices(String serviceType);
    
    List<Service> getAll();

    Service getService(String serviceId);
}
