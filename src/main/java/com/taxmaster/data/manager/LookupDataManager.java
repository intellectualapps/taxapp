/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.LegalEntityType;
import com.taxmaster.model.CompanyIncomeTaxFilingPeriod;
import com.taxmaster.model.ContactFormSubject;
import com.taxmaster.model.Industry;
import com.taxmaster.model.RevenueRange;
import com.taxmaster.model.Service;
import com.taxmaster.model.State;
import com.taxmaster.model.User;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class LookupDataManager implements LookupDataManagerLocal {

    @EJB
    private DataProviderLocal crud;

    @Override
    public List<Industry> getIndustries() {
        return crud.findAll(Industry.class);
    }

    @Override
    public List<LegalEntityType> getLegalEntityTypes() {
        return crud.findAll(LegalEntityType.class);
    }

    @Override
    public List<RevenueRange> getRevenueRanges() {
        return crud.findAll(RevenueRange.class);
    }

    @Override
    public List<CompanyIncomeTaxFilingPeriod> getCompanyIncomeTaxFilingPeriod() {
        return crud.findAll(CompanyIncomeTaxFilingPeriod.class);
    }

    @Override
    public List<State> getAllStates() {
        return crud.findAll(State.class);
    }

    @Override
    public List<ContactFormSubject> getAllSubjects() {
        return crud.findAll(ContactFormSubject.class);
    }

    @Override
    public Industry getIndustry(String id) {
        return crud.find(id, Industry.class);
    }

    @Override
    public RevenueRange getRevenueRange(String id) {
        return crud.find(id, RevenueRange.class);
    }

    @Override
    public State getState(String id) {
        return crud.find(id, State.class);
    }

    @Override
    public LegalEntityType getLegalEntityType(String id) {
        return crud.find(id, LegalEntityType.class);
    }

    @Override
    public List<Service> getAllServices() {
        return crud.findAll(Service.class);
    }

    @Override
    public List<User> getUsersByRole(String role) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("role", role);

        return crud.findByNamedQuery("User.findByRole", parameters, User.class);

    }

}
