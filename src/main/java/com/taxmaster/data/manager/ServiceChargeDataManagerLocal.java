/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.Otp;
import com.taxmaster.model.ServiceCharge;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface ServiceChargeDataManagerLocal {
    
    ServiceCharge create(ServiceCharge serviceCharge) throws EJBTransactionRolledbackException;
    
    ServiceCharge get(String id);

    ServiceCharge update(ServiceCharge serviceCharge);
    
}
