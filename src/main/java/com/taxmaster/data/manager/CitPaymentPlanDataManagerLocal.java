/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.CitPaymentPlan;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

/**
 *
 * @author RAYNOLD
 */
@Local
public interface CitPaymentPlanDataManagerLocal {
    
    List<CitPaymentPlan> get(String rangeId, String year) throws EJBTransactionRolledbackException;
    
    CitPaymentPlan create(CitPaymentPlan citPaymentPlan) throws EJBTransactionRolledbackException;
    
}
