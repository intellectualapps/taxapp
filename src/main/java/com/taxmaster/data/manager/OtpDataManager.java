/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.Otp;
import com.taxmaster.model.User;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class OtpDataManager implements OtpDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public Otp create(Otp otp) throws EJBTransactionRolledbackException {
        return crud.create(otp);
    }
   
    @Override
    public Otp get(String username) {
        return crud.find(username, Otp.class);
    }

    @Override
    public void delete(Otp otp) {
        crud.delete(otp);
    }
     
}

