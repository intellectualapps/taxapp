/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.IncorporationRequest;
import com.taxmaster.model.UserLegalEntity;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface UserLegalEntityDataManagerLocal {
    
    UserLegalEntity create(UserLegalEntity user) throws EJBTransactionRolledbackException;

    UserLegalEntity update(UserLegalEntity user);

    void delete(UserLegalEntity user);
    
    List<UserLegalEntity> getUserLegalEntitiesByUsername(String username);
    
    List<IncorporationRequest> getUserIncorporationRequestsByUsername(String username);

    List<UserLegalEntity> getUserLegalEntitiesByEntity(String entityId);
    
}
