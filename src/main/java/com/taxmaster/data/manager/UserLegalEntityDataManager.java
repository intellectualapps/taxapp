/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.IncorporationRequest;
import com.taxmaster.model.UserLegalEntity;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class UserLegalEntityDataManager implements UserLegalEntityDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public UserLegalEntity create(UserLegalEntity userLegalEntity) throws EJBTransactionRolledbackException {
        return crud.create(userLegalEntity);
    }

    @Override
    public UserLegalEntity update(UserLegalEntity userLegalEntity) {
        return crud.update(userLegalEntity);
    }

    @Override
    public void delete(UserLegalEntity userLegalEntity) {
        crud.delete(userLegalEntity);
    }

    @Override
    public List<UserLegalEntity> getUserLegalEntitiesByUsername(String username) {
        Map<String, Object> parameters = new HashMap<String, Object>();            
        parameters.put("username", username);
                
        List<UserLegalEntity> userLegalEntities = crud.findByNamedQuery("UserLegalEntity.findByUsername", parameters, UserLegalEntity.class);
        
        return userLegalEntities;
    }

    @Override
    public List<IncorporationRequest> getUserIncorporationRequestsByUsername(String username) {
        Map<String, Object> parameters = new HashMap<String, Object>();            
        parameters.put("username", username);
                
        List<IncorporationRequest> userIncorporationRequests = crud.findByNamedQuery("IncorporationRequest.findByUsername", parameters, IncorporationRequest.class);
        
        return userIncorporationRequests;
    }

    @Override
    public List<UserLegalEntity> getUserLegalEntitiesByEntity(String entityId) {
        Map<String, Object> parameters = new HashMap<String, Object>();            
        parameters.put("entityId", entityId);
                
        List<UserLegalEntity> userLegalEntities = crud.findByNamedQuery("UserLegalEntity.findByEntity", parameters, UserLegalEntity.class);
        System.out.println("USER LEGAL SIZE " + userLegalEntities.size() + "ID " + entityId);
        
        return userLegalEntities;
    }
   
}

