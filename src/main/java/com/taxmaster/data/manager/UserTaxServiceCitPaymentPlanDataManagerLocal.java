/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.UserTaxServiceCitPaymentPlan;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface UserTaxServiceCitPaymentPlanDataManagerLocal {
    
    UserTaxServiceCitPaymentPlan create(UserTaxServiceCitPaymentPlan userTaxServiceCitPaymentPlan) throws EJBTransactionRolledbackException;       
    
    UserTaxServiceCitPaymentPlan get(String userTaxServiceId) throws EJBTransactionRolledbackException;       
    
}
