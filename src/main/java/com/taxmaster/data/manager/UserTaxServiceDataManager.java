/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.UserTaxService;
import com.taxmaster.util.ServiceStatusType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class UserTaxServiceDataManager implements UserTaxServiceDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    
    

    @Override
    public UserTaxService create(UserTaxService userTaxService) throws EJBTransactionRolledbackException {
        return crud.create(userTaxService);
    }

    @Override
    public UserTaxService get(String id) throws EJBTransactionRolledbackException {
        return crud.find(id, UserTaxService.class);
    }
    
    @Override
    public UserTaxService update(UserTaxService userTaxService) {
        return crud.update(userTaxService);
    }

    @Override
    public void delete(UserTaxService userTaxService) {
        crud.delete(userTaxService);
    }

    @Override
    public List<UserTaxService> getUserTaxServicesByUsername(String username) {
        Map<String, Object> parameters = new HashMap<String, Object>();            
        parameters.put("username", username);
                
        List<UserTaxService> userTaxServices = crud.findByNamedQuery("UserTaxService.findByUsername", parameters, UserTaxService.class);
        
        return userTaxServices;
    }

    @Override
    public List<UserTaxService> getAllTaxServicesExcludingCompletedStatus() {        
        Map<String, Object> parameters = new HashMap<String, Object>();            
        parameters.put("status", ServiceStatusType.COMPLETED.getDescription());
                
        List<UserTaxService> userTaxServices = crud.findByNamedQuery("UserTaxService.findAllExcludingCompletedStatus", parameters, UserTaxService.class);
        
        return userTaxServices;
    }

    @Override
    public List<UserTaxService> searchUserTaxServices(String username, String serviceId, String status) {        
        Map<String, Object> parameters = new HashMap<String, Object>();                    
        parameters.put("serviceId", serviceId);
        parameters.put("status",  status);
                
        List<UserTaxService> userTaxServices = new ArrayList<UserTaxService>();
        
        if (username != null && !username.isEmpty() && serviceId != null && !serviceId.isEmpty() &&
                status != null && !status.isEmpty()) {
            parameters.put("username", "%" + username + "%");
            userTaxServices = crud.findByNamedQuery("UserTaxService.searchByUsernameAndServiceAndStatus", parameters, UserTaxService.class);
        } else if ((username == null || username.isEmpty()) && serviceId != null && !serviceId.isEmpty() &&
                status != null && !status.isEmpty()) { 
            userTaxServices = crud.findByNamedQuery("UserTaxService.searchByServiceAndStatus", parameters, UserTaxService.class);
        } else {
            parameters.put("username", "%" + username + "%");
            userTaxServices = crud.findByNamedQuery("UserTaxService.searchByUsernameOrServiceOrStatus", parameters, UserTaxService.class);
        }
        
        return userTaxServices;
    }
        
}

