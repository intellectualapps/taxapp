/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.UserTaxServiceNote;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class UserTaxServiceNoteDataManager implements UserTaxServiceNoteDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    
    

    @Override
    public UserTaxServiceNote create(UserTaxServiceNote userTaxServiceNote) throws EJBTransactionRolledbackException {
        return crud.create(userTaxServiceNote);
    }

    @Override
    public UserTaxServiceNote get(String id) throws EJBTransactionRolledbackException {
        return crud.find(id, UserTaxServiceNote.class);
    }
    
    @Override
    public UserTaxServiceNote update(UserTaxServiceNote userTaxServiceNote) {
        return crud.update(userTaxServiceNote);
    }

    @Override
    public void delete(UserTaxServiceNote userTaxServiceNote) {
        crud.delete(userTaxServiceNote);
    }

    @Override
    public List<UserTaxServiceNote> getUserTaxServiceNotesByUserTaxServiceId(String userTaxServiceId) {
        Map<String, Object> parameters = new HashMap<String, Object>();            
        parameters.put("userTaxServiceId", userTaxServiceId);
                
        List<UserTaxServiceNote> userTaxServiceNotes = crud.findByNamedQuery("UserTaxServiceNote.findByUserTaxServiceId", parameters, UserTaxServiceNote.class);
        
        return userTaxServiceNotes;
    }
        
}

