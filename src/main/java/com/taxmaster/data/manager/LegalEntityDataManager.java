/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.LegalEntity;
import com.taxmaster.model.IncorporationRequest;
import com.taxmaster.util.ServiceStatusType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class LegalEntityDataManager implements LegalEntityDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public LegalEntity create(LegalEntity legalEntity) throws EJBTransactionRolledbackException {
        return crud.create(legalEntity);
    }

    @Override
    public LegalEntity update(LegalEntity legalEntity) {
        return crud.update(legalEntity);
    }

    @Override
    public LegalEntity get(String legalEntityId) {
        return crud.find(legalEntityId, LegalEntity.class);
    }

    @Override
    public void delete(LegalEntity user) {
        crud.delete(user);
    }

    @Override
    public IncorporationRequest createIncorporationRequest(IncorporationRequest incorporationRequest) throws EJBTransactionRolledbackException {
        return crud.create(incorporationRequest);
    }

    @Override
    public List<IncorporationRequest> getAllIncorporationRequestsExcludingCompletedStatus() {
        Map<String, Object> parameters = new HashMap<String, Object>();            
        parameters.put("status", ServiceStatusType.COMPLETED.getDescription());
                
        List<IncorporationRequest> incorporationRequests = crud.findByNamedQuery("IncorporationRequest.findAllExcludingCompletedStatus", parameters, IncorporationRequest.class);
        
        return incorporationRequests;
    }

    @Override
    public List<IncorporationRequest> getIncorporationRequestsByUsername(String username) {
        Map<String, Object> parameters = new HashMap<String, Object>();            
        parameters.put("username", username);
                
        List<IncorporationRequest> incorporationRequests = crud.findByNamedQuery("IncorporationRequest.findByUsername", parameters, IncorporationRequest.class);
        
        return incorporationRequests;
    }

    @Override
    public IncorporationRequest getIncorporationRequest(String id) {
        return crud.find(id, IncorporationRequest.class);
    }

    @Override
    public IncorporationRequest updateIncorporationRequest(IncorporationRequest incorporationRequest) {
        return crud.update(incorporationRequest);
    }

    @Override
    public List<LegalEntity> getByLegalEntityName(String searchTerm) {
        Map<String, Object> parameters = new HashMap<String, Object>();            
        parameters.put("name", "%" + searchTerm + "%");
                
        List<LegalEntity> legalEntities = crud.findByNamedQuery("LegalEntity.findByLegalEntityName", parameters, LegalEntity.class);
        System.out.println("LEGAL ENTITY SIZE " + legalEntities.size());
        
        return legalEntities;        
    }
    
}

