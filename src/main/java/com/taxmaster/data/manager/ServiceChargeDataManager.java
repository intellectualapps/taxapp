/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.ServiceCharge;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class ServiceChargeDataManager implements ServiceChargeDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public ServiceCharge create(ServiceCharge serviceCharge) throws EJBTransactionRolledbackException {
        return crud.create(serviceCharge);
    }

    @Override
    public ServiceCharge get(String id) {
        return crud.find(id, ServiceCharge.class);
    }

    @Override
    public ServiceCharge update(ServiceCharge serviceCharge) {
        return crud.update(serviceCharge);
    }
     
}

