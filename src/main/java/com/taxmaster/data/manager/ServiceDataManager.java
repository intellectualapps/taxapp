/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.Service;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author RAYNOLD
 */
@Stateless
public class ServiceDataManager implements ServiceDataManagerLocal {

    @EJB
    private DataProviderLocal crud;
    
    @Override
    public List<Service> getServices(String categoryType) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("categoryType", categoryType);
        
        return crud.findByNamedQuery("Service.findByServiceCategoryType", parameters, Service.class);
    }

    @Override
    public List<Service> getAll() {
        return crud.findAll(Service.class);
    }

    @Override
    public Service getService(String serviceId) {
        return crud.find(serviceId, Service.class);
    }
    
}
