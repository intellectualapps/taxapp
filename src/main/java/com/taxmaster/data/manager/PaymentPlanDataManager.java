/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.PaymentPlan;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class PaymentPlanDataManager implements PaymentPlanDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public List<PaymentPlan> get(String serviceId) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("serviceId", serviceId);        
        return crud.findByNamedQuery("PaymentPlan.findByServiceId",
                parameters, PaymentPlan.class);
    }
}

