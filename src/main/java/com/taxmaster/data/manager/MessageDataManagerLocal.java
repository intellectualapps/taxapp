/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.Message;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Badmus
 */
@Local
public interface MessageDataManagerLocal {
    
    Message create(Message messageDetails);

    Message update(Message messageDetails);

    Message get(String messageDetailsId);

    void delete(Message messageDetails);

    List<Message> getAllMessagesDetails();

}