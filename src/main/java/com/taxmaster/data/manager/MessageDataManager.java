package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.Message;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless
public class MessageDataManager implements MessageDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public Message create(Message messageDetails) {
        return crud.create(messageDetails);
    }

    @Override
    public Message update(Message messageDetails) {
        return crud.update(messageDetails);
    }

    @Override
    public Message get(String messageDetailsId) {
        return crud.find(messageDetailsId, Message.class);
    }

    @Override
    public void delete(Message messageDetails) {
        crud.delete(messageDetails);
    }

    @Override
    public List<Message> getAllMessagesDetails() {
        return crud.findAll(Message.class);
    }

}

