/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.PaymentPlan;
import java.util.List;
import javax.ejb.Local;

@Local
public interface PaymentPlanDataManagerLocal {

    List<PaymentPlan> get(String serviceId);
    
}
