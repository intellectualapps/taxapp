/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.UserTaxServiceCitPaymentPlan;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class UserTaxServiceCitPaymentPlanDataManager implements UserTaxServiceCitPaymentPlanDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public UserTaxServiceCitPaymentPlan create(UserTaxServiceCitPaymentPlan userTaxServiceCitPaymentPlan) throws EJBTransactionRolledbackException {
        return crud.create(userTaxServiceCitPaymentPlan);
    }

    @Override
    public UserTaxServiceCitPaymentPlan get(String userTaxServiceId) throws EJBTransactionRolledbackException {
        return crud.find(userTaxServiceId, UserTaxServiceCitPaymentPlan.class);
    }
    
}

