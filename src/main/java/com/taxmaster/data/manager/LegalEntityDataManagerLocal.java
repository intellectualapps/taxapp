/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.LegalEntity;
import com.taxmaster.model.IncorporationRequest;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface LegalEntityDataManagerLocal {
    
    LegalEntity create(LegalEntity legalEntity) throws EJBTransactionRolledbackException;

    LegalEntity update(LegalEntity legalEntity);

    LegalEntity get(String legalEntityId);

    void delete(LegalEntity legalEntity);
    
    IncorporationRequest createIncorporationRequest(IncorporationRequest incorporationRequest) throws EJBTransactionRolledbackException;
    
    List<IncorporationRequest> getAllIncorporationRequestsExcludingCompletedStatus();

    List<IncorporationRequest> getIncorporationRequestsByUsername(String username);
    
    IncorporationRequest getIncorporationRequest(String id);

    IncorporationRequest updateIncorporationRequest(IncorporationRequest incorporationRequest);

    List<LegalEntity> getByLegalEntityName(String searchTerm);

}
