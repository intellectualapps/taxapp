/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.data.provider.DataProviderLocal;
import com.taxmaster.model.CitPaymentPlan;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author RAYNOLD
 */
@Stateless
public class CitPaymentPlanDataManager implements CitPaymentPlanDataManagerLocal {
    
    @EJB
    private DataProviderLocal crud;    

    @Override
    public List<CitPaymentPlan> get(String revenueRangeId, String year) throws EJBTransactionRolledbackException{
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("revenueRangeId", revenueRangeId);
        parameters.put("year", year);
        return crud.findByNamedQuery("CitPaymentPlan.findByRevenueRangeIdAndYear",
                parameters, CitPaymentPlan.class);
    }

    @Override
    public CitPaymentPlan create(CitPaymentPlan citPaymentPlan) throws EJBTransactionRolledbackException {
        return crud.create(citPaymentPlan);
    }
}
