/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.data.manager;

import com.taxmaster.model.UserTaxServiceNote;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface UserTaxServiceNoteDataManagerLocal {
    
    UserTaxServiceNote create(UserTaxServiceNote userTaxServiceNote) throws EJBTransactionRolledbackException;

    UserTaxServiceNote update(UserTaxServiceNote userTaxServiceNote);

    void delete(UserTaxServiceNote userTaxServiceNote);
    
    List<UserTaxServiceNote> getUserTaxServiceNotesByUserTaxServiceId(String userTaxServiceId);      

    UserTaxServiceNote get(String id);
    
}
