/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.config;

import com.taxmaster.service.AdminService;
import com.taxmaster.service.LegalEntityService;
import com.taxmaster.service.LookupService;
import com.taxmaster.service.PaymentPlanService;
import com.taxmaster.service.ServiceRequestService;
import com.taxmaster.service.UserService;
import com.taxmaster.util.exception.GeneralAppExceptionMapper;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Lateefah
 */
@javax.ws.rs.ApplicationPath("/api")

public class ApplicationConfig extends Application{
    @Override    
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        
        s.add(UserService.class);
        s.add(LegalEntityService.class);
        s.add(LookupService.class);
        s.add(PaymentPlanService.class);
        s.add(ServiceRequestService.class);
        s.add(AdminService.class);
        
        s.add(GeneralAppExceptionMapper.class);
        
        return s;
    }
}
