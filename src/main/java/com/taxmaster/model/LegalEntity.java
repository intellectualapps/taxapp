/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "entity")
@XmlRootElement
@NamedQueries({  
  @NamedQuery(name = "LegalEntity.findByLegalEntityName", query = "SELECT le FROM LegalEntity le WHERE le.name LIKE :name")})


public class LegalEntity implements Serializable {
    @Id
    @NotNull
    @Column(name = "id")
    private String id;
            
    @Column(name = "name")
    private String name;
        
    @Column(name = "address")
    private String address;
       
    @Column(name = "type_id")
    private String typeId;
       
    @Column(name = "industry_id")
    private String industryId;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "incorporation_date")
    private Date incorporationDate;
        
    @Column(name = "tin")
    private String tin;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "commencement_date")
    private Date commencementDate;                        
        
    @Column(name = "revenue_range_id")
    private String revenueRangeId;
    
    @Column(name = "state_id")
    private String stateId;
    
    @Column(name = "address_line_1")
    private String addressLine1;
    
    @Column(name = "address_line_2")
    private String addressLine2;
    
    @Column(name = "city")
    private String city;
   
    public LegalEntity() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public Date getIncorporationDate() {
        return incorporationDate;
    }

    public void setIncorporationDate(Date incorporationDate) {
        this.incorporationDate = incorporationDate;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public Date getCommencementDate() {
        return commencementDate;
    }

    public void setCommencementDate(Date commencementDate) {
        this.commencementDate = commencementDate;
    }

    public String getRevenueRangeId() {
        return revenueRangeId;
    }

    public void setRevenueRangeId(String revenueRangeId) {
        this.revenueRangeId = revenueRangeId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }    
            
}
