/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RAYNOLD
 */

@Entity
@Table(name = "state")
@XmlRootElement

public class State implements Serializable {
    
    @Id
    @NotNull
    @Column(name = "id")
    private String id;
            
    @Column(name = "name")
    private String name;
    
    public State() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return name;
    }

    public void setState(String name) {
        this.name = name;
    }
}
