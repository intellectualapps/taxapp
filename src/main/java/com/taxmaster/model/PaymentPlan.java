/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "payment_plan")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentPlan.findByServiceId", query = "SELECT pp FROM PaymentPlan pp WHERE pp.serviceId = :serviceId")
})

public class PaymentPlan implements Serializable {
    @Id
    @NotNull
    @Column(name = "id")
    private String id;
            
    @Column(name = "service_id")
    private String serviceId;
        
    @Column(name = "frequency")
    private String frequency;
       
    @Column(name = "amount")
    private int amount;
   
    public PaymentPlan() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
        
}
