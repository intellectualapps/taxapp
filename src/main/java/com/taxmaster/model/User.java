/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "user")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email "),
  @NamedQuery(name = "User.findByFacebookEmail", query = "SELECT u FROM User u WHERE u.facebookEmail = :email "),
  @NamedQuery(name = "User.findByTwitterEmail", query = "SELECT u FROM User u WHERE u.twitterEmail = :email "),
  @NamedQuery(name = "User.findByUserHash", query = "SELECT u FROM User u WHERE u.hash = :hash "),
  @NamedQuery(name = "User.findByRole", query = "SELECT u FROM User u WHERE u.role = :role "),
  @NamedQuery(name = "User.findByUsernameFirstAndLastName", query = "SELECT u FROM User u WHERE u.username LIKE :username OR u.firstName LIKE :firstName OR u.lastName LIKE :lastName"),
  @NamedQuery(name = "User.findByUsernameFirstNameAndLastNameAndPhoneNumber", query = "SELECT u FROM User u WHERE u.username LIKE :username OR u.firstName LIKE :firstName OR u.lastName LIKE :lastName OR u.phoneNumber LIKE :phoneNumber")})

public class User implements Serializable {
    @Id
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "username")
    private String username;
    
        
    @Column(name = "email")
    private String email;
        
    @Column(name = "first_name")
    private String firstName;
       
    @Column(name = "last_name")
    private String lastName;
       
    @Column(name = "phone_number")
    private String phoneNumber;
        
    @Column(name = "password")
    private String password;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "creation_date")
    private Date creationDate;
        
    @Column(name = "photo_url")
    private String photoUrl;
                        
        
    @Column(name = "facebook_email")
    private String facebookEmail;
        
    @Column(name = "twitter_email")
    private String twitterEmail;
    
        
    @Column(name = "location")
    private String location;
    
    @Column(name = "mem_hash")
    private String hash;
    
    @Column(name = "verified")
    private Integer verified;
    
    @Column(name = "validated")
    private Integer validated;
    
    @Column(name = "role")
    private String role;
        
    @Temporal(TemporalType.DATE)
    @Column(name = "birthday")
    private Date birthday;
    
    public User() { }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber () {
        return phoneNumber;
    }
    
    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Date getCreationDate() {
        return creationDate;
    }
    
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
        
    public String getPhotoUrl() {
        return photoUrl;
    }
    
    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
    
    public String getFacebookEmail() {
        return facebookEmail;
    }
    
    public void setFacebookEmail(String facebookEmail) {
        this.facebookEmail = facebookEmail;
    }
    
    public String getTwitterEmail() {
        return twitterEmail;
    }
    
    public void setTwitterEmail(String twitterEmail) {
        this.twitterEmail = twitterEmail;
    }
    
    public String getLocation() {
        return location;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }    

    public Integer getVerified() {
        return verified;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }        

    public Integer getValidated() {
        return validated;
    }

    public void setValidated(Integer validated) {
        this.validated = validated;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }    

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
        
}
