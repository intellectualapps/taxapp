/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "service_charge")
@XmlRootElement
public class ServiceCharge implements Serializable {
    @Id
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "id")
    private String id;    
        
    @Column(name = "cit_service")
    private Integer citService;
    
    @Column(name = "vat_service")
    private Integer vatService;
        
    @Column(name = "delivery_service")
    private Integer deliveryService;
    
    @Column(name = "sub_total")
    private Integer subTotal;
    
    @Column(name = "vat")
    private Double vat;
        
    @Column(name = "total")
    private Double total;
    
    @Column(name = "discount_code")
    private String discountCode;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "discount_date")
    private Date discountDate;
    
    @Column(name = "discount_amount")
    private Double discountAmount;
    
    public ServiceCharge() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCitService() {
        return citService;
    }

    public void setCitService(Integer citService) {
        this.citService = citService;
    }

    public Integer getVatService() {
        return vatService;
    }

    public void setVatService(Integer vatService) {
        this.vatService = vatService;
    }

    public Integer getDeliveryService() {
        return deliveryService;
    }

    public void setDeliveryService(Integer deliveryService) {
        this.deliveryService = deliveryService;
    }

    public Double getVat() {
        return vat;
    }

    public void setVat(Double vat) {
        this.vat = vat;
    }

    public Integer getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Integer subTotal) {
        this.subTotal = subTotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    } 

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public Date getDiscountDate() {
        return discountDate;
    }

    public void setDiscountDate(Date discountDate) {
        this.discountDate = discountDate;
    }    

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }
   
}
