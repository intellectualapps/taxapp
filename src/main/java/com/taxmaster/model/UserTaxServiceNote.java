/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "user_tax_service_note")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserTaxServiceNote.findByUserTaxServiceId", query = "SELECT utsn FROM UserTaxServiceNote utsn WHERE utsn.userTaxServiceId = :userTaxServiceId ORDER BY utsn.dateAdded ASC")
})

public class UserTaxServiceNote implements Serializable {
    @Id
    @NotNull
    @Column(name = "id")
    private String id;
    
    @Column(name = "user_tax_service_id")
    private String userTaxServiceId;
    
    @Column(name = "text")
    private String text;
    
    @Column(name = "username")
    private String username;
            
    @Column(name = "date_added")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAdded;
    
    public UserTaxServiceNote() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserTaxServiceId() {
        return userTaxServiceId;
    }

    public void setUserTaxServiceId(String userTaxServiceId) {
        this.userTaxServiceId = userTaxServiceId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }        
    
}
