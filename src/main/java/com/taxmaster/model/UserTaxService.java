/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "user_tax_service")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserTaxService.findByUsername", query = "SELECT uts FROM UserTaxService uts WHERE uts.username = :username"),
    @NamedQuery(name = "UserTaxService.findAllExcludingCompletedStatus", query = "SELECT uts FROM UserTaxService uts WHERE uts.status <> :status"),
    @NamedQuery(name = "UserTaxService.searchByUsernameOrServiceOrStatus", query = "SELECT uts FROM UserTaxService uts WHERE uts.username LIKE :username OR uts.serviceId = :serviceId OR uts.status = :status"),
    @NamedQuery(name = "UserTaxService.searchByServiceAndStatus", query = "SELECT uts FROM UserTaxService uts WHERE uts.serviceId = :serviceId AND uts.status = :status"),
    @NamedQuery(name = "UserTaxService.searchByUsernameAndServiceAndStatus", query = "SELECT uts FROM UserTaxService uts WHERE uts.username LIKE :username AND uts.serviceId = :serviceId AND uts.status = :status")
})

public class UserTaxService implements Serializable {
    @Id
    @NotNull
    @Column(name = "id")
    private String id;
    
    @Column(name = "username")
    private String username;
            
    @Column(name = "business_id")
    private String businessId;
    
    @Column(name = "service_id")
    private String serviceId;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "date_requested")
    private Date dateRequested;
    
    @Column(name = "accounting_year")
    private Integer accountingYear;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "from_date")
    private Date fromDate;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "to_date")
    private Date toDate;
    
    @Column(name = "document_urls")
    private String documentUrls;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "non_cit_payment_plan")
    private String nonCitPaymentPlan;
    
    @Column(name = "cit_filed_before")
    private String citFiledBeforeStatus;
    
    @Column(name = "cit_vat_status_filed_before")
    private String citVatFiledBeforeStatus;
    
    @Column(name = "vat_status_filed_before")
    private String vatFiledBeforeStatus;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "last_tcc_expiry_date")
    private Date tccExpiryYear;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "last_vat_filed_date")
    private Date lastVatFiledDate;
    
    @Column(name = "assigned_to")
    private String assignedTo;
    
    public UserTaxService() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public Integer getAccountingYear() {
        return accountingYear;
    }

    public void setAccountingYear(Integer accountingYear) {
        this.accountingYear = accountingYear;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getDocumentUrls() {
        return documentUrls;
    }

    public void setDocumentUrls(String documentUrls) {
        this.documentUrls = documentUrls;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }        

    public String getNonCitPaymentPlan() {
        return nonCitPaymentPlan;
    }

    public void setNonCitPaymentPlan(String nonCitPaymentPlan) {
        this.nonCitPaymentPlan = nonCitPaymentPlan;
    }

    public String getCitFiledBeforeStatus() {
        return citFiledBeforeStatus;
    }

    public void setCitFiledBeforeStatus(String citFiledBeforeStatus) {
        this.citFiledBeforeStatus = citFiledBeforeStatus;
    }

    public String getCitVatFiledBeforeStatus() {
        return citVatFiledBeforeStatus;
    }

    public void setCitVatFiledBeforeStatus(String citVatFiledBeforeStatus) {
        this.citVatFiledBeforeStatus = citVatFiledBeforeStatus;
    }

    public String getVatFiledBeforeStatus() {
        return vatFiledBeforeStatus;
    }

    public void setVatFiledBeforeStatus(String vatFiledBeforeStatus) {
        this.vatFiledBeforeStatus = vatFiledBeforeStatus;
    }

    public Date getTccExpiryYear() {
        return tccExpiryYear;
    }

    public void setTccExpiryYear(Date tccExpiryYear) {
        this.tccExpiryYear = tccExpiryYear;
    }

    public Date getLastVatFiledDate() {
        return lastVatFiledDate;
    }

    public void setLastVatFiledDate(Date lastVatFiledDate) {
        this.lastVatFiledDate = lastVatFiledDate;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }
    
    
}
