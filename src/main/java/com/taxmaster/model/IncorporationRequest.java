/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RAYNOLD
 */

@Entity
@Table(name = "incorporation_request")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IncorporationRequest.findByUsername", query = "SELECT ir FROM IncorporationRequest ir WHERE ir.username = :username"),
    @NamedQuery(name = "IncorporationRequest.findAllExcludingCompletedStatus", query = "SELECT ir FROM IncorporationRequest ir WHERE ir.status <> :status")
})
public class IncorporationRequest implements Serializable {
    
    @Id
    @NotNull
    @Column(name = "request_id")
    private String id;
    
    @Column(name = "proposed_name_1")
    private String proposedCompanyNameOne;
    
    @Column(name = "proposed_name_2")
    private String proposedCompanyNameTwo;
    
    @Column(name = "proposed_name_3")
    private String proposedCompanyNameThree;
    
    @Column(name = "proposed_name_4")
    private String proposedCompanyNameFour;
    
    @Column(name = "proposed_address")
    private String companyAddress;
    
    @Column(name = "industry_id")
    private String companyIndustry;
    
    @Column(name = "username")
    private String username;
    
    @Column(name = "service_id")
    private String serviceId;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "request_date")
    private Date requestDate;
    
    @Column(name = "document_urls")
    private String documentUrls;
    
    @Column(name = "status")
    private String status;

    public IncorporationRequest() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProposedCompanyNameOne() {
        return proposedCompanyNameOne;
    }

    public void setProposedCompanyNameOne(String proposedCompanyNameOne) {
        this.proposedCompanyNameOne = proposedCompanyNameOne;
    }

    public String getProposedCompanyNameTwo() {
        return proposedCompanyNameTwo;
    }

    public void setProposedCompanyNameTwo(String proposedCompanyNameTwo) {
        this.proposedCompanyNameTwo = proposedCompanyNameTwo;
    }

    public String getProposedCompanyNameThree() {
        return proposedCompanyNameThree;
    }

    public void setProposedCompanyNameThree(String proposedCompanyNameThree) {
        this.proposedCompanyNameThree = proposedCompanyNameThree;
    }

    public String getProposedCompanyNameFour() {
        return proposedCompanyNameFour;
    }

    public void setProposedCompanyNameFour(String proposedCompanyNameFour) {
        this.proposedCompanyNameFour = proposedCompanyNameFour;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyIndustry() {
        return companyIndustry;
    }

    public void setCompanyIndustry(String companyIndustry) {
        this.companyIndustry = companyIndustry;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getDocumentUrls() {
        return documentUrls;
    }

    public void setDocumentUrls(String documentUrls) {
        this.documentUrls = documentUrls;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
