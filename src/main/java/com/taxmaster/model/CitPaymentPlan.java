/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RAYNOLD
 */

@Entity
@Table(name = "cit_payment_plan")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CitPaymentPlan.findByRevenueRangeIdAndYear", query = "SELECT cpp FROM CitPaymentPlan cpp WHERE cpp.revenueRangeId = :revenueRangeId AND cpp.year = :year")
})
public class CitPaymentPlan {
    
    @Id
    @NotNull
    @Column(name = "revenue_range_id")
    private String revenueRangeId;
          
    @Id
    @Column(name = "year")
    private String year;
        
    @Column(name = "fee")
    private String fee;
       
    public CitPaymentPlan() {
    }

    public String getRevenueRangeId() {
        return revenueRangeId;
    }

    public void setRevenueRangeId(String revenueRangeId) {
        this.revenueRangeId = revenueRangeId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getfee() {
        return fee;
    }

    public void setfee(String fee) {
        this.fee = fee;
    }
    
}
