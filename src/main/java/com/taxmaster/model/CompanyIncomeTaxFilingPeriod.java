/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RAYNOLD
 */

@Entity
@Table(name = "company_income_tax_filling_period")
@XmlRootElement
public class CompanyIncomeTaxFilingPeriod {
    
    @Id
    @NotNull
    @Column(name = "id")
    private String id;
            
    @Column(name = "description")
    private String description;

    public CompanyIncomeTaxFilingPeriod() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }  
    
}
