/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "user_tax_service_cit_payment_plan")
@XmlRootElement

public class UserTaxServiceCitPaymentPlan implements Serializable {
    @Id
    @NotNull
    @Column(name = "user_tax_service_id")
    private String userTaxServiceId;
    
    @Column(name = "revenue_range")
    private String revenueRange;
            
    @Column(name = "year")
    private String year;

    public String getUserTaxServiceId() {
        return userTaxServiceId;
    }

    public void setUserTaxServiceId(String userTaxServiceId) {
        this.userTaxServiceId = userTaxServiceId;
    }

    public String getRevenueRange() {
        return revenueRange;
    }

    public void setRevenueRange(String revenueRange) {
        this.revenueRange = revenueRange;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    
}
