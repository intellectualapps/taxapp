/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "user_business")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserLegalEntity.findByUsername", query = "SELECT ule FROM UserLegalEntity ule WHERE ule.username = :username"),
    @NamedQuery(name = "UserLegalEntity.findByEntity", query = "SELECT ule FROM UserLegalEntity ule WHERE ule.businessId = :entityId")
})
public class UserLegalEntity implements Serializable {
    @Id
    @NotNull
    @Column(name = "username")
    private String username;
            
    @Column(name = "business_id")
    private String businessId;
    
    public UserLegalEntity() { }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }    

    public String getLegalEntityId() {
        return businessId;
    }

    public void setLegalEntityId(String businessId) {
        this.businessId = businessId;
    }
    
}
