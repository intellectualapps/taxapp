/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.service;


import com.taxmaster.manager.AdminManagerLocal;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author buls
 */
@Stateless
@Path("/v1/admin")
public class AdminService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    AdminManagerLocal adminManager;
           
    @Path("/service-status/{id}/{status}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateServiceStatus(@PathParam("id") String id, @PathParam("status") String status, 
            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppBoolean appBoolean = adminManager.UpdateRequest(id, status, rawToken);
        return Response.ok(appBoolean).build();
           
    }
    
    @Path("/payment-confirmation")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmPayment(@PathParam("id") String id, @PathParam("status") String status, 
            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppBoolean appBoolean = adminManager.ConfirmPayment(id, status, rawToken);
        return Response.ok(appBoolean).build();
           
    }
    
    @Path("/assign-task/{id}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignTaskToUser(@PathParam("id") String taskId, @QueryParam("username") String username, 
            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppBoolean appBoolean = adminManager.assignTaskToUser(taskId, username, rawToken);
        return Response.ok(appBoolean).build();
           
    }
    
}
