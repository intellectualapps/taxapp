/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.service;

import com.taxmaster.manager.AdminManagerLocal;
import com.taxmaster.manager.LegalEntityManagerLocal;
import com.taxmaster.manager.UserTaxServManager;
import com.taxmaster.manager.UserTaxServManagerLocal;
import com.taxmaster.manager.UserTaxServiceNoteManagerLocal;
import com.taxmaster.manager.UtsManagerLocal;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppServiceRequestSummary;
import com.taxmaster.pojo.AppUserTaxService;
import com.taxmaster.pojo.AppUserTaxServiceNote;
import com.taxmaster.pojo.ServiceRequestSummaryPayload;
import com.taxmaster.pojo.UserTaxServiceNotePayload;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author buls
 */
@Stateless
@Path("/v1/service")
public class ServiceRequestService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    LegalEntityManagerLocal legalEntityManager;
    
    @EJB
    UtsManagerLocal userTaxServiceManager;
   
    @EJB
    UserTaxServiceNoteManagerLocal userTaxServiceNoteManager;
       
    @Path("/tax")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUserTaxService(@QueryParam("entity-id") String entityId,
                            @QueryParam("service-id") String serviceId,
                            @QueryParam("payment-plan-id") String paymentPlanId, 
                            @QueryParam("revenue-range-id") String revenueRangeId,
                            @QueryParam("year") String year, @QueryParam("filed-before-status") String citFiledBeforeStatus,
                            @QueryParam("cit-vat-filed-before-status") String citVatFiledBeforeStatus,
                            @QueryParam("vat-filed-before-status") String vatFiledBeforeStatus,
                            @QueryParam("tcc-expiry-year") String tccExpiryYear,
                            @QueryParam("last-vat-filed-date") String lastVatFiledDate,
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
                
        AppUserTaxService appUserTaxService = userTaxServiceManager.createUserTaxService(entityId, 
                serviceId, paymentPlanId, revenueRangeId, year, citFiledBeforeStatus, 
                citVatFiledBeforeStatus, vatFiledBeforeStatus, tccExpiryYear, lastVatFiledDate, rawToken);
        return Response.ok(appUserTaxService).build();
           
    }
    
    @Path("/tax")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRequestedServices(@QueryParam("page-size") String pageSize,
                            @QueryParam("page-number") String pageNumber, 
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
                
        ServiceRequestSummaryPayload serviceRequestSummaries = userTaxServiceManager.getServiceRequestSummaries(pageNumber, pageSize, rawToken);              
        return Response.ok(serviceRequestSummaries).build();
           
    }
    
    @Path("/request-details")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRequestedServiceDetails(@QueryParam("id") String id,
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
                
        AppServiceRequestSummary appServiceRequestSummary = userTaxServiceManager.getServiceRequestDetails(id, rawToken);              
        return Response.ok(appServiceRequestSummary).build();
           
    }
    
    @Path("/user")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserServices(@QueryParam("page-size") String pageSize,
                            @QueryParam("page-number") String pageNumber, 
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
                
        ServiceRequestSummaryPayload serviceRequestSummaries = userTaxServiceManager.getServiceRequestSummariesForUser(pageNumber, pageSize, rawToken);              
        return Response.ok(serviceRequestSummaries).build();
           
    }
    
    @Path("/search")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchUserTaxServices(@QueryParam("search-term") String searchTerm,
            @QueryParam("service-id") String serviceId,
            @QueryParam("status") String status,
            @QueryParam("page-size") String pageSize,
            @QueryParam("page-number") String pageNumber, 
            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
                
        ServiceRequestSummaryPayload searchResults = userTaxServiceManager.searchUserTaxServices(searchTerm, serviceId, status, pageNumber, pageSize, rawToken);
        return Response.ok(searchResults).build();
           
    }
    
    @POST
    @Path("/tax/document-url")
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveDocumentUrl(@QueryParam("id") String id, @QueryParam("url") String url, 
            @HeaderParam("Authorization") String rawToken) 
            throws GeneralAppException {
        
        AppBoolean appBoolean = userTaxServiceManager.saveDocumentUrl(id, url, rawToken);
        
        return Response.ok(appBoolean).build();
    }
    
    @POST
    @Path("/note")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUserTaxServiceNote(@QueryParam("user-tax-service-id") String userTaxServiceId, @QueryParam("note-text") String noteText, 
            @HeaderParam("Authorization") String rawToken) 
            throws GeneralAppException {
        
        AppUserTaxServiceNote appUserTaxServiceNote = userTaxServiceNoteManager.createUserTaxServiceNote(userTaxServiceId, noteText, rawToken);
        
        return Response.ok(appUserTaxServiceNote).build();
    }
    
    @GET
    @Path("/note/{user-tax-service-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserTaxServiceNotes(@PathParam("user-tax-service-id") String userTaxServiceId,            
            @QueryParam("page-size") String pageSize,
            @QueryParam("page-number") String pageNumber,
            @HeaderParam("Authorization") String rawToken) 
            throws GeneralAppException {
        
        UserTaxServiceNotePayload appUserTaxServiceNote = userTaxServiceNoteManager.getUserTaxServiceNotes(userTaxServiceId, pageNumber, pageSize, rawToken);
        
        return Response.ok(appUserTaxServiceNote).build();
    }
    
    @PUT
    @Path("/tax/{id}/discount")
    @Produces(MediaType.APPLICATION_JSON)
    public Response applyDiscount(@PathParam("id") String id, @QueryParam("discount-code") String discountCode, 
            @HeaderParam("Authorization") String rawToken) 
            throws GeneralAppException {
        
        AppUserTaxService appUserTaxService = userTaxServiceManager.applyDiscount(id, discountCode, rawToken);
        
        return Response.ok(appUserTaxService).build();
    }
    
}
