/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.service;

import com.taxmaster.manager.PaymentPlanManagerLocal;
import com.taxmaster.pojo.AppCitPaymentPlan;
import com.taxmaster.pojo.AppPaymentPlan;
import com.taxmaster.pojo.IncorporationPaymentPlanPayload;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/v1/payment-plan")
public class PaymentPlanService {
       
    @EJB    
    PaymentPlanManagerLocal paymentPlanManager;
   
           
    @GET
    @Path("/{service-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPaymentPlans(@HeaderParam("Authorization") String rawToken, 
            @PathParam("service-id") String serviceId) throws GeneralAppException {  
        
        List<AppPaymentPlan> appPaymentPlans = paymentPlanManager.getPaymentPlans(serviceId, rawToken);
        GenericEntity<List<AppPaymentPlan>> response = new GenericEntity<List<AppPaymentPlan>>(appPaymentPlans) {};                
        return Response.ok(response).build();
           
    }
    
    @GET
    @Path("/incorporation-plans")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIncorporationTypes(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {
        
        IncorporationPaymentPlanPayload incorporationPaymentPlanPayload = paymentPlanManager.getIncorporationPaymentPlans(rawToken);
        return Response.ok(incorporationPaymentPlanPayload).build();
    }
    
    @GET
    @Path("/cit")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCitPaymentPlan(@QueryParam("business-id") String businessId, 
            @QueryParam("year") String year, @HeaderParam("Authorization") String rawToken) 
            throws GeneralAppException {
        
        List<AppPaymentPlan> appPaymentPlans = paymentPlanManager.getCitPaymentPlans(businessId, year, rawToken);
        GenericEntity<List<AppPaymentPlan>> appCitPaymentPlans = new GenericEntity<List<AppPaymentPlan>>(appPaymentPlans) {};                
        return Response.ok(appCitPaymentPlans).build();
        
    }
    
}
