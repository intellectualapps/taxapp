/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.service;

import com.taxmaster.manager.LegalEntityManagerLocal;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppLegalEntity;
import com.taxmaster.pojo.AppIncorporationRequest;
import com.taxmaster.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/v1/business")
public class LegalEntityService {
       
    @EJB    
    LegalEntityManagerLocal legalEntityManager;
           
    @POST
    @Path("/tax")
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerLegalEntity(@HeaderParam("Authorization") String rawToken, 
            @QueryParam("business-name") String name, @QueryParam("business-address") String address,
            @QueryParam("business-type-id") String typeId, @QueryParam("business-industry-id") String industryId, 
            @QueryParam("incorporation-date") String incorporationDate, @QueryParam("tin") String tin, 
            @QueryParam("commencement-date") String commencementDate, 
            @QueryParam("revenue-range-id") String revenueRangeId,
            @QueryParam("state-id") String stateId, @QueryParam("address-line-1") String addressLine1,
            @QueryParam("address-line-2") String addressLine2, @QueryParam("city") String city) throws GeneralAppException {  
        
        AppLegalEntity appLegalEntity = legalEntityManager.create(rawToken, name, address, 
                typeId, industryId, incorporationDate, tin, commencementDate, revenueRangeId, stateId,
                addressLine1, addressLine2, city);
        return Response.ok(appLegalEntity).build();
           
    }    
    
    
    @POST
    @Path("/incorporation-request")
    @Produces(MediaType.APPLICATION_JSON)
    public Response makeIncorporationRequest(@HeaderParam("Authorization") String rawToken,
            @QueryParam("proposed-company-name-one") String proposedCompanyNameOne,
            @QueryParam("proposed-company-name-two") String proposedCompanyNameTwo,
            @QueryParam("proposed-company-name-three") String proposedCompanyNameThree,
            @QueryParam("proposed-company-name-four") String proposedCompanyNameFour,
            @QueryParam("company-address") String companyAddress,
            @QueryParam("company-industry") String companyIndustry) throws GeneralAppException {
        
        AppIncorporationRequest appIncorporationRequest = legalEntityManager.createIncorporationRequest(rawToken, proposedCompanyNameOne, proposedCompanyNameTwo, proposedCompanyNameThree, proposedCompanyNameFour, companyAddress, companyIndustry);
        
        return Response.ok(appIncorporationRequest).build();
    }
    
    @GET
    @Path("/exists")
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmUserHasAnEntity(@HeaderParam("Authorization") String rawToken) 
            throws GeneralAppException {
        
        AppBoolean appBoolean = legalEntityManager.hasLegalEntity(rawToken);
        
        return Response.ok(appBoolean).build();
    }
    
    @GET
    @Path("/incorporation-request/exists")
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmUserHasAnIncorporationRequest(@HeaderParam("Authorization") String rawToken) 
            throws GeneralAppException {
        
        AppBoolean appBoolean = legalEntityManager.hasIncorporationRequest(rawToken);
        
        return Response.ok(appBoolean).build();
    }        
    
}
