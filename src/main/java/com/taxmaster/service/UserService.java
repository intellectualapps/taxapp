/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.service;


import com.taxmaster.manager.UserManagerLocal;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.pojo.UserPayload;
import com.taxmaster.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lateefah
 */
@Stateless
@Path("/v1/user")
public class UserService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    UserManagerLocal userManager;
       
    //http://localhost:8080/zus/api/v1/user/?username=buls&email=lateefah.abdulkareem@intellectualapps.com&password=password
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@QueryParam("id") String email,
                            @QueryParam("full-name") String fullName,
                            @QueryParam("phone-number") String phoneNumber,
                            @QueryParam("birth-date") String birthdate) throws GeneralAppException {  
        
        AppUser appUser = userManager.register(email, fullName, phoneNumber, birthdate);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{username}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@HeaderParam("Authorization") String rawToken,
                            @PathParam("username") String username) throws GeneralAppException {  
        
        AppUser appUser = userManager.getUserDetails(username, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{username}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("username") String username,
                                @QueryParam("email") String email,
                                @QueryParam("location") String location,
                                @QueryParam("first-name") String firstName,
                                @QueryParam("last-name") String lastName,
                                @QueryParam("photo-url") String photoUrl,
                                @QueryParam("phone-number") String phoneNumber,
                                @QueryParam("facebook-email") String facebookEmail,
                                @QueryParam("twitter-email") String twitterEmail,
                                @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppUser appUser = userManager.updateUserDetails(username, email, firstName, lastName, phoneNumber, photoUrl, 
                                                        location, facebookEmail, twitterEmail, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{username}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("username") String username) throws GeneralAppException {  
        
        return Response.ok(userManager.deleteUser(username)).build();
           
    }
    
    @Path("/authenticate")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticateUser(@QueryParam("id") String id,
                            @QueryParam("password") String password,
                            @QueryParam("auth-type") String type) throws GeneralAppException {  
        
        AppUser appUser = userManager.authenticate(id, password, type);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/username")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUsername(@QueryParam("username") String username,
                            @QueryParam("email") String email, 
                            @QueryParam("temp-key") String tempKey) throws GeneralAppException {  
        
        AppUser appUser = userManager.createUsername(username, email, tempKey);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/verify/{username}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyUsername(@PathParam("username") String username) throws GeneralAppException {   
        
        return Response.ok(userManager.verifyUsername(username)).build();            
    
    }
    
    @Path("/link-social/{username}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response linkSocialAccount(@PathParam("username") String username,
                            @QueryParam("platform") String platform,
                            @QueryParam("email") String email,
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppUser appUser = userManager.linkSocialAcount(username, platform, email, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/unlink-social/{username}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response unlinkSocialAccount(@PathParam("username") String username,
                            @QueryParam("platform") String platform,
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppUser appUser = userManager.unlinkSocialAcount(username, platform, rawToken);
        return Response.ok(appUser).build();
           
    }

    @Path("/get-list")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListOfUsers(@HeaderParam("Authorization") String rawToken,
                            @QueryParam("usernames") String usernames) throws GeneralAppException {  
        
        UserPayload appUser = userManager.getListOfUsers(usernames, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/notify")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response notifyUser(@QueryParam("offline-username") String offlineUsername,
            @QueryParam("online-username") String onlineUsername,
            @QueryParam("message-id") String messageId,
            @QueryParam("platform") String notificationPlatform,
            @HeaderParam("Authorization") String rawToken)
    throws GeneralAppException {  
        userManager.notifyOfflineUser(offlineUsername, onlineUsername, messageId, 
                rawToken, notificationPlatform);        
        return Response.ok().build();
           
    }        
    
    @Path("/search-users")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchUsers(@HeaderParam("Authorization") String rawToken,
                            @QueryParam("query") String searchTerm,
                            @QueryParam("page-size") String pageSize,
                            @QueryParam("page-number") String pageNumber) throws GeneralAppException {  
        
        UserPayload appUser = userManager.searchUsers(searchTerm, pageNumber, pageSize, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/login")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response logUserInFromWelcomeEmail(@QueryParam("e") String userHash) throws GeneralAppException {  
        
        AppUser appUser = userManager.LogUserInFromWelcomeEmail(userHash);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/reset-otp/{email-address}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response resetUserOtp(@PathParam("email-address") String emailAddress) throws GeneralAppException {  
        
        userManager.resetOtp(emailAddress);
        return Response.ok().build();
           
    }
    
    @Path("/contact")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response contactUs(@QueryParam("name") String name, @QueryParam("email") String email,
                            @QueryParam("subject") String subject, @QueryParam("message") String message) throws GeneralAppException {  
        
        AppBoolean appBoolean = userManager.contactUs(name, email, subject, message);
        return Response.ok(appBoolean).build();
           
    }
    
}
