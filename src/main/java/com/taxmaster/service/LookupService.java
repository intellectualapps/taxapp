/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.service;

import com.taxmaster.manager.LegalEntityManagerLocal;
import com.taxmaster.manager.LookupManagerLocal;
import com.taxmaster.model.LegalEntityType;
import com.taxmaster.model.CompanyIncomeTaxFilingPeriod;
import com.taxmaster.model.ContactFormSubject;
import com.taxmaster.model.Industry;
import com.taxmaster.model.RevenueRange;
import com.taxmaster.model.Service;
import com.taxmaster.model.State;
import com.taxmaster.pojo.AppLegalEntity;
import com.taxmaster.pojo.AppServiceStatusType;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/v1/lookup")
public class LookupService {
       
    @EJB    
    LookupManagerLocal lookupManager;
           
    @GET
    @Path("/industries")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIndustries(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<Industry> industries = lookupManager.getIndustries();
        GenericEntity<List<Industry>> response = new GenericEntity<List<Industry>>(industries) {};                
        return Response.ok(response).build();
           
    }    
    
    @GET
    @Path("/business-types")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLegalEntityType(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<LegalEntityType> businessTypes = lookupManager.getLegalEntityTypes();
        GenericEntity<List<LegalEntityType>> response = new GenericEntity<List<LegalEntityType>>(businessTypes) {};                
        return Response.ok(response).build();
           
    }    
    
    @GET
    @Path("/revenue-ranges")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRevenueRanges(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<RevenueRange> revenueRanges = lookupManager.getRevenueRanges();
        GenericEntity<List<RevenueRange>> response = new GenericEntity<List<RevenueRange>>(revenueRanges) {};                
        return Response.ok(response).build();
           
    }    
    
    @GET
    @Path("/company-income-tax-periods")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCompanyIncomeTaxFillingPeriods() {
        
        List<CompanyIncomeTaxFilingPeriod> companyIncomeTaxFilingPeriod = lookupManager.getCompanyIncomeTaxFilingPeriod();
        GenericEntity<List<CompanyIncomeTaxFilingPeriod>> response = new GenericEntity<List<CompanyIncomeTaxFilingPeriod>>(companyIncomeTaxFilingPeriod){};
        return Response.ok(response).build();
    }
    
    @GET
    @Path("/states")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStates() {
        
        List<State> states = lookupManager.getAllStates();
        GenericEntity<List<State>> response = new GenericEntity<List<State>>(states){};
        return Response.ok(response).build();
    }
    
    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@QueryParam ("role") String role ){
        List<AppUser> appUsers = lookupManager.getUsersByRole(role);
        GenericEntity<List<AppUser>> response = new GenericEntity<List<AppUser>>(appUsers){};
        return Response.ok(response).build();
    }
    
    @GET
    @Path("/contact-form-subject")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContactFormSubject() {
        
        List<ContactFormSubject> contactFormSubjects = lookupManager.getAllSubject();
        GenericEntity<List<ContactFormSubject>> response = new GenericEntity<List<ContactFormSubject>>(contactFormSubjects){};
        return Response.ok(response).build();
    }
    
    @GET
    @Path("/services")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServices(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<Service> industries = lookupManager.getServices();
        GenericEntity<List<Service>> response = new GenericEntity<List<Service>>(industries) {};                
        return Response.ok(response).build();
           
    } 
    
    @GET
    @Path("/service-statuses")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServiceStatuses(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<AppServiceStatusType> serviceStatuses = lookupManager.getServiceStatuses();
        GenericEntity<List<AppServiceStatusType>> response = new GenericEntity<List<AppServiceStatusType>>(serviceStatuses) {};                
        return Response.ok(response).build();
           
    } 
}
