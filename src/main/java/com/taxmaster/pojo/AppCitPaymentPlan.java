/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

/**
 *
 * @author RAYNOLD
 */
public class AppCitPaymentPlan extends AppPaymentPlan {
    
    String revenueRangeId;
    String year;
    String fee;
    
    public AppCitPaymentPlan() {
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getfee() {
        return fee;
    }

    public void setfee(String fee) {
        this.fee = fee;
    }

    public String getRevenueRangeId() {
        return revenueRangeId;
    }

    public void setRevenueRangeId(String revenueRangeId) {
        this.revenueRangeId = revenueRangeId;
    }
        
}
