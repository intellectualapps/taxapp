/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

import java.io.Serializable;
import java.util.Date;


public class AppUserTaxService implements Serializable {
    
    private String id;
    private String username;
    private String businessId;
    private String entityName;
    private String serviceId;
    private String serviceName;
    private String dateRequested;
    private Date dateRequestedDateObject;
    private Integer accountingYear;
    private Date fromDate;
    private Date toDate;
    private String documentUrls;
    private String status;
    private String nonCitPaymentPlan;
    private String citFiledBeforeStatus;
    private String citVatFiledBeforeStatus;
    private String vatFiledBeforeStatus;
    private Date tccExpiryYear;
    private Date lastVatFiledDate;
    private AppServiceCharge serviceCharge;
    
    public AppUserTaxService() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(String dateRequested) {
        this.dateRequested = dateRequested;
    }

    public Date getDateRequestedDateObject() {
        return dateRequestedDateObject;
    }

    public void setDateRequestedDateObject(Date dateRequestedDateObject) {
        this.dateRequestedDateObject = dateRequestedDateObject;
    }

    public Integer getAccountingYear() {
        return accountingYear;
    }

    public void setAccountingYear(Integer accountingYear) {
        this.accountingYear = accountingYear;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getDocumentUrls() {
        return documentUrls;
    }

    public void setDocumentUrls(String documentUrls) {
        this.documentUrls = documentUrls;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }        

    public String getNonCitPaymentPlan() {
        return nonCitPaymentPlan;
    }

    public void setNonCitPaymentPlan(String nonCitPaymentPlan) {
        this.nonCitPaymentPlan = nonCitPaymentPlan;
    }

    public String getCitFiledBeforeStatus() {
        return citFiledBeforeStatus;
    }

    public void setCitFiledBeforeStatus(String citFiledBeforeStatus) {
        this.citFiledBeforeStatus = citFiledBeforeStatus;
    }

    public String getCitVatFiledBeforeStatus() {
        return citVatFiledBeforeStatus;
    }

    public void setCitVatFiledBeforeStatus(String citVatFiledBeforeStatus) {
        this.citVatFiledBeforeStatus = citVatFiledBeforeStatus;
    }

    public String getVatFiledBeforeStatus() {
        return vatFiledBeforeStatus;
    }

    public void setVatFiledBeforeStatus(String vatFiledBeforeStatus) {
        this.vatFiledBeforeStatus = vatFiledBeforeStatus;
    }

    public Date getTccExpiryYear() {
        return tccExpiryYear;
    }

    public void setTccExpiryYear(Date tccExpiryYear) {
        this.tccExpiryYear = tccExpiryYear;
    }

    public Date getLastVatFiledDate() {
        return lastVatFiledDate;
    }

    public void setLastVatFiledDate(Date lastVatFiledDate) {
        this.lastVatFiledDate = lastVatFiledDate;
    }

    public AppServiceCharge getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(AppServiceCharge serviceCharge) {
        this.serviceCharge = serviceCharge;
    }    

    
}
