/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

import com.taxmaster.model.LegalEntity;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public class AppUser implements Serializable {
    private String username;  
    private String email;
    private String firstName;    
    private String lastName; 
    private String phoneNumber; 
    private String password;
    private Date creationDate;
    private String photoUrl;
    private String facebookEmail;
    private String twitterEmail;
    private String location;    
    private String authToken;
    private String tempKey;
    private Integer verified;
    private Integer validated;
    private String hash;
    private String onboardingStatus;
    private String role;
    private Date birthday;
    private List<AppLegalEntity> businesses;
    
    
    public AppUser() {
    
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber () {
        return phoneNumber;
    }
    
    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Date getCreationDate() {
        return creationDate;
    }
    
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    
    public String getPhotoUrl() {
        return photoUrl;
    }
    
    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
    
    public String getFacebookEmail() {
        return facebookEmail;
    }
    
    public void setFacebookEmail(String facebookEmail) {
        this.facebookEmail = facebookEmail;
    }
    
    public String getTwitterEmail() {
        return twitterEmail;
    }
    
    public void setTwitterEmail(String twitterEmail) {
        this.twitterEmail = twitterEmail;
    }
    
    public String getLocation() {
        return location;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
    
    public String getAuthToken() {
        return authToken;
    }

    public String getTempKey() {
        return tempKey;
    }

    public void setTempKey(String tempKey) {
        this.tempKey = tempKey;
    }        

    public Integer getVerified() {
        return verified;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }

    public Integer getValidated() {
        return validated;
    }

    public void setValidated(Integer validated) {
        this.validated = validated;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getOnboardingStatus() {
        return onboardingStatus;
    }

    public void setOnboardingStatus(String onboardingStatus) {
        this.onboardingStatus = onboardingStatus;
    }    

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<AppLegalEntity> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<AppLegalEntity> businesses) {
        this.businesses = businesses;
    }
    
}
