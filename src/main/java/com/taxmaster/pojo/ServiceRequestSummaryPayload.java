/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author buls
 */

public class ServiceRequestSummaryPayload implements Serializable {
    
    private List<AppServiceRequestSummary> appServiceRequestSummaries;
    private int pageSize;
    private int pageNumber;
    private int totalCount;
    
    public ServiceRequestSummaryPayload() {
        this.appServiceRequestSummaries = new ArrayList<AppServiceRequestSummary>();
    }

    public List<AppServiceRequestSummary> getAppServiceRequestSummaries() {
        List<AppServiceRequestSummary> result = new ArrayList<AppServiceRequestSummary>();
                 
        int offset = (pageNumber - 1) * pageSize + 1  ;
        int end = pageNumber*pageSize;
        
        if (offset > appServiceRequestSummaries.size()) {
            return result;
        }
        
        if (end > appServiceRequestSummaries.size()) {
            end = appServiceRequestSummaries.size();
        }
        result = appServiceRequestSummaries.subList(offset-1, end);
        
        return result;
    }

    public List<AppServiceRequestSummary> acquireAllAppServiceRequestSummaries() {
        return this.appServiceRequestSummaries;
    }
    
    public void setAppServiceRequestSummaries(List<AppServiceRequestSummary> appServiceRequestSummaries) {
        this.appServiceRequestSummaries = appServiceRequestSummaries;
    }

    public void addAppServiceRequestSummaries(List<AppServiceRequestSummary> appServiceRequestSummaries) {
        for (AppServiceRequestSummary appServiceRequestSummary : appServiceRequestSummaries) {
            this.appServiceRequestSummaries.add(appServiceRequestSummary);
        }
        
    }    

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }    
    
}
