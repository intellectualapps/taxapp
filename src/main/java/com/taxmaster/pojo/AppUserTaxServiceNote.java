/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

import java.io.Serializable;
import java.util.Date;

public class AppUserTaxServiceNote implements Serializable {
  
    private String id;
    private String userTaxServiceId;
    private String text;
    private String username;
    private String dateAdded;
    
    public AppUserTaxServiceNote() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserTaxServiceId() {
        return userTaxServiceId;
    }

    public void setUserTaxServiceId(String userTaxServiceId) {
        this.userTaxServiceId = userTaxServiceId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }        
    
}
