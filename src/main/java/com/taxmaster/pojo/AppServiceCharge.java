/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

import java.io.Serializable;

public class AppServiceCharge implements Serializable {
   
    private String id;       
    private Integer citService;
    private Integer vatService;
    private Integer deliveryService;
    private Integer subTotal;
    private Double vat;
    private Double total;
    private String discountCode;
    private String discountDate;
    private Double discountAmount;
    private Double preDiscountTotal;
    
    public AppServiceCharge() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCitService() {
        return citService;
    }

    public void setCitService(Integer citService) {
        this.citService = citService;
    }

    public Integer getVatService() {
        return vatService;
    }

    public void setVatService(Integer vatService) {
        this.vatService = vatService;
    }

    public Integer getDeliveryService() {
        return deliveryService;
    }

    public void setDeliveryService(Integer deliveryService) {
        this.deliveryService = deliveryService;
    }

    public Double getVat() {
        return vat;
    }

    public void setVat(Double vat) {
        this.vat = vat;
    }

    public Integer getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Integer subTotal) {
        this.subTotal = subTotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    } 

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getDiscountDate() {
        return discountDate;
    }

    public void setDiscountDate(String discountDate) {
        this.discountDate = discountDate;
    }    

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }    

    public Double getPreDiscountTotal() {
        return preDiscountTotal;
    }

    public void setPreDiscountTotal(Double preDiscountTotal) {
        this.preDiscountTotal = preDiscountTotal;
    }
    
    
   
}
