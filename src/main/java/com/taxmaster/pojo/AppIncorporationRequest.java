/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

import java.io.Serializable;

/**
 *
 * @author RAYNOLD
 */
public class AppIncorporationRequest implements Serializable{
    private String proposedCompanyNameOne;
    private String proposedCompanyNameTwo;
    private String proposedCompanyNameThree;
    private String proposedCompanyNameFour;
    private String companyAddress;
    private String companyIndustry;

    public AppIncorporationRequest() {
    }

    public String getProposedCompanyNameOne() {
        return proposedCompanyNameOne;
    }

    public void setProposedCompanyNameOne(String proposedCompanyNameOne) {
        this.proposedCompanyNameOne = proposedCompanyNameOne;
    }

    public String getProposedCompanyNameTwo() {
        return proposedCompanyNameTwo;
    }

    public void setProposedCompanyNameTwo(String proposedCompanyNameTwo) {
        this.proposedCompanyNameTwo = proposedCompanyNameTwo;
    }

    public String getProposedCompanyNameThree() {
        return proposedCompanyNameThree;
    }

    public void setProposedCompanyNameThree(String proposedCompanyNameThree) {
        this.proposedCompanyNameThree = proposedCompanyNameThree;
    }

    public String getProposedCompanyNameFour() {
        return proposedCompanyNameFour;
    }

    public void setProposedCompanyNameFour(String proposedCompanyNameFour) {
        this.proposedCompanyNameFour = proposedCompanyNameFour;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyIndustry() {
        return companyIndustry;
    }

    public void setCompanyIndustry(String companyIndustry) {
        this.companyIndustry = companyIndustry;
    }

    
}