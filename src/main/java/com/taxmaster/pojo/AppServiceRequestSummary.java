/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

import com.taxapp.controller.AppIncorporationType;
import com.taxmaster.model.LegalEntity;
import com.taxmaster.model.ServiceCharge;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author buls
 */

public class AppServiceRequestSummary implements Serializable {
    
    private String id;
    private String username;
    private String clientName;
    private String requestedServiceId;
    private String requestedServiceName;
    private String dateRequested;
    private Date dateRequestedDateObject;
    private String requestStatus;
    private AppLegalEntity entity;
    private AppIncorporationRequest incorporationRequest;
    private ServiceCharge serviceCharge;
    private String assignedTo;
    private String documentUrls;

    public AppServiceRequestSummary() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getRequestedServiceId() {
        return requestedServiceId;
    }

    public void setRequestedServiceId(String requestedServiceId) {
        this.requestedServiceId = requestedServiceId;
    }

    public String getRequestedServiceName() {
        return requestedServiceName;
    }

    public void setRequestedServiceName(String requestedServiceName) {
        this.requestedServiceName = requestedServiceName;
    }

    public String getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(String dateRequested) {
        this.dateRequested = dateRequested;
    }

    public Date getDateRequestedDateObject() {
        return dateRequestedDateObject;
    }

    public void setDateRequestedDateObject(Date dateRequestedDateObject) {
        this.dateRequestedDateObject = dateRequestedDateObject;
    }
    
    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public AppLegalEntity getEntity() {
        return entity;
    }

    public void setEntity(AppLegalEntity entity) {
        this.entity = entity;
    }

    public AppIncorporationRequest getIncorporationRequest() {
        return incorporationRequest;
    }

    public void setIncorporationRequest(AppIncorporationRequest incorporationRequest) {
        this.incorporationRequest = incorporationRequest;
    }   

    public ServiceCharge getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(ServiceCharge serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getDocumentUrls() {
        return documentUrls;
    }

    public void setDocumentUrls(String documentUrls) {
        this.documentUrls = documentUrls;
    }
    
}
