/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author buls
 */
public class UserTaxServiceNotePayload {

    private int pageSize;
    private int pageNumber;
    private int totalCount;    
    private List<AppUserTaxServiceNote> notes;
    
    public UserTaxServiceNotePayload() {
        notes = new ArrayList<>();
    }

    public List<AppUserTaxServiceNote> getNotes() {
        List<AppUserTaxServiceNote> usersTaxServiceNotePage = new ArrayList<>();
                 
        int offset = (pageNumber - 1) * pageSize + 1  ;
        int end = pageNumber*pageSize;
        
        if (offset > notes.size()) {
            return usersTaxServiceNotePage;
        }
        
        if (end > notes.size()) {
            end = notes.size();
        }
        usersTaxServiceNotePage = notes.subList(offset-1, end);
        
        return usersTaxServiceNotePage;
    }

    public void setNotes(List<AppUserTaxServiceNote> notes) {
        this.notes = notes;
    }        

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
    
    
    
}
