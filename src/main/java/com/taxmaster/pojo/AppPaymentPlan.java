
package com.taxmaster.pojo;

import java.io.Serializable;

public class AppPaymentPlan implements Serializable {
    
    private String id;
    private String serviceId;
    private String frequency;
    private int amount;
    private String serviceName;
    private String revenueRangeId;
    private String year;
    private String fee;
   
    public AppPaymentPlan() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getRevenueRangeId() {
        return revenueRangeId;
    }

    public void setRevenueRangeId(String revenueRangeId) {
        this.revenueRangeId = revenueRangeId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }
        
}
