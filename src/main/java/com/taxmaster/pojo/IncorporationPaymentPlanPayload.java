/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author RAYNOLD
 */
public class IncorporationPaymentPlanPayload  implements Serializable {
    
    List<AppPaymentPlan> paymentPlans;
    List<AppService> services;

    public IncorporationPaymentPlanPayload() {
        paymentPlans = new ArrayList<>();
        services = new ArrayList<>();
    }

    public List<AppPaymentPlan> getPaymentPlans() {
        return paymentPlans;
    }

    public void setPaymentPlans(List<AppPaymentPlan> paymentPlans) {
        this.paymentPlans = paymentPlans;
    }
    
    public void setPaymentPlan(AppPaymentPlan appPaymentPlan) {
        this.paymentPlans.add(appPaymentPlan);
    }

    public List<AppService> getServices() {
        return services;
    }

    public void setServices(List<AppService> services) {
        this.services = services;
    }
    
    public void setService(AppService service) {
        this.services.add(service);
    }
    
}
