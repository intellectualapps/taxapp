/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.UserTaxService;
import com.taxmaster.pojo.AppUserTaxServiceNote;
import com.taxmaster.pojo.UserTaxServiceNotePayload;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author buls
 */
@Local
public interface UserTaxServiceNoteManagerLocal {
    
    AppUserTaxServiceNote createUserTaxServiceNote(String userTaxServiceId, String noteText, 
            String rawToken) throws GeneralAppException;

    void delete(UserTaxService userTaxService) throws GeneralAppException;
    
    List<UserTaxService> getUserTaxServiceNotesByUserTaxServiceId(String userTaxServiceId) throws GeneralAppException;           
    
    UserTaxService updateNote(String id, String noteText) throws GeneralAppException;           

    UserTaxServiceNotePayload getUserTaxServiceNotes(String userTaxServiceId, String pageNumber, String pageSize, String rawToken) throws GeneralAppException;           
}
