/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.CitPaymentPlanDataManagerLocal;
import com.taxmaster.data.manager.PaymentPlanDataManagerLocal;
import com.taxmaster.model.LegalEntity;
import com.taxmaster.model.CitPaymentPlan;
import com.taxmaster.model.PaymentPlan;
import com.taxmaster.model.Service;
import com.taxmaster.pojo.AppCitPaymentPlan;
import com.taxmaster.pojo.AppPaymentPlan;
import com.taxmaster.pojo.AppService;
import com.taxmaster.pojo.IncorporationPaymentPlanPayload;
import com.taxmaster.util.ServiceCategoryType;
import com.taxmaster.util.ServiceType;
import com.taxmaster.util.Verifier;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class PaymentPlanManager implements PaymentPlanManagerLocal {

    @EJB
    private PaymentPlanDataManagerLocal paymentPlanManager; 
    
    @EJB
    private ServiceManagerLocal serviceManager; 
    
    @EJB
    CitPaymentPlanDataManagerLocal citPaymentPlanDataManager;
    
    @EJB
    LegalEntityManagerLocal businessManager;
    
    @EJB
    private Verifier verifier;  
    
    private final String PAYMENT_PLANS_LINK = "/payment-plans";
    private final String CIT_PAYMENT_PLAN_RESOURCE = "/cit-payment-plan";

    @Override
    public List<AppPaymentPlan> getPaymentPlans(String serviceId, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(PAYMENT_PLANS_LINK).verifyParams(serviceId, rawToken);
        verifier.setResourceUrl(PAYMENT_PLANS_LINK).verifyJwt(rawToken);
        
        List<PaymentPlan> paymentPlans = paymentPlanManager.get(serviceId);
        
        return getAppPaymentPlans(paymentPlans);
    }
    
    @Override
    public List<AppPaymentPlan> getPaymentPlans(String serviceId) throws GeneralAppException {
        verifier.setResourceUrl(PAYMENT_PLANS_LINK).verifyParams(serviceId);        
        
        List<PaymentPlan> paymentPlans = paymentPlanManager.get(serviceId);
        
        return getAppPaymentPlans(paymentPlans);
    }

    
    private List<AppPaymentPlan> getAppPaymentPlans(List<PaymentPlan> paymentPlans) {
        List<AppPaymentPlan> appPaymentPlans = new ArrayList<AppPaymentPlan>();
        for (PaymentPlan paymentPlan : paymentPlans) {
            AppPaymentPlan appPaymentPlan = getAppPaymentPlan(paymentPlan);
            if (!appPaymentPlan.getFrequency().equals("Flat")) {
                appPaymentPlans.add(appPaymentPlan);
            }
        }
        
        return appPaymentPlans;
    }
    
    private AppPaymentPlan getAppPaymentPlan(PaymentPlan paymentPlan) {
        AppPaymentPlan appPaymentPlan = new AppPaymentPlan();
        if (paymentPlan != null) {
            appPaymentPlan.setId(paymentPlan.getId());
            appPaymentPlan.setServiceId(paymentPlan.getServiceId());
            appPaymentPlan.setFrequency(paymentPlan.getFrequency());
            appPaymentPlan.setAmount(paymentPlan.getAmount());
            appPaymentPlan.setServiceId(paymentPlan.getServiceId());
        }
        
        return appPaymentPlan;
    }

    @Override
    public IncorporationPaymentPlanPayload getIncorporationPaymentPlans(String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(PAYMENT_PLANS_LINK).verifyJwt(rawToken);

        List<Service> services = serviceManager.getServices(ServiceCategoryType.INCORPORATION.getDescription());
        IncorporationPaymentPlanPayload incorporationPaymentPlanPayload = getAppPaymentPlansFromService(services);
        
        return incorporationPaymentPlanPayload;
    }
    
    private IncorporationPaymentPlanPayload getAppPaymentPlansFromService(List<Service> services) throws GeneralAppException {
        List<PaymentPlan> paymentPlans = new ArrayList<>();
        
        for (Service service : services) {
            if (!service.getId().equals(ServiceType.COMPANY_INCORPORATION.getDescription())) {
                List<PaymentPlan> existingPaymentPlans = paymentPlanManager.get(service.getId());
                if (paymentPlans.size() >= 0) {
                    PaymentPlan paymentPlan = existingPaymentPlans.get(0);
                    paymentPlans.add(paymentPlan);
                }
            }
        }
        
        List<AppPaymentPlan> appPaymentPlans = getAppPaymentPlans(paymentPlans);
        List<AppService> appServices = serviceManager.getAppServices(services);
        
        IncorporationPaymentPlanPayload incorporationPaymentPlanPayload = new IncorporationPaymentPlanPayload();
        incorporationPaymentPlanPayload.setPaymentPlans(appPaymentPlans);
        incorporationPaymentPlanPayload.setServices(appServices);
        
        return incorporationPaymentPlanPayload;
    }
    
    @Override
    public List<AppPaymentPlan> getCitPaymentPlans(String businessId, String year, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(CIT_PAYMENT_PLAN_RESOURCE).verifyParams(businessId, year, rawToken);
        verifier.setResourceUrl(CIT_PAYMENT_PLAN_RESOURCE).verifyJwt(rawToken);
        
        LegalEntity business = businessManager.getLegalEntity(businessId);
        List<AppPaymentPlan> appPaymentPlans = new ArrayList<>();
        if (business != null) {
            String revenueRangeId = business.getRevenueRangeId();
            List<CitPaymentPlan> citPaymentplans = citPaymentPlanDataManager.get(revenueRangeId, year);
                      
            appPaymentPlans = getAppPaymentPlansBasedOnCitPaymentPlan(citPaymentplans);
        }
        
        return appPaymentPlans;
    }
    
    
    private List<AppPaymentPlan> getAppPaymentPlansBasedOnCitPaymentPlan(List<CitPaymentPlan> citPaymentPlans) {
        List<AppPaymentPlan> appPaymentPlans = new ArrayList<>();
        
        for (CitPaymentPlan citPaymentPlan : citPaymentPlans) {
            AppPaymentPlan appPaymentPlan = new AppPaymentPlan();
            appPaymentPlan.setServiceId(ServiceType.COMPANY_INCOME_TAX.getDescription());
            appPaymentPlan.setRevenueRangeId(citPaymentPlan.getRevenueRangeId());
            appPaymentPlan.setFrequency(citPaymentPlan.getYear());
            appPaymentPlan.setAmount(Integer.valueOf(citPaymentPlan.getfee()));
            appPaymentPlans.add(appPaymentPlan);
        }
        
        return appPaymentPlans;
    }

    @Override
    public CitPaymentPlan createCitPaymentPlan(CitPaymentPlan citPaymentPlan) throws GeneralAppException {
        return citPaymentPlanDataManager.create(citPaymentPlan);
    }

    @Override
    public PaymentPlan getFlatRatePaymentPlan(String serviceId) throws GeneralAppException {
        List<PaymentPlan> paymentPlans = paymentPlanManager.get(serviceId);
        
        PaymentPlan flatPaymentPlan = new PaymentPlan();
        
        for (PaymentPlan paymentPlan : paymentPlans) {
            if (paymentPlan.getFrequency().equals("Flat")) {
                flatPaymentPlan = paymentPlan;
            }
        }
        
        return flatPaymentPlan;
    }
    
    
    
}

