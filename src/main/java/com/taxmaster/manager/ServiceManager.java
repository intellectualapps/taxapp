/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.ServiceDataManagerLocal;
import com.taxmaster.model.Service;
import com.taxmaster.pojo.AppService;
import com.taxmaster.util.ServiceCategoryType;
import com.taxmaster.util.ServiceType;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author RAYNOLD
 */
@Stateless
public class ServiceManager implements ServiceManagerLocal {
    
    @EJB
    ServiceDataManagerLocal serviceDataManager;

    @Override
    public List<Service> getServices(String serviceType) throws GeneralAppException {
        if (serviceType == null) {
            return serviceDataManager.getAll();
        } else {
            return serviceDataManager.getServices(serviceType);
        } 
    }

    @Override
    public List<AppService> getAppServices(List<Service> services) throws GeneralAppException {
        List<AppService> appServices = new ArrayList<>();
        if (services != null) {
            for (Service service : services) {
                AppService appService = getAppService(service);
                appServices.add(appService);
            }
        }
        return appServices;
    }

    @Override
    public AppService getAppService(Service service) throws GeneralAppException {
        AppService appService = new AppService();
        if (service != null) {
            appService.setId(service.getId());
            appService.setDescription(service.getDescription());
            appService.setCategoryType(service.getCategoryType());
        }
        
        return appService;
    }

    @Override
    public Service getService(String serviceId) throws GeneralAppException {
        return serviceDataManager.getService(serviceId);
    }
    
}
