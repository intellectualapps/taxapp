/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import java.util.Properties;
import javax.ejb.Stateless;
import javax.mail.Session;
import com.taxmaster.model.Message;
import com.taxmaster.model.Otp;
import com.taxmaster.model.User;
import com.taxmaster.model.UserTaxService;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.pojo.AppUserTaxService;
import com.taxmaster.util.MessageType;
import com.taxmaster.util.Sendgrid;
import javax.ejb.EJB;
import org.json.JSONException;

@Stateless
public class SendGridEmailManager implements EmailManagerLocal {
    
    @EJB
    MessageManagerLocal messageManager;

    private String HOST_LOGIN_URL = "http://104.154.208.252:8080/ta/login";
    private String PAYMENT_HOST = "http://104.154.208.252:8080/ta";
    private String PAYMENT_COMPLETE_PAGE = "/firs-payment.html";
    private String PASSWORD_RESET_HOST = "http://pentor-bff-and.appspot.com/reset-password";
    private String CALLBACK = "?callback=";
    private String ENCODED_EMAIL_KEY = "&e=";
    private String PASSWORD_RESET_TOKEN = "&t=";
    private final String FROM_EMAIL = "noreply@taxapp.com";
    private final String CONTACT_US_EMAIL = "bulama@intellectualapps.com";
    
    private final String USER_EMAIL_PLACEHOLDER = "<user-email>";
    private final String USER_FIRST_NAME_PLACEHOLDER = "<users-first-name>";
    private final String HOST_PLACEHOLDER = "<host-and-callback>";
    private final String USER_OTP_PLACEHOLDER = "<user-otp>";
    private final String CLIENT_NAME_PLACEHOLDER = "<client-name>";
    private final String SERVICE_REQUESTED_PLACEHOLDER = "<service-requested>";
    private final String DATE_REQUESTED_PLACEHOLDER = "<date-requested>";
    private final String STATUS_PLACEHOLDER = "<status>";
    private final String LOGIN_URL_PLACEHOLDER = "<login-url>";
    private final String WELCOME_EMAIL_SUBJECT = "Welcome to Tax Master";
    private final String TAX_COMPLETE_SUBJECT = "Tax Complete";
    private final String PAYMENT_COMPLETE_SUBJECT = "Payment Complete";
    private final String NEW_TASK_ASSIGNED_SUBJECT = "New Task Assigned";
    private final String NEW_OTP_REQUESTED_SUBJECT = "New OTP Requested";
    private final String CONTACT_US_SUBJECT = "Contact Us";
    
    
    @Override
    public void sendVerificationEmail(User user, Otp otp, MessageType messageType) {
        Message message = messageManager.getMessage(messageType.getDescription());
        log(" Sending " + message.getId() +  " : " + user.getEmail() + " " + user.getFirstName());
        
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        String completeBody = message.getBody().replace(USER_EMAIL_PLACEHOLDER, user.getHash());
        completeBody = completeBody.replace(USER_FIRST_NAME_PLACEHOLDER, user.getFirstName());
        String link = buildEmailVerificationLink();
        completeBody = completeBody.replace(HOST_PLACEHOLDER, link);
        completeBody = completeBody.replace(USER_OTP_PLACEHOLDER, otp.getOtp());

        try {            
            Sendgrid sendGridMail = new Sendgrid("buls", "mail1mail2mail3");
            sendGridMail.setTo(user.getEmail())
                .setFrom(FROM_EMAIL)
                .setSubject(WELCOME_EMAIL_SUBJECT)
                .setText("")
                .setHtml(completeBody);
            
            sendGridMail.send();

            log(message.getId() + " sent to: " + user.getEmail());
        } catch (JSONException e) {
            log("json exception: " + e.getMessage());
        } catch (Exception e) {
            log("exception: " + e.getMessage());
        }
    }
    
    @Override
    public void sendTaxCompleteEmail(User user, MessageType messageType) {
        Message message = messageManager.getMessage(messageType.getDescription());
        log(" Sending " + message.getId() +  " : " + user.getEmail() + " " + user.getFirstName());
        
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        String completeBody = message.getBody().replace(USER_EMAIL_PLACEHOLDER, "");
        completeBody = completeBody.replace(USER_FIRST_NAME_PLACEHOLDER, user.getFirstName());
        String link = buildTaxPaymentLink();
        completeBody = completeBody.replace(HOST_PLACEHOLDER, link);

        try {            
            Sendgrid sendGridMail = new Sendgrid("buls", "mail1mail2mail3");
            sendGridMail.setTo(user.getEmail())
                .setFrom(FROM_EMAIL)
                .setSubject(TAX_COMPLETE_SUBJECT)
                .setText("")
                .setHtml(completeBody);
            
            sendGridMail.send();

            log(message.getId() + " sent to: " + user.getEmail());
        } catch (JSONException e) {
            log("json exception: " + e.getMessage());
        } catch (Exception e) {
            log("exception: " + e.getMessage());
        }
    }
    
    @Override
    public void sendPaymentConfirmation(User user, MessageType messageType) {
        user = new User();
        user.setFirstName("Bridget");
        user.setEmail("nuhuaisien@gmail.com");
        user.setHash("hash");
        Message message = messageManager.getMessage(messageType.getDescription());
        log(" Sending " + message.getId() +  " : " + user.getEmail() + " " + user.getFirstName());
        
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        String completeBody = message.getBody().replace(USER_EMAIL_PLACEHOLDER, user.getHash());
        completeBody = completeBody.replace(USER_FIRST_NAME_PLACEHOLDER, user.getFirstName());
        String link = buildTaxPaymentLink();
        completeBody = completeBody.replace(HOST_PLACEHOLDER, link);

        try {            
            Sendgrid sendGridMail = new Sendgrid("buls", "mail1mail2mail3");
            sendGridMail.setTo(user.getEmail())
                .setFrom(FROM_EMAIL)
                .setSubject(PAYMENT_COMPLETE_SUBJECT)
                .setText("")
                .setHtml(completeBody);
            
            sendGridMail.send();

            log(message.getId() + " sent to: " + user.getEmail());
        } catch (JSONException e) {
            log("json exception: " + e.getMessage());
        } catch (Exception e) {
            log("exception: " + e.getMessage());
        }
    }
    
    @Override
    public void sendTaskAssignmentConfirmation(AppUser appUser, AppUserTaxService appUserTaxService, MessageType messageType) {
       Message message = messageManager.getMessage(messageType.getDescription());
        log(" Sending " + message.getId() +  " : " + appUser.getEmail() + " " + appUser.getFirstName());
        
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        String completeBody = message.getBody().replace(USER_FIRST_NAME_PLACEHOLDER, appUser.getFirstName());
        completeBody = completeBody.replace(CLIENT_NAME_PLACEHOLDER, appUserTaxService.getEntityName());
        completeBody = completeBody.replace(SERVICE_REQUESTED_PLACEHOLDER, appUserTaxService.getServiceName());
        completeBody = completeBody.replace(DATE_REQUESTED_PLACEHOLDER, appUserTaxService.getDateRequested());
        completeBody = completeBody.replace(STATUS_PLACEHOLDER, appUserTaxService.getStatus());
        completeBody = completeBody.replace(LOGIN_URL_PLACEHOLDER, HOST_LOGIN_URL);        

        try {            
            Sendgrid sendGridMail = new Sendgrid("buls", "mail1mail2mail3");
            sendGridMail.setTo(appUser.getEmail())
                .setFrom(FROM_EMAIL)
                .setSubject(NEW_TASK_ASSIGNED_SUBJECT)
                .setText("")
                .setHtml(completeBody);
            
            sendGridMail.send();

            log(message.getId() + " sent to: " + appUser.getEmail());
        } catch (JSONException e) {
            log("json exception: " + e.getMessage());
        } catch (Exception e) {
            log("exception: " + e.getMessage());
        }
    }
    
    @Override
    public void sendNewOtpEmail(User user, String newOtp, MessageType messageType) {
        Message message = messageManager.getMessage(messageType.getDescription());
        log(" Sending " + message.getId() +  " : " + user.getEmail() + " " + user.getFirstName());
        
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        String completeBody = message.getBody().replace(USER_FIRST_NAME_PLACEHOLDER, user.getFirstName());
        completeBody = completeBody.replace(USER_OTP_PLACEHOLDER, newOtp);
        completeBody = completeBody.replace(LOGIN_URL_PLACEHOLDER, HOST_LOGIN_URL);        

        try {            
            Sendgrid sendGridMail = new Sendgrid("buls", "mail1mail2mail3");
            sendGridMail.setTo(user.getEmail())
                .setFrom(FROM_EMAIL)
                .setSubject(NEW_OTP_REQUESTED_SUBJECT)
                .setText("")
                .setHtml(completeBody);
            
            sendGridMail.send();

            log(message.getId() + " sent to: " + user.getEmail());
        } catch (JSONException e) {
            log("json exception: " + e.getMessage());
        } catch (Exception e) {
            log("exception: " + e.getMessage());
        }
    }
    
    @Override
    public void contactUs(String name, String email, String subject, String message) {
        log(" Sending " + message +  " : " + email + " " + name);
        
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);  
        String fullMessage = "FROM: " + name + "<br/><br/>EMAIL: " + email + "<br/><br/>SUBJECT: " + subject+ "<br/><br/>MESSAGE: " + message; 

        try {            
            Sendgrid sendGridMail = new Sendgrid("buls", "mail1mail2mail3");
            sendGridMail.setTo(CONTACT_US_EMAIL)
                .setFrom(FROM_EMAIL)
                .setSubject(CONTACT_US_SUBJECT)
                .setText("")
                .setHtml(fullMessage);
            
            sendGridMail.send();

            log(message + " sent to: " + email);
        } catch (JSONException e) {
            log("json exception: " + e.getMessage());
        } catch (Exception e) {
            log("exception: " + e.getMessage());
        }
    }
    
    private void log(String message) {
        System.out.println(SendGridEmailManager.class.getName() + 
                    ": " + message);
    }       
    
    private String buildEmailVerificationLink(){
        return HOST_LOGIN_URL + CALLBACK + ENCODED_EMAIL_KEY;
    }
    
    private String buildTaxPaymentLink(){
        return PAYMENT_HOST + PAYMENT_COMPLETE_PAGE;
    }
    
    private String buildPasswordResetLink(String encodedEmail, String token) {
        return PASSWORD_RESET_HOST + CALLBACK + ENCODED_EMAIL_KEY + encodedEmail
                + PASSWORD_RESET_TOKEN + token;
    }            

}
