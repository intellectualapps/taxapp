/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.User;
import com.taxmaster.model.UserTaxService;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.pojo.AppUserTaxService;
import com.taxmaster.util.MessageType;
import com.taxmaster.util.Verifier;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author buls
 */
@Stateless
public class AdminManager implements AdminManagerLocal {

    @EJB
    private UtsManagerLocal userTaxServiceManager;
    
    @EJB
    private LegalEntityManagerLocal legalEntityManager;
    
    @EJB
    private Verifier verifier;

    @EJB
    private EmailManagerLocal emailManager;    
    
    @EJB
    private UserManagerLocal userManager;    
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;    

    private final String SERVICE_STATUS_LINK = "/admin/service-status";
    private final String ASSIGN_TASK_LINK = "/admin/assign-task";

    @Override
    public AppBoolean UpdateRequest(String id, String status, String rawToken) throws GeneralAppException {
        //verify rawToken
        AppBoolean updated = userTaxServiceManager.updateStatus(id, status, rawToken);
        if (!updated.getStatus()) {
            updated = legalEntityManager.updadeIncorporationRequestStatus(id, status, rawToken);
        }
        
        return updated;
    }
    
    @Override
    public AppBoolean ConfirmPayment(String id, String status, String rawToken) throws GeneralAppException {
        emailManager.sendPaymentConfirmation(null, MessageType.PAYMENT_CONFIRMATION);
        AppBoolean appBoolean = new AppBoolean();
        appBoolean.setStatus(Boolean.TRUE);
        return appBoolean;
    }

    @Override
    public List<AppUserTaxService> getUserTaxServices(String rawToken) throws GeneralAppException {
        //return userTaxServiceManager.getUserTaxServicesAllExcludingCompletedStatus();
        return null;
    }

    @Override
    public AppBoolean assignTaskToUser(String taskId, String username, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(ASSIGN_TASK_LINK).verifyParams(taskId, username, rawToken);
        verifier.setResourceUrl(ASSIGN_TASK_LINK).verifyJwt(rawToken);
        
        AppUser adminUser = userManager.getUserDetails(username, rawToken);
        if (adminUser == null) {
            exceptionManager.throwUserDoesNotExistException(ASSIGN_TASK_LINK);
        }
        
        UserTaxService userTaxService = userTaxServiceManager.getUserTaxService(taskId);
        
        if (userTaxService == null) {
            exceptionManager.throwUserTaxServiceNotFoundException(ASSIGN_TASK_LINK);
        }
        
        userTaxService.setAssignedTo(adminUser.getUsername());
        
        Logger.getLogger(AdminManager.class.getName()).info("Updating user tax service: " + userTaxService.getId());
        
        userTaxServiceManager.update(userTaxService);
        
        AppBoolean appBoolean = new AppBoolean();
        appBoolean.setStatus(Boolean.TRUE);
        
        AppUserTaxService appUserTaxService = userTaxServiceManager.getAppUserTaxService(userTaxService);
        emailManager.sendTaskAssignmentConfirmation(adminUser, appUserTaxService, MessageType.TASK_ASSIGNMENT_CONFIRMATION);
        
        return appBoolean;
    }
    
}
