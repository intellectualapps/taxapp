/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.UserTaxServiceNoteDataManagerLocal;
import com.taxmaster.model.UserTaxService;
import com.taxmaster.model.UserTaxServiceNote;
import com.taxmaster.pojo.AppUserTaxServiceNote;
import com.taxmaster.pojo.UserTaxServiceNotePayload;
import com.taxmaster.util.CodeGenerator;
import com.taxmaster.util.DateHandler;
import com.taxmaster.util.Verifier;
import com.taxmaster.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class UserTaxServiceNoteManager implements UserTaxServiceNoteManagerLocal {
    
    @EJB
    private Verifier verifier;    
    
    @EJB
    private CodeGenerator codeGenerator;   
    
    @EJB
    private UserTaxServiceNoteDataManagerLocal userTaxServiceNoteDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionThrowerManager; 
    
    private final String USER_TAX_SERVICE_NOTE_RESOURCE = "/service/note";

    @Override
    public AppUserTaxServiceNote createUserTaxServiceNote(String userTaxServiceId, String noteText, String rawToken) throws GeneralAppException {
        Claims claims = verifier.setResourceUrl(USER_TAX_SERVICE_NOTE_RESOURCE)
                .verifyParams(userTaxServiceId, noteText, rawToken)
                .verifyJwt(rawToken);
        
        String username = claims.getSubject();
        
        UserTaxServiceNote userTaxServiceNote = new UserTaxServiceNote();
        userTaxServiceNote.setId(codeGenerator.getToken());
        userTaxServiceNote.setUserTaxServiceId(userTaxServiceId);
        userTaxServiceNote.setText(noteText);
        userTaxServiceNote.setUsername(username);
        userTaxServiceNote.setDateAdded(new Date());
        userTaxServiceNoteDataManager.create(userTaxServiceNote);
        
        AppUserTaxServiceNote appUserTaxServiceNote = getAppUserTaxServiceNote(userTaxServiceNote);
        
        return appUserTaxServiceNote;        
    }

    @Override
    public void delete(UserTaxService userTaxService) throws GeneralAppException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UserTaxService> getUserTaxServiceNotesByUserTaxServiceId(String userTaxServiceId) throws GeneralAppException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserTaxService updateNote(String id, String noteText) throws GeneralAppException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public UserTaxServiceNotePayload getUserTaxServiceNotes(String userTaxServiceId, String pageNumber, String pageSize, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(USER_TAX_SERVICE_NOTE_RESOURCE)
                .verifyParams(userTaxServiceId, pageNumber, pageSize, rawToken)
                .verifyJwt(rawToken);
        int pageNumberInt = 0;
        int pageSizeInt = 0;
        try {
            pageNumberInt = Integer.parseInt(pageNumber);
            pageSizeInt = Integer.parseInt(pageSize);
        } catch (NumberFormatException nfe) {
            exceptionThrowerManager.throwInvaidIntegerException(USER_TAX_SERVICE_NOTE_RESOURCE);
        }
        
        List<UserTaxServiceNote> userTaxServiceNotes = userTaxServiceNoteDataManager
                .getUserTaxServiceNotesByUserTaxServiceId(userTaxServiceId);
        List<AppUserTaxServiceNote> appUserTaxServiceNotes = getAppUserTaxServiceNotes(userTaxServiceNotes);
        
        UserTaxServiceNotePayload userTaxServiceNotePayload = new UserTaxServiceNotePayload();
        userTaxServiceNotePayload.setPageNumber(pageNumberInt);
        userTaxServiceNotePayload.setPageSize(pageSizeInt);
        userTaxServiceNotePayload.setTotalCount(userTaxServiceNotes.size());
        userTaxServiceNotePayload.setNotes(appUserTaxServiceNotes);
        
        return userTaxServiceNotePayload;
    }
    
    
    private List<AppUserTaxServiceNote> getAppUserTaxServiceNotes(List<UserTaxServiceNote> userTaxServiceNotes) {
        List<AppUserTaxServiceNote> appUserTaxServiceNotes = new ArrayList<>();
        for (UserTaxServiceNote userTaxServiceNote : userTaxServiceNotes) {
            AppUserTaxServiceNote appUserTaxServiceNote = getAppUserTaxServiceNote(userTaxServiceNote);
            appUserTaxServiceNotes.add(appUserTaxServiceNote);
        }
        
        return appUserTaxServiceNotes;
    }
    
    private AppUserTaxServiceNote getAppUserTaxServiceNote(UserTaxServiceNote userTaxServiceNote) {
        AppUserTaxServiceNote appUserTaxServiceNote = new AppUserTaxServiceNote();
        if (userTaxServiceNote != null) {
            appUserTaxServiceNote.setId(userTaxServiceNote.getId());
            appUserTaxServiceNote.setUserTaxServiceId(userTaxServiceNote.getUserTaxServiceId());
            appUserTaxServiceNote.setText(userTaxServiceNote.getText());
            appUserTaxServiceNote.setUsername(userTaxServiceNote.getUsername());
            appUserTaxServiceNote.setDateAdded(DateHandler.formatDateFullWithTime(userTaxServiceNote.getDateAdded()));
        }
        
        return appUserTaxServiceNote;
    }
        
}
