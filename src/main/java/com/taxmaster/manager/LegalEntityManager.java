/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.LegalEntityDataManagerLocal;
import com.taxmaster.data.manager.UserDataManagerLocal;
import com.taxmaster.model.LegalEntity;
import com.taxmaster.model.IncorporationRequest;
import com.taxmaster.model.Industry;
import com.taxmaster.model.LegalEntityType;
import com.taxmaster.model.RevenueRange;
import com.taxmaster.model.Service;
import com.taxmaster.model.State;
import com.taxmaster.model.User;
import com.taxmaster.model.UserLegalEntity;
import com.taxmaster.model.UserTaxService;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppLegalEntity;
import com.taxmaster.pojo.AppIncorporationRequest;
import com.taxmaster.pojo.ServiceRequestSummaryPayload;
import com.taxmaster.util.CodeGenerator;
import com.taxmaster.util.DateHandler;
import com.taxmaster.util.IncorporationRequestStatusType;
import com.taxmaster.util.IndustryType;
import com.taxmaster.util.LegalEntityCategory;
import com.taxmaster.util.MessageType;
import com.taxmaster.util.RoleType;
import com.taxmaster.util.ServiceStatusType;
import com.taxmaster.util.ServiceType;
import com.taxmaster.util.Verifier;
import com.taxmaster.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class LegalEntityManager implements LegalEntityManagerLocal {

    @EJB
    private CodeGenerator codeGenerator;    
    
    @EJB
    private Verifier verifier;    
    
    @EJB
    private LegalEntityDataManagerLocal legalEntityDataManager;  
    
    @EJB
    private UserDataManagerLocal userManager;  
    
    @EJB
    private EmailManagerLocal emailManager;  
    
    @EJB
    private LookupManagerLocal lookupManager;  
    
    @EJB
    private UserLegalEntityManagerLocal userLegalEntityManager;  
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionThrowerManager;      
    
    private String ADD_BUSINESS_RESOURCE = "/business/legalEntity/tax";
    private String ADD_COMPANY_RESOURCE = "/business/legalEntity/incorporation-request";
    private String ENTITY_EXISTS_RESOURCE = "/business/exists";
    private String INCORPORATION_REQUEST_EXISTS_RESOURCE = "/business/incorporation-request/exists";    
    
    @Override
    public AppLegalEntity create(String rawToken, String name, String address, String typeId, String industryId, 
            String incorporationDate, String tin, String commencementDate, 
            String revenueRangeId, String stateId, String addressLine1, 
            String addressLine2, String city) throws EJBTransactionRolledbackException, GeneralAppException {
        
        verifier.setResourceUrl(ADD_BUSINESS_RESOURCE).verifyParams(rawToken, address, stateId);
        
        Claims claims = verifier.setResourceUrl(ADD_BUSINESS_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        User user = userManager.get(username);
        if (user == null) {
            exceptionThrowerManager.throwUserDoesNotExistException(ADD_BUSINESS_RESOURCE);
        }
        
        if (name == null || name.isEmpty()) {
            name = user.getFirstName().concat(" ").concat(user.getLastName());
            typeId = LegalEntityCategory.INDIVIDUAL.getDescription();
            industryId = IndustryType.OTHER.getDescription();
            DateFormat d = new SimpleDateFormat("dd-MM-yyyy");
            Date today = new Date();
            commencementDate = d.format(today);
            incorporationDate = d.format(today);
        }
        
        LegalEntity legalEntity = new LegalEntity();
        legalEntity.setId(codeGenerator.getToken());        
        legalEntity.setName(name);
        legalEntity.setAddress(address);
        legalEntity.setTypeId(typeId);
        legalEntity.setIndustryId(industryId);
        legalEntity.setRevenueRangeId(revenueRangeId);
        legalEntity.setTin(tin);
        legalEntity.setStateId(stateId);
        legalEntity.setAddressLine1(addressLine1);
        legalEntity.setAddressLine2(addressLine2);
        legalEntity.setCity(city);
        
        Date legalEntityCommencementDate = DateHandler.getDateDayMonthYear(commencementDate);
        Date legalEntityIncorporationDate = DateHandler.getDateDayMonthYear(incorporationDate);
        
        legalEntity.setCommencementDate(legalEntityCommencementDate);
        legalEntity.setIncorporationDate(legalEntityIncorporationDate);
        
        legalEntityDataManager.create(legalEntity);
        
        UserLegalEntity userLegalEntity = new UserLegalEntity();
        userLegalEntity.setLegalEntityId(legalEntity.getId());
        userLegalEntity.setUsername(username);
        
        userLegalEntityManager.create(userLegalEntity);
        
        AppLegalEntity appLegalEntity = getAppLegalEntity(legalEntity);
        
        return appLegalEntity;
    }
    
    @Override
    public AppLegalEntity getAppLegalEntity(LegalEntity legalEntity) throws GeneralAppException {
        AppLegalEntity appLegalEntity = new AppLegalEntity();
        if (legalEntity != null) {
            appLegalEntity.setId(legalEntity.getId());
            appLegalEntity.setName(legalEntity.getName());
            appLegalEntity.setAddress(legalEntity.getAddress());
            appLegalEntity.setTypeId(legalEntity.getTypeId());
            appLegalEntity.setIndustryId(legalEntity.getIndustryId());
            appLegalEntity.setRevenueRangeId(legalEntity.getRevenueRangeId());
            appLegalEntity.setCommencementDate(DateHandler.formatDate(legalEntity.getCommencementDate()));
            appLegalEntity.setIncorporationDate(DateHandler.formatDate(legalEntity.getIncorporationDate()));
            appLegalEntity.setStateId(legalEntity.getStateId());
            appLegalEntity.setTin(legalEntity.getTin());
            appLegalEntity.setAddressLine1(legalEntity.getAddressLine1());
            appLegalEntity.setAddressLine2(legalEntity.getAddressLine2());
            appLegalEntity.setCity(legalEntity.getCity());
            
            List<UserLegalEntity> userLegalEntities = userLegalEntityManager.getLegalEntitiesByEntity(legalEntity.getId());
            if (!userLegalEntities.isEmpty()) {
                String username = userLegalEntities.get(0).getUsername();
                User user = userManager.get(username);
                if (user != null) {
                    appLegalEntity.setContactPersonFullName(user.getFirstName() + " " + user.getLastName());
                }
            }
            
            LegalEntityType legalEntityType = lookupManager.getLegalEntityType(appLegalEntity.getTypeId());            
            if (legalEntityType != null) {
                appLegalEntity.setTypeName(legalEntityType.getDescription());
            }
            
            Industry industry = lookupManager.getIndustry(appLegalEntity.getIndustryId());
            if (industry != null) {
                 appLegalEntity.setIndustryName(industry.getDescription());
            }
            
            State state = lookupManager.getState(appLegalEntity.getStateId());
            if (state != null) {
                 appLegalEntity.setStateName((state.getState()));
            }
            
            RevenueRange revenueRange = lookupManager.getRevenueRange(appLegalEntity.getRevenueRangeId());
            if (revenueRange != null) {
                 appLegalEntity.setRevenueRangeName((revenueRange.getDescription()));
            }
            
        }
        
        return appLegalEntity;
    }
    
    @Override
    public AppIncorporationRequest createIncorporationRequest(String rawToken, String proposedCompanyNameOne, String proposedCompanyNameTwo, String proposedCompanyNameThree,
            String proposedCompanyNameFour, String companyAddress, String companyIndustry) throws EJBTransactionRolledbackException, GeneralAppException {
        verifier.setResourceUrl(ADD_COMPANY_RESOURCE).verifyParams(rawToken, proposedCompanyNameOne, proposedCompanyNameTwo,
                proposedCompanyNameThree, proposedCompanyNameFour, companyAddress, companyIndustry);
        
        Claims claims = verifier.setResourceUrl(ADD_COMPANY_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        
        IncorporationRequest incorporationRequest = new IncorporationRequest();
        incorporationRequest.setId(codeGenerator.getToken());
        incorporationRequest.setProposedCompanyNameOne(proposedCompanyNameOne);
        incorporationRequest.setProposedCompanyNameTwo(proposedCompanyNameTwo);
        incorporationRequest.setProposedCompanyNameThree(proposedCompanyNameThree);
        incorporationRequest.setProposedCompanyNameFour(proposedCompanyNameFour);
        incorporationRequest.setCompanyAddress(companyAddress);
        incorporationRequest.setCompanyIndustry(companyIndustry);
        incorporationRequest.setUsername(username);
        incorporationRequest.setStatus(IncorporationRequestStatusType.PENDING.getDescription());
        incorporationRequest.setServiceId(ServiceType.COMPANY_INCORPORATION.getDescription());
        
        Date companyRequestDate = DateHandler.getNow();
        incorporationRequest.setRequestDate(companyRequestDate);
        
        IncorporationRequest legalEntityIncorporationRequest = legalEntityDataManager.createIncorporationRequest(incorporationRequest);
        
        AppIncorporationRequest appIncorporationRequest = getAppIncorporationRequest(legalEntityIncorporationRequest); 
        
        return appIncorporationRequest;
    
    }

    
    @Override
    public AppIncorporationRequest getAppIncorporationRequest(IncorporationRequest incorporationRequest) {
        AppIncorporationRequest appIncorporationRequest = new AppIncorporationRequest();
        appIncorporationRequest.setProposedCompanyNameOne(incorporationRequest.getProposedCompanyNameOne());
        appIncorporationRequest.setProposedCompanyNameTwo(incorporationRequest.getProposedCompanyNameTwo());
        appIncorporationRequest.setProposedCompanyNameThree(incorporationRequest.getProposedCompanyNameThree());
        appIncorporationRequest.setProposedCompanyNameFour(incorporationRequest.getProposedCompanyNameFour());
        appIncorporationRequest.setCompanyAddress(incorporationRequest.getCompanyAddress());
        appIncorporationRequest.setCompanyIndustry(incorporationRequest.getCompanyIndustry());
        
        return appIncorporationRequest;
        
    }

    @Override
    public LegalEntity getLegalEntity(String legalEntityId) {
        LegalEntity legalEntity = new LegalEntity();
        legalEntity = legalEntityDataManager.get(legalEntityId);
            
        return legalEntity;
 
    }

    @Override
    public AppBoolean hasLegalEntity(String rawToken) throws GeneralAppException{
        verifier.setResourceUrl(ENTITY_EXISTS_RESOURCE).verifyParams(rawToken);
        
        Claims claims = verifier.setResourceUrl(ENTITY_EXISTS_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        User user = userManager.get(username);
        if (user == null) {
            exceptionThrowerManager.throwUserDoesNotExistException(ENTITY_EXISTS_RESOURCE);
        }
        
        List<UserLegalEntity> userLegalEntities = userLegalEntityManager.getLegalEntitiesByUsername(username);
        
        AppBoolean appBoolean = new AppBoolean();
        if (userLegalEntities.isEmpty()) {
            appBoolean.setStatus(Boolean.FALSE);
        } else {
            appBoolean.setStatus(Boolean.TRUE);
        }
        
        return appBoolean;        
    }

    @Override
    public AppBoolean hasIncorporationRequest(String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(INCORPORATION_REQUEST_EXISTS_RESOURCE).verifyParams(rawToken);
        
        Claims claims = verifier.setResourceUrl(INCORPORATION_REQUEST_EXISTS_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        User user = userManager.get(username);
        if (user == null) {
            exceptionThrowerManager.throwUserDoesNotExistException(INCORPORATION_REQUEST_EXISTS_RESOURCE);
        }
        
        List<IncorporationRequest> userIncorporationRequests = userLegalEntityManager.getIncorporationRequestByUsername(username);
        
        AppBoolean appBoolean = new AppBoolean();
        if (userIncorporationRequests.isEmpty()) {
            appBoolean.setStatus(Boolean.FALSE);
        } else {
            appBoolean.setStatus(Boolean.TRUE);
        }
        
        return appBoolean;        
    }

    @Override
    public List<IncorporationRequest> getAllIncorporationRequestsExcludingCompletedStatus() {
        return legalEntityDataManager.getAllIncorporationRequestsExcludingCompletedStatus();
    }

    @Override
    public List<IncorporationRequest> getIncorporationRequestsByUsername(String username) {
        return legalEntityDataManager.getIncorporationRequestsByUsername(username);
    }

    @Override
    public IncorporationRequest getIncorporationRequest(String id) {
        return legalEntityDataManager.getIncorporationRequest(id);
    }

    @Override
    public AppBoolean updadeIncorporationRequestStatus(String id, String status, String rawToken) {
        AppBoolean appBoolean = new AppBoolean();
        Boolean sendCompletedEmail = Boolean.FALSE;
        IncorporationRequest incorporationRequest = legalEntityDataManager.getIncorporationRequest(id);
        if (incorporationRequest != null) {
            if (status.equals(ServiceStatusType.COMPLETED.getDescription())) {
                incorporationRequest.setStatus(ServiceStatusType.COMPLETED.getDescription());
                sendCompletedEmail = Boolean.TRUE;
            } else if (status.equals(ServiceStatusType.IN_PROCESS.getDescription())) {
                incorporationRequest.setStatus(ServiceStatusType.IN_PROCESS.getDescription());
            } else if (status.equals(ServiceStatusType.PENDING.getDescription())) {
                incorporationRequest.setStatus(ServiceStatusType.PENDING.getDescription());
            }
            legalEntityDataManager.updateIncorporationRequest(incorporationRequest);
            appBoolean.setStatus(Boolean.TRUE);
            if (sendCompletedEmail) {
                User user = userManager.get(incorporationRequest.getUsername());
                emailManager.sendTaxCompleteEmail(user, MessageType.TAX_COMPLETE);
            }
        } else {
            appBoolean.setStatus(Boolean.FALSE);
            return appBoolean;
        }
        
        return appBoolean;
    }

    @Override
    public List<LegalEntity> getByEntityName(String searchTerm) {
        return legalEntityDataManager.getByLegalEntityName(searchTerm);
    }
    
}

