/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.CitPaymentPlan;
import com.taxmaster.model.UserTaxService;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppPaymentPlan;
import com.taxmaster.pojo.AppUserTaxService;
import com.taxmaster.pojo.IncorporationPaymentPlanPayload;
import com.taxmaster.pojo.ServiceRequestSummaryPayload;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.Local;

@Local
public interface UserTaxServManagerLocal {

    AppUserTaxService createUserTaxService(String entityId, String serviceId, 
            String paymentPlanId, String revenueRangeId, String year, String rawToken) throws GeneralAppException;

    AppBoolean updateStatus(String id, String status, String rawToken) throws GeneralAppException;

    void delete(UserTaxService userTaxService);
    
    List<UserTaxService> getUserTaxServicesByUsername(String username);           
    
    UserTaxService getUserTaxServicesById(String id);           
    
    UserTaxService update(UserTaxService userTaxService);           
    
    ServiceRequestSummaryPayload getServiceRequestSummaries(String pageNumber, String pageSize, String rawToken) throws GeneralAppException;

    ServiceRequestSummaryPayload getServiceRequestSummariesForUser(String pageNumber, String pageSize, String rawToken) throws GeneralAppException;
    
}
