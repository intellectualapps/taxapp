/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.LookupDataManagerLocal;
import com.taxmaster.model.LegalEntityType;
import com.taxmaster.model.CompanyIncomeTaxFilingPeriod;
import com.taxmaster.model.ContactFormSubject;
import com.taxmaster.model.Industry;
import com.taxmaster.model.RevenueRange;
import com.taxmaster.model.Service;
import com.taxmaster.model.State;
import com.taxmaster.model.User;
import com.taxmaster.pojo.AppServiceStatusType;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.util.ServiceStatusType;
import com.taxmaster.util.ServiceType;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class LookupManager implements LookupManagerLocal {

    @EJB
    private LookupDataManagerLocal lookupDataManager;

    @EJB
    UserManagerLocal userManager;

    @Override
    public List<Industry> getIndustries() {
        return lookupDataManager.getIndustries();
    }

    @Override
    public List<LegalEntityType> getLegalEntityTypes() {
        return lookupDataManager.getLegalEntityTypes();
    }

    @Override
    public List<RevenueRange> getRevenueRanges() {
        return lookupDataManager.getRevenueRanges();
    }

    @Override
    public List<CompanyIncomeTaxFilingPeriod> getCompanyIncomeTaxFilingPeriod() {
        return lookupDataManager.getCompanyIncomeTaxFilingPeriod();
    }

    @Override
    public List<State> getAllStates() {
        return lookupDataManager.getAllStates();
    }

    @Override
    public List<ContactFormSubject> getAllSubject() {
        return lookupDataManager.getAllSubjects();
    }

    @Override
    public Industry getIndustry(String id) {
        return lookupDataManager.getIndustry(id);
    }

    @Override
    public RevenueRange getRevenueRange(String id) {
        return lookupDataManager.getRevenueRange(id);
    }

    @Override
    public State getState(String id) {
        return lookupDataManager.getState(id);
    }

    @Override
    public LegalEntityType getLegalEntityType(String id) {
        return lookupDataManager.getLegalEntityType(id);
    }

    @Override
    public List<Service> getServices() {
        List<Service> servicesToReturn = new ArrayList<Service>();

        List<Service> services = lookupDataManager.getAllServices();
        for (Service service : services) {
            if (!service.getId().equals(ServiceType.OBTAIN_TIN.getDescription())) {
                servicesToReturn.add(service);
            }
        }

        return servicesToReturn;
    }

    @Override
    public List<AppServiceStatusType> getServiceStatuses() {
        List<AppServiceStatusType> appServiceStatusTypes = new ArrayList<AppServiceStatusType>();

        AppServiceStatusType completed = new AppServiceStatusType();
        completed.setId(ServiceStatusType.COMPLETED.getDescription());
        completed.setDescription(ServiceStatusType.COMPLETED.getDescription());
        appServiceStatusTypes.add(completed);

        AppServiceStatusType pending = new AppServiceStatusType();
        pending.setId(ServiceStatusType.PENDING.getDescription());
        pending.setDescription(ServiceStatusType.PENDING.getDescription());
        appServiceStatusTypes.add(pending);

        AppServiceStatusType inProcess = new AppServiceStatusType();
        inProcess.setId(ServiceStatusType.IN_PROCESS.getDescription());
        inProcess.setDescription(ServiceStatusType.IN_PROCESS.getDescription());
        appServiceStatusTypes.add(inProcess);

        return appServiceStatusTypes;
    }

    @Override
    public List<AppUser> getUsersByRole(String role) {
        List<User> userList = lookupDataManager.getUsersByRole(role);
        List<AppUser> appUserList = userManager.getAppUsers(userList);

        return appUserList;
    }

}
