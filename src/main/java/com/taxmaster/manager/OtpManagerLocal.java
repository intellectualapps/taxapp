/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.Otp;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface OtpManagerLocal {
    
    Otp create(Otp otp) throws EJBTransactionRolledbackException;
    
    Otp get(String username);

    void delete(Otp otp);
}
