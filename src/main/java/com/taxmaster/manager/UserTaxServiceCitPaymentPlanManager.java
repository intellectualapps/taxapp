/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.UserTaxServiceCitPaymentPlanDataManagerLocal;
import com.taxmaster.model.UserTaxServiceCitPaymentPlan;
import com.taxmaster.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class UserTaxServiceCitPaymentPlanManager implements UserTaxServiceCitPaymentPlanManagerLocal {

    @EJB
    private UserTaxServiceCitPaymentPlanDataManagerLocal userTaxServiceCitPaymentPlanDataManager;    

    @Override
    public UserTaxServiceCitPaymentPlan create(UserTaxServiceCitPaymentPlan userTaxServiceCitPaymentPlan) throws GeneralAppException {
        return userTaxServiceCitPaymentPlanDataManager.create(userTaxServiceCitPaymentPlan);
    }

    @Override
    public UserTaxServiceCitPaymentPlan get(String userTaxServiceId) throws GeneralAppException {
        return userTaxServiceCitPaymentPlanDataManager.get(userTaxServiceId);
    }

}

