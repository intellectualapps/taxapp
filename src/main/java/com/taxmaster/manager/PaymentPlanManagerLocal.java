/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.CitPaymentPlan;
import com.taxmaster.model.PaymentPlan;
import com.taxmaster.pojo.AppPaymentPlan;
import com.taxmaster.pojo.IncorporationPaymentPlanPayload;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.Local;

@Local
public interface PaymentPlanManagerLocal {

    List<AppPaymentPlan> getPaymentPlans(String serviceId, String rawToken) throws GeneralAppException;
    
    List<AppPaymentPlan> getPaymentPlans(String serviceId) throws GeneralAppException;
    
    IncorporationPaymentPlanPayload getIncorporationPaymentPlans(String rawToken) throws GeneralAppException;
    
    List<AppPaymentPlan> getCitPaymentPlans(String businessId, String year, String rawToken) throws GeneralAppException;
    
    CitPaymentPlan createCitPaymentPlan(CitPaymentPlan citPaymentPlan) throws GeneralAppException;
    
    PaymentPlan getFlatRatePaymentPlan(String serviceId) throws GeneralAppException;

}
