/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.Service;
import com.taxmaster.pojo.AppService;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author RAYNOLD
 */

@Local
public interface ServiceManagerLocal {
    
    List<Service> getServices(String serviceType) throws GeneralAppException;
    
    Service getService(String serviceId) throws GeneralAppException;
    
    List<AppService> getAppServices(List<Service> services) throws GeneralAppException;
    
    AppService getAppService(Service service) throws GeneralAppException;
    
}
