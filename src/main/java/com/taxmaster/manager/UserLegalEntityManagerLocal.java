/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.IncorporationRequest;
import com.taxmaster.model.UserLegalEntity;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface UserLegalEntityManagerLocal {
    
    UserLegalEntity create(UserLegalEntity user) throws EJBTransactionRolledbackException;  
    
    List<UserLegalEntity> getLegalEntitiesByUsername(String username) throws EJBTransactionRolledbackException; 
    
    List<UserLegalEntity> getLegalEntitiesByEntity(String entityId) throws EJBTransactionRolledbackException; 
    
    List<IncorporationRequest> getIncorporationRequestByUsername(String username) throws EJBTransactionRolledbackException; 
    
}
