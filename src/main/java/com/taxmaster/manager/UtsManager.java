/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.DiscountCouponDataManagerLocal;
import com.taxmaster.data.manager.UserDataManagerLocal;
import com.taxmaster.data.manager.UserTaxServiceDataManagerLocal;
import com.taxmaster.model.DiscountCoupon;
import com.taxmaster.model.IncorporationRequest;
import com.taxmaster.model.LegalEntity;
import com.taxmaster.model.PaymentPlan;
import com.taxmaster.model.Service;
import com.taxmaster.model.ServiceCharge;
import com.taxmaster.model.User;
import com.taxmaster.model.UserLegalEntity;
import com.taxmaster.model.UserTaxService;
import com.taxmaster.model.UserTaxServiceCitPaymentPlan;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppIncorporationRequest;
import com.taxmaster.pojo.AppLegalEntity;
import com.taxmaster.pojo.AppPaymentPlan;
import com.taxmaster.pojo.AppServiceCharge;
import com.taxmaster.pojo.AppServiceRequestSummary;
import com.taxmaster.pojo.AppUserTaxService;
import com.taxmaster.pojo.ServiceRequestSummaryPayload;
import com.taxmaster.util.CodeGenerator;
import com.taxmaster.util.DateHandler;
import com.taxmaster.util.MessageType;
import com.taxmaster.util.RoleType;
import com.taxmaster.util.ServiceStatusType;
import com.taxmaster.util.ServiceType;
import com.taxmaster.util.TaxStatusType;
import com.taxmaster.util.Verifier;
import com.taxmaster.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class UtsManager implements UtsManagerLocal {
    
    @EJB
    private UserTaxServiceDataManagerLocal userTaxServiceDataManager;    
    
    @EJB
    private Verifier verifier;    
    
    @EJB
    private CodeGenerator codeGenerator;    
    
    @EJB
    private UserDataManagerLocal userManager;
    
    @EJB
    private EmailManagerLocal emailManager;
    
    @EJB
    private UserTaxServiceCitPaymentPlanManagerLocal userTaxServiceCitPaymentPlanManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionThrowerManager;  
    
    @EJB
    private ServiceManagerLocal serviceManager;
    
    @EJB
    private LegalEntityManagerLocal legalEntityManager;
    
    @EJB
    private UserLegalEntityManagerLocal userLegalEntityManager;
    
    @EJB
    private PaymentPlanManagerLocal paymentPlanManager;
    
    @EJB
    private ServiceChargeManagerLocal serviceChargeManager;
    
    @EJB
    private DiscountCouponDataManagerLocal discountCouponDataManager;
    
    private String TAX_SERVICE_RESOURCE = "/service/tax";
    private String SAVE_DOCUMENT_URL_RESOURCE = TAX_SERVICE_RESOURCE + "/document-url";
    private String TAX_SERVICE_DISCOUNT_RESOURCE = "/service/tax/discount";
    
    private String REVENUE_RANGE_1 = "a";
    private String REVENUE_RANGE_2 = "b";
    private String REVENUE_RANGE_3 = "c";
    private String REVENUE_RANGE_4 = "d";
    private String REVENUE_RANGE_5 = "e";
    private String REVENUE_RANGE_6 = "f";
    private int DELIVERY_CHARGE = 1500;
    private String THIS_CLASS = UtsManager.class.getName();

    @Override
    public void nothing() throws GeneralAppException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AppUserTaxService createUserTaxService(String entityId, String serviceId, 
            String paymentPlanId, String revenueRangeId, String year, String citFiledBeforeStatus, 
            String citVatFiledBeforeStatus, String vatFiledBeforeStatus, String tccExpiryYear, String lastVatFiledDate, String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(serviceId);            
        
        if (serviceId.equals(ServiceType.COMPANY_INCOME_TAX.getDescription()) || serviceId.equals(ServiceType.PERSONAL_INCOME_TAX.getDescription())) {
            Logger.getLogger(THIS_CLASS).info("************: " + entityId + " " + serviceId + " " + revenueRangeId + " " + citVatFiledBeforeStatus + " " + tccExpiryYear);
            verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(entityId, 
                    revenueRangeId, year, rawToken);            
        } else if (serviceId.equals(ServiceType.VALUED_ADDED_TAX.getDescription())) {
            Logger.getLogger(THIS_CLASS).info("Entity Id: " + entityId + " service id: " + serviceId + " vat filed before status: " + vatFiledBeforeStatus + " last vat field date: " + lastVatFiledDate);
            verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(entityId, 
                    vatFiledBeforeStatus, rawToken);            
        } else if (serviceId.equals(ServiceType.PERSONAL_INCOME_TAX_PAYE.getDescription())) {
            Logger.getLogger(THIS_CLASS).info("Entity Id: " + entityId + " service id: " + serviceId);
            verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(entityId, serviceId, paymentPlanId);            
        } else {
            verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(entityId, serviceId, rawToken);
        }
        
        Claims claims = verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        User user = userManager.get(username);
        if (user == null) {
            exceptionThrowerManager.throwUserDoesNotExistException(TAX_SERVICE_RESOURCE);
        }
        
        UserTaxService userTaxService = new UserTaxService();
        userTaxService.setId(codeGenerator.getToken());
        userTaxService.setBusinessId(entityId);
        userTaxService.setServiceId(serviceId);
        userTaxService.setUsername(username);
        userTaxService.setStatus(ServiceStatusType.PENDING.getDescription());
        userTaxService.setDateRequested(DateHandler.getNow());
        userTaxService.setFromDate(DateHandler.getNow());
        userTaxService.setToDate(DateHandler.getNow());
        userTaxService.setCitFiledBeforeStatus(citFiledBeforeStatus);
        userTaxService.setCitVatFiledBeforeStatus(citVatFiledBeforeStatus);
        userTaxService.setVatFiledBeforeStatus(vatFiledBeforeStatus);
        Calendar now = Calendar.getInstance();
        
        if (serviceId.equals(ServiceType.COMPANY_INCOME_TAX.getDescription()) || serviceId.equals(ServiceType.PERSONAL_INCOME_TAX.getDescription())) {
            try {
                int tccExpiryYearValue = Integer.parseInt(tccExpiryYear);
                now.set(Calendar.YEAR, tccExpiryYearValue);
                userTaxService.setTccExpiryYear(now.getTime());
            } catch (NumberFormatException nfe) {
                //TODO exceptionThrowerManager.
                now.set(Calendar.YEAR, 1971);
                userTaxService.setTccExpiryYear(now.getTime());
            }
        } else {
            now.set(Calendar.YEAR, 1971);
            userTaxService.setTccExpiryYear(now.getTime());
        }
        
        if (lastVatFiledDate != null && !lastVatFiledDate.isEmpty()) {
            Date lastVatFiledDateValue = DateHandler.getDateDayMonthYear(lastVatFiledDate);
            userTaxService.setLastVatFiledDate(lastVatFiledDateValue);
        } else {
            Calendar placeHolderDate = Calendar.getInstance();
            placeHolderDate.set(Calendar.DATE, 1);
            placeHolderDate.set(Calendar.MONTH, 0);
            placeHolderDate.set(Calendar.YEAR, 1971);
            userTaxService.setLastVatFiledDate(placeHolderDate.getTime());
        }
        
        userTaxService = userTaxServiceDataManager.create(userTaxService);
        
        if (serviceId.equals(ServiceType.COMPANY_INCOME_TAX.getDescription()) || serviceId.equals(ServiceType.PERSONAL_INCOME_TAX.getDescription())) {
            UserTaxServiceCitPaymentPlan userTaxServiceCitPaymentPlan = new UserTaxServiceCitPaymentPlan();
            userTaxServiceCitPaymentPlan.setRevenueRange(revenueRangeId);
            userTaxServiceCitPaymentPlan.setYear(year);
            userTaxServiceCitPaymentPlan.setUserTaxServiceId(userTaxService.getId());
            
            userTaxServiceCitPaymentPlanManager.create(userTaxServiceCitPaymentPlan);
        } else {
            userTaxService.setNonCitPaymentPlan(paymentPlanId);
        }
        
        ServiceCharge serviceCharge = new ServiceCharge();
        
        if (serviceId.equals(ServiceType.COMPANY_INCOME_TAX.getDescription()) || serviceId.equals(ServiceType.PERSONAL_INCOME_TAX.getDescription())) {
            serviceCharge = computeChargesForEntity(userTaxService.getId(), entityId, serviceId, revenueRangeId, citFiledBeforeStatus, citVatFiledBeforeStatus, tccExpiryYear, lastVatFiledDate);    
        } else if (serviceId.equals(ServiceType.VALUED_ADDED_TAX.getDescription())) {
            serviceCharge = computeStandAloneVatChargesForEntity(userTaxService.getId(), entityId, serviceId, revenueRangeId, vatFiledBeforeStatus, lastVatFiledDate, paymentPlanId);
            System.out.println("SINGLE VAT COMPUTATION GOES HERE: ");
        } else if (serviceId.equals(ServiceType.PERSONAL_INCOME_TAX_PAYE.getDescription())) {
            serviceCharge = computeChargesForPaye(userTaxService.getId(), entityId, serviceId, paymentPlanId);
        }
                
        AppServiceCharge appServiceCharge = getAppServiceCharge(serviceCharge);
        
        AppUserTaxService appUserTaxService = getAppUserTaxService(userTaxService);
        appUserTaxService.setServiceCharge(appServiceCharge);
        
        return appUserTaxService;
    }

    @Override
    public AppBoolean updateStatus(String id, String status, String rawToken) throws GeneralAppException {
        AppBoolean appBoolean = new AppBoolean();
        Boolean sendCompletedEmail = Boolean.FALSE;
        UserTaxService userTaxService = userTaxServiceDataManager.get(id);
        if (userTaxService != null) {
            if (status.equals(ServiceStatusType.COMPLETED.getDescription())) {
                userTaxService.setStatus(ServiceStatusType.COMPLETED.getDescription());
                sendCompletedEmail = Boolean.TRUE;
            } else if (status.equals(ServiceStatusType.IN_PROCESS.getDescription())) {
                userTaxService.setStatus(ServiceStatusType.IN_PROCESS.getDescription());
            } else if (status.equals(ServiceStatusType.PENDING.getDescription())) {
                userTaxService.setStatus(ServiceStatusType.PENDING.getDescription());
            }
            userTaxServiceDataManager.update(userTaxService);
            appBoolean.setStatus(Boolean.TRUE);
            
            if (sendCompletedEmail) {
                User user = userManager.get(userTaxService.getUsername());
                emailManager.sendTaxCompleteEmail(user, MessageType.TAX_COMPLETE);
            }
        } else {
            appBoolean.setStatus(Boolean.FALSE);
            return appBoolean;
        }
        
        return appBoolean;
    }

    @Override
    public void delete(UserTaxService userTaxService) {
        userTaxServiceDataManager.delete(userTaxService);
    }

    @Override
    public List<UserTaxService> getUserTaxServicesByUsername(String username) {                
        return userTaxServiceDataManager.getUserTaxServicesByUsername(username);
    }
    
    @Override
    public AppUserTaxService getAppUserTaxService(UserTaxService userTaxService) throws GeneralAppException {
        AppUserTaxService appUserTaxService = new AppUserTaxService();
        
        if (userTaxService == null) {
            return appUserTaxService;
        }
        Service service = serviceManager.getService(userTaxService.getServiceId()); 
        LegalEntity entity = legalEntityManager.getLegalEntity(userTaxService.getBusinessId());
        if (service != null && entity != null) {
            appUserTaxService.setId(userTaxService.getId());
            appUserTaxService.setUsername(userTaxService.getUsername());
            appUserTaxService.setBusinessId(userTaxService.getBusinessId());
            appUserTaxService.setEntityName(entity.getName());
            appUserTaxService.setServiceId(userTaxService.getServiceId());
            appUserTaxService.setServiceName(service.getDescription());
            String formattedDate = DateHandler.formatDateFull(userTaxService.getDateRequested());
            appUserTaxService.setDateRequested(formattedDate);
            appUserTaxService.setDateRequestedDateObject(userTaxService.getDateRequested());
            appUserTaxService.setNonCitPaymentPlan(userTaxService.getNonCitPaymentPlan());
            appUserTaxService.setStatus(userTaxService.getStatus());
            appUserTaxService.setCitFiledBeforeStatus(userTaxService.getCitFiledBeforeStatus());
            appUserTaxService.setCitVatFiledBeforeStatus(userTaxService.getCitVatFiledBeforeStatus());
            appUserTaxService.setVatFiledBeforeStatus(userTaxService.getVatFiledBeforeStatus());
        }
        
        return appUserTaxService;
    }
    
    private AppServiceCharge getAppServiceCharge(ServiceCharge serviceCharge) throws GeneralAppException {
        AppServiceCharge appServiceCharge = new AppServiceCharge();
        
        if (serviceCharge == null) {
            return appServiceCharge;
        } else {
            appServiceCharge.setId(serviceCharge.getId());
            appServiceCharge.setCitService(serviceCharge.getCitService());
            appServiceCharge.setVatService(serviceCharge.getVatService());
            appServiceCharge.setDeliveryService(serviceCharge.getDeliveryService());
            appServiceCharge.setSubTotal(serviceCharge.getSubTotal());
            appServiceCharge.setVat(serviceCharge.getVat());
            appServiceCharge.setTotal(serviceCharge.getTotal());
            appServiceCharge.setDiscountCode(serviceCharge.getDiscountCode());
            appServiceCharge.setDiscountDate(DateHandler.formatDateFullWithTime(serviceCharge.getDiscountDate()));
            appServiceCharge.setDiscountAmount(serviceCharge.getDiscountAmount());
            if (appServiceCharge.getDiscountAmount() != null && appServiceCharge.getDiscountAmount() > 0.0) {
                Double preDiscountTotal = appServiceCharge.getTotal() + appServiceCharge.getDiscountAmount();
                appServiceCharge.setPreDiscountTotal(preDiscountTotal);
            }
        }

        return appServiceCharge;
    }
    
    private List<AppUserTaxService> getAppUserTaxServices(List<UserTaxService> userTaxServices) throws GeneralAppException {
        List<AppUserTaxService> appUserTaxServices = new ArrayList<AppUserTaxService>();
        for (UserTaxService userTaxService : userTaxServices) {                      
            AppUserTaxService appUserTaxService = getAppUserTaxService(userTaxService);
            appUserTaxServices.add(appUserTaxService);
        }
        
        return appUserTaxServices;
    }
    
    private AppServiceRequestSummary getAppServiceRequestSummary(IncorporationRequest incorporationRequest) throws GeneralAppException {
        AppServiceRequestSummary appServiceRequestSummary = new AppServiceRequestSummary();
        if (incorporationRequest != null) {
            Service service = serviceManager.getService(incorporationRequest.getServiceId()); 
            if (service != null) {
                appServiceRequestSummary.setId(incorporationRequest.getId());
                appServiceRequestSummary.setUsername(incorporationRequest.getUsername());
                appServiceRequestSummary.setClientName(incorporationRequest.getProposedCompanyNameOne());
                appServiceRequestSummary.setRequestedServiceId(incorporationRequest.getServiceId());
                appServiceRequestSummary.setRequestedServiceName(service.getDescription());
                String formattedDate = DateHandler.formatDateFull(incorporationRequest.getRequestDate());
                appServiceRequestSummary.setDateRequested(formattedDate);
                appServiceRequestSummary.setDateRequestedDateObject(incorporationRequest.getRequestDate());
                appServiceRequestSummary.setRequestStatus(incorporationRequest.getStatus());
            }
        }
        
        return appServiceRequestSummary;
    }
    
    private AppServiceRequestSummary getAppServiceRequestSummary(UserTaxService userTaxService) throws GeneralAppException {
        AppServiceRequestSummary appServiceRequestSummary = new AppServiceRequestSummary();        
        if (userTaxService != null) {
            Service service = serviceManager.getService(userTaxService.getServiceId()); 
            LegalEntity entity = legalEntityManager.getLegalEntity(userTaxService.getBusinessId());
            if (service != null && entity != null) {                
            appServiceRequestSummary.setId(userTaxService.getId());
            appServiceRequestSummary.setUsername(userTaxService.getUsername());
            appServiceRequestSummary.setClientName(entity.getName());
            appServiceRequestSummary.setRequestedServiceId(userTaxService.getServiceId());
            appServiceRequestSummary.setRequestedServiceName(service.getDescription());
            String formattedDate = DateHandler.formatDateFull(userTaxService.getDateRequested());
            appServiceRequestSummary.setDateRequested(formattedDate);
            appServiceRequestSummary.setDateRequestedDateObject(userTaxService.getDateRequested());
            appServiceRequestSummary.setRequestStatus(userTaxService.getStatus());      
            appServiceRequestSummary.setAssignedTo(userTaxService.getAssignedTo());
            appServiceRequestSummary.setDocumentUrls(userTaxService.getDocumentUrls());
            }
        }

        return appServiceRequestSummary;
    }

    
    private ServiceRequestSummaryPayload getAppServiceRequestSummaryIncorporation(List<IncorporationRequest> incorporationRequests, ServiceRequestSummaryPayload payload) throws GeneralAppException {
        List<AppServiceRequestSummary> appServiceRequestSummaries = new ArrayList<AppServiceRequestSummary>();
        for (IncorporationRequest incorporationRequest : incorporationRequests) {
            AppServiceRequestSummary appServiceRequestSummary = new AppServiceRequestSummary();            
            
            appServiceRequestSummary = getAppServiceRequestSummary(incorporationRequest);
            appServiceRequestSummaries.add(appServiceRequestSummary);
                            
        }
        
        payload.addAppServiceRequestSummaries(appServiceRequestSummaries);
        
        return payload;
    }
    
    private ServiceRequestSummaryPayload getAppServiceRequestSummaryTax(List<UserTaxService> userTaxServices, ServiceRequestSummaryPayload payload) throws GeneralAppException {
        List<AppServiceRequestSummary> appServiceRequestSummaries = new ArrayList<AppServiceRequestSummary>();
        for (UserTaxService userTaxService : userTaxServices) {
            AppServiceRequestSummary appServiceRequestSummary = new AppServiceRequestSummary();
            
            
            appServiceRequestSummary = getAppServiceRequestSummary(userTaxService);
            
            appServiceRequestSummaries.add(appServiceRequestSummary);
                         
        }
        
        payload.addAppServiceRequestSummaries(appServiceRequestSummaries);               
        
        return payload;
    }

    @Override
    public ServiceRequestSummaryPayload getServiceRequestSummaries(String pageNumber, String pageSize, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(pageNumber, pageSize, rawToken);
        
        Claims claims = verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        User user = userManager.get(username);
        if (user == null) {
            exceptionThrowerManager.throwUserDoesNotExistException(TAX_SERVICE_RESOURCE);
        }
        
        ServiceRequestSummaryPayload payload = new ServiceRequestSummaryPayload();
            List<UserTaxService> userTaxServicesTemp = userTaxServiceDataManager.getAllTaxServicesExcludingCompletedStatus();
            List<IncorporationRequest> incorporationRequestsTemp = legalEntityManager.getAllIncorporationRequestsExcludingCompletedStatus();
            
            List<UserTaxService> userTaxServices = new ArrayList<UserTaxService>();
            List<IncorporationRequest> incorporationRequests = new ArrayList<IncorporationRequest>();
            if (user.getRole().equals(RoleType.ADMIN.getDescription())) {
                for (UserTaxService userTaxService : userTaxServicesTemp) {
                    if (userTaxService.getAssignedTo() != null && userTaxService.getAssignedTo().equals(user.getUsername())) {
                        userTaxServices.add(userTaxService);
                    }
                }                                
            } else {
                userTaxServices.addAll(userTaxServicesTemp);
                incorporationRequests.addAll(incorporationRequestsTemp);
            }
            
            payload = getAppServiceRequestSummaryIncorporation(incorporationRequests, payload);
            payload = getAppServiceRequestSummaryTax(userTaxServices, payload);
            
            int totalSize = userTaxServices.size() + incorporationRequests.size();
            
            List<AppServiceRequestSummary> appServiceRequestSummaries = new ArrayList<AppServiceRequestSummary>();
            appServiceRequestSummaries = payload.acquireAllAppServiceRequestSummaries();
            
            Collections.sort(appServiceRequestSummaries, new Comparator<AppServiceRequestSummary>() {
            public int compare(AppServiceRequestSummary o1, AppServiceRequestSummary o2) {
                if (o1.getDateRequestedDateObject() == null || o2.getDateRequestedDateObject() == null) {
                    return 0;
                }
                return o1.getDateRequestedDateObject().compareTo(o2.getDateRequestedDateObject());
            }
            });
            
            payload.setAppServiceRequestSummaries(appServiceRequestSummaries);
            
            payload.setPageNumber(Integer.parseInt(pageNumber));
            payload.setPageSize(Integer.parseInt(pageSize));
            payload.setTotalCount(totalSize);
            
        
            
            //payload.getAppServiceRequestSummaries().sort(Comparator.comparing(o -> o.getDateRequestedDateObject()));
            

            return payload;
       // }
        
        //return payload;
    }
    
    @Override
    public ServiceRequestSummaryPayload getServiceRequestSummariesForUser(String pageNumber, String pageSize, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(pageNumber, pageSize, rawToken);
        
        Claims claims = verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        User user = userManager.get(username);
        if (user == null) {
            exceptionThrowerManager.throwUserDoesNotExistException(TAX_SERVICE_RESOURCE);
        }
        
        ServiceRequestSummaryPayload payload = new ServiceRequestSummaryPayload();
        List<UserTaxService> userTaxServices = userTaxServiceDataManager.getUserTaxServicesByUsername(username);
        payload = getAppServiceRequestSummaryTax(userTaxServices, payload);

        int totalSize = userTaxServices.size();

        List<AppServiceRequestSummary> appServiceRequestSummaries = payload.acquireAllAppServiceRequestSummaries();

        Collections.sort(appServiceRequestSummaries, new Comparator<AppServiceRequestSummary>() {
            public int compare(AppServiceRequestSummary o1, AppServiceRequestSummary o2) {
                if (o1.getDateRequestedDateObject() == null || o2.getDateRequestedDateObject() == null) {
                    return 0;
                }
                return o1.getDateRequestedDateObject().compareTo(o2.getDateRequestedDateObject());
            }
        });

        payload.setAppServiceRequestSummaries(appServiceRequestSummaries);
        payload.setPageNumber(Integer.parseInt(pageNumber));
        payload.setPageSize(Integer.parseInt(pageSize));
        payload.setTotalCount(totalSize);

            return payload;
    }
    
    private ServiceCharge computeChargesForEntity(String userTaxServiceId, String entityId, String serviceId, String revenueRangeId, String citFiledBeforeStatus, 
            String citVatFiledBeforeStatus, String tccExpiryYear, String lastVatFiledDate) throws GeneralAppException {
        LegalEntity entity = legalEntityManager.getLegalEntity(entityId);
        
        Integer citServiceCharge = 0;
        Integer vatServiceCharge = 0;
        Integer deliveryCharge = 0;
        Integer subTotal = 0;
        Double vat = 0.0;
        Double total = 0.0;
        
        int flatRateCitCharge = 0;
        int flatRateVatCharge = 0;
        int flatRatePitCharge = 0;
        PaymentPlan citFlatRatePaymentPlan = new PaymentPlan();
        PaymentPlan vatFlatRatePaymentPlan = new PaymentPlan();
        PaymentPlan pitFlatRatePaymentPlan = new PaymentPlan();
        
        if (serviceId.equals(ServiceType.COMPANY_INCOME_TAX.getDescription())) {
            citFlatRatePaymentPlan = paymentPlanManager.getFlatRatePaymentPlan(serviceId);
            flatRateCitCharge = citFlatRatePaymentPlan.getAmount();
            vatFlatRatePaymentPlan = paymentPlanManager.getFlatRatePaymentPlan(ServiceType.VALUED_ADDED_TAX.getDescription());
            flatRateVatCharge = vatFlatRatePaymentPlan.getAmount();
            citServiceCharge = getCitServiceCharge(entity, citFiledBeforeStatus, flatRateCitCharge, tccExpiryYear);
        } else if (serviceId.equals(ServiceType.PERSONAL_INCOME_TAX.getDescription())) {
            pitFlatRatePaymentPlan = paymentPlanManager.getFlatRatePaymentPlan(serviceId);
            flatRatePitCharge = pitFlatRatePaymentPlan.getAmount();
            vatFlatRatePaymentPlan = paymentPlanManager.getFlatRatePaymentPlan(ServiceType.VALUED_ADDED_TAX.getDescription());
            flatRateVatCharge = vatFlatRatePaymentPlan.getAmount();
            citServiceCharge = getCitServiceCharge(entity, citFiledBeforeStatus, flatRatePitCharge, tccExpiryYear);
        }
        
        
        vatServiceCharge = getVatServiceCharge(entity, citVatFiledBeforeStatus, flatRateVatCharge, lastVatFiledDate, revenueRangeId);
        deliveryCharge = 1500;
        subTotal = citServiceCharge + vatServiceCharge;
        vat = subTotal * 0.05;
        total = subTotal + deliveryCharge + vat;
        
        ServiceCharge serviceCharge = new ServiceCharge();
        serviceCharge.setId(userTaxServiceId);
        serviceCharge.setCitService(citServiceCharge);
        serviceCharge.setVatService(vatServiceCharge);
        serviceCharge.setDeliveryService(deliveryCharge);
        serviceCharge.setSubTotal(subTotal);
        serviceCharge.setVat(vat);
        serviceCharge.setTotal(total);
        
        serviceCharge = serviceChargeManager.createServiceCharge(serviceCharge);
        
        return serviceCharge;
    }
    
    private ServiceCharge computeStandAloneVatChargesForEntity(String userTaxServiceId, String entityId, String serviceId, String revenueRangeId,  
            String vatFiledBeforeStatus, String lastVatFiledDate, String paymentPlanId) throws GeneralAppException {
        LegalEntity entity = legalEntityManager.getLegalEntity(entityId);
                
        Integer vatServiceCharge = 0;
        Integer deliveryCharge = 0;
        Integer subTotal = 0;
        Double vat = 0.0;
        Double total = 0.0;
        
        int flatRateVatCharge = 0;                
        PaymentPlan vatFlatRatePaymentPlan = new PaymentPlan();        
               
        vatFlatRatePaymentPlan = paymentPlanManager.getFlatRatePaymentPlan(serviceId);
        flatRateVatCharge = vatFlatRatePaymentPlan.getAmount();
                        
        if (revenueRangeId == null || revenueRangeId.isEmpty()) {
            revenueRangeId = entity.getRevenueRangeId();
            System.out.println("Revenue Range is NULL using entity's revenue range");
        }
        
        if (paymentPlanId == null || paymentPlanId.isEmpty()) {
            vatServiceCharge = getVatServiceCharge(entity, vatFiledBeforeStatus, flatRateVatCharge, lastVatFiledDate, revenueRangeId);
        } else {
            List<AppPaymentPlan> vatPaymentPlans = paymentPlanManager.getPaymentPlans(serviceId);
            for (AppPaymentPlan appPaymentPlan : vatPaymentPlans) {
                if (paymentPlanId.equals(appPaymentPlan.getId())) {
                    vatServiceCharge = appPaymentPlan.getAmount();
                }
            }
        }
        
        deliveryCharge = 1500;
        subTotal = vatServiceCharge;
        vat = subTotal * 0.05;
        total = subTotal + deliveryCharge + vat;
        
        ServiceCharge serviceCharge = new ServiceCharge();
        serviceCharge.setId(userTaxServiceId);
        serviceCharge.setVatService(vatServiceCharge);
        serviceCharge.setDeliveryService(deliveryCharge);
        serviceCharge.setSubTotal(subTotal);
        serviceCharge.setVat(vat);
        serviceCharge.setTotal(total);
        
        serviceCharge = serviceChargeManager.createServiceCharge(serviceCharge);
        
        return serviceCharge;
    }
    
    private Integer getCitServiceCharge(LegalEntity entity, String citFiledBeforeStatus, 
            Integer flatRateCitCharge, String tccExpiryYear) {
        
        Integer citServiceCharge = 0;
        if (citFiledBeforeStatus.equals(TaxStatusType.NEVER.getDescription())) {
            Date incorporationDate = entity.getIncorporationDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(incorporationDate);
            Integer commencementYear = calendar.get(Calendar.YEAR);
                        
            Integer currentYear = DateHandler.getCurrentYear();
            
            Integer yearsToCharge = currentYear - commencementYear;
            if (yearsToCharge < 1) {
                yearsToCharge = 1;
            }
            citServiceCharge = flatRateCitCharge * yearsToCharge;
            
        } else if (citFiledBeforeStatus.equals(TaxStatusType.OTHER.getDescription())) {            
            Integer lastCitFiledYear = Integer.parseInt(tccExpiryYear);
            
            Integer currentYear = DateHandler.getCurrentYear();
            
            Integer yearsToCharge = currentYear - lastCitFiledYear;
            if (yearsToCharge < 1) {
                yearsToCharge = 1;
            }
            citServiceCharge = flatRateCitCharge * yearsToCharge;
        } else if (citFiledBeforeStatus.equals(TaxStatusType.TAX_MASTER.getDescription())) {
            Integer lastCitFiledYear = Integer.parseInt(tccExpiryYear);
            
            Integer currentYear = DateHandler.getCurrentYear();
            
            Integer yearsToCharge = currentYear - lastCitFiledYear;
            if (yearsToCharge < 1) {
                yearsToCharge = 1;
            }
            citServiceCharge = flatRateCitCharge * yearsToCharge;
        } else {            
            Integer currentYear = DateHandler.getCurrentYear();
            
            Integer yearsToCharge = currentYear - (currentYear - 1);
            if (yearsToCharge < 1) {
                yearsToCharge = 1;
            }
            citServiceCharge = flatRateCitCharge * yearsToCharge;
        }
        
        return citServiceCharge;
    }
    
    private Integer getVatServiceCharge(LegalEntity entity, String vatFiledBeforeStatus, 
            Integer flatRateVatCharge, String lastVatFiledDate, String revenueRangeId) {
        
        System.out.println("VAAAAAAAAAAAAAAAAT " + vatFiledBeforeStatus + " " + flatRateVatCharge + " " + lastVatFiledDate);
        Integer vatServiceCharge = 0;
        if (vatFiledBeforeStatus.equals(TaxStatusType.NEVER.getDescription())) {
            Date commencementDate = entity.getCommencementDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(commencementDate);
            Integer commencementYear = calendar.get(Calendar.YEAR);
                        
            Integer currentYear = DateHandler.getCurrentYear();
            
            Integer yearsToCharge = currentYear - commencementYear + 1; //plus 1 because commencementYear is inclusive
            
            if (revenueRangeId.equals(REVENUE_RANGE_1)) {
                yearsToCharge = 1;
            } else if (revenueRangeId.equals(REVENUE_RANGE_2)) {
                if (yearsToCharge >= 2) {
                    yearsToCharge = 2;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_3)) {
                if (yearsToCharge >= 3) {
                    yearsToCharge = 3;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_4)) {
                if (yearsToCharge >= 3) {
                    yearsToCharge = 3;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_5)) {
                if (yearsToCharge >= 4) {
                    yearsToCharge = 4;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_6)) {
                yearsToCharge = currentYear - commencementYear + 1;
                if (yearsToCharge < 1) {
                    yearsToCharge = 1;
                }
            }
            System.out.println("VAAAAAAAAAAAAAAAAT 1 years to charge " + vatServiceCharge);
            vatServiceCharge = flatRateVatCharge * yearsToCharge;
            
        } else if (vatFiledBeforeStatus.equals(TaxStatusType.OTHER.getDescription())) {            
            Date lastDateVatFiled = DateHandler.getDateDayMonthYear(lastVatFiledDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(lastDateVatFiled);
            
            Integer lastVatFiledYear = calendar.get(Calendar.YEAR);   
            Integer currentYear = DateHandler.getCurrentYear();
            
            Integer yearsToCharge = currentYear - lastVatFiledYear + 1; //plus 1 because lastVatFiledYear is inclusive
                        
            if (revenueRangeId.equals(REVENUE_RANGE_1)) {
                yearsToCharge = 1;
            } else if (revenueRangeId.equals(REVENUE_RANGE_2)) {
                if (yearsToCharge >= 2) {
                    yearsToCharge = 2;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_3)) {
                if (yearsToCharge >= 3) {
                    yearsToCharge = 3;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_4)) {
                if (yearsToCharge >= 3) {
                    yearsToCharge = 3;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_5)) {
                if (yearsToCharge >= 4) {
                    yearsToCharge = 4;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_6)) {
                yearsToCharge = currentYear - lastVatFiledYear + 1;
                if (yearsToCharge < 1) {
                    yearsToCharge = 1;
                }
            }
            System.out.println("VAAAAAAAAAAAAAAAAT 2 years to charge " + vatServiceCharge);
            vatServiceCharge = flatRateVatCharge * yearsToCharge;
        } else if (vatFiledBeforeStatus.equals(TaxStatusType.TAX_MASTER.getDescription())) {
            Date lastDateVatFiled = DateHandler.getDateDayMonthYear(lastVatFiledDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(lastDateVatFiled);
            
            Integer lastVatFiledYear = calendar.get(Calendar.YEAR);   
            Integer currentYear = DateHandler.getCurrentYear();
            
            Integer yearsToCharge = currentYear - lastVatFiledYear + 1; //plus 1 because lastVatFiledYear is inclusive
                        
            if (revenueRangeId.equals(REVENUE_RANGE_1)) {
                yearsToCharge = 1;
            } else if (revenueRangeId.equals(REVENUE_RANGE_2)) {
                if (yearsToCharge >= 2) {
                    yearsToCharge = 2;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_3)) {
                if (yearsToCharge >= 3) {
                    yearsToCharge = 3;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_4)) {
                if (yearsToCharge >= 3) {
                    yearsToCharge = 3;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_5)) {
                if (yearsToCharge >= 4) {
                    yearsToCharge = 4;
                }
            } else if (revenueRangeId.equals(REVENUE_RANGE_6)) {
                yearsToCharge = currentYear - lastVatFiledYear + 1;
                if (yearsToCharge < 1) {
                    yearsToCharge = 1;
                }
            }
            System.out.println("VAAAAAAAAAAAAAAAAT 3 years to charge " + vatServiceCharge);
            vatServiceCharge = flatRateVatCharge * yearsToCharge;
        } else {                        
            vatServiceCharge = 0;
            System.out.println("VAAAAAAAAAAAAAAAAT 4 years to charge " + vatServiceCharge);
        }
        
        System.out.println("VAAAAAAAAAAAAAAAAT cit vatcervie charge " + vatServiceCharge);
        
        return vatServiceCharge;
    }

    private ServiceCharge computeChargesForPaye(String userTaxServiceId, String entityId, String serviceId, String paymentPlanId) throws GeneralAppException {
        ServiceCharge serviceCharge = new ServiceCharge();
        List<AppPaymentPlan> payePaymentPlans = paymentPlanManager.getPaymentPlans(serviceId);
        for (AppPaymentPlan appPaymentPlan : payePaymentPlans) {
            if(appPaymentPlan.getId().equals(paymentPlanId)) {
                serviceCharge.setCitService(appPaymentPlan.getAmount());
                serviceCharge.setDeliveryService(DELIVERY_CHARGE);
                double vatCharge = serviceCharge.getCitService() * 0.05;
                serviceCharge.setVat(vatCharge);
                serviceCharge.setSubTotal(serviceCharge.getCitService());
                serviceCharge.setTotal(serviceCharge.getCitService() + serviceCharge.getVat() + serviceCharge.getDeliveryService());
                serviceCharge.setId(userTaxServiceId);
            }
        }
        
        return serviceCharge;
    }
    
    @Override
    public AppServiceRequestSummary getServiceRequestDetails(String id, String rawToken) throws GeneralAppException {
        UserTaxService userTaxService = new UserTaxService();
        IncorporationRequest incorporationRequest = new IncorporationRequest();
        AppServiceRequestSummary appServiceRequestSummary = new AppServiceRequestSummary();
        userTaxService = userTaxServiceDataManager.get(id);
        if (userTaxService != null) {
            appServiceRequestSummary = getAppServiceRequestSummary(userTaxService);
            LegalEntity entity = legalEntityManager.getLegalEntity(userTaxService.getBusinessId());
            AppLegalEntity appLegalEntity = legalEntityManager.getAppLegalEntity(entity);
            ServiceCharge serviceCharge = serviceChargeManager.getServiceCharge(userTaxService.getId());
            appServiceRequestSummary.setEntity(appLegalEntity);
            appServiceRequestSummary.setServiceCharge(serviceCharge);
        } else {
            incorporationRequest = legalEntityManager.getIncorporationRequest(id);
            appServiceRequestSummary = getAppServiceRequestSummary(incorporationRequest);
            AppIncorporationRequest appIncorporationRequest = legalEntityManager.getAppIncorporationRequest(incorporationRequest);
            appServiceRequestSummary.setIncorporationRequest(appIncorporationRequest);
        }
        
        return appServiceRequestSummary;
    }

    @Override
    public UserTaxService getUserTaxService(String id) {
        return userTaxServiceDataManager.get(id);
    }
    
    @Override
    public ServiceRequestSummaryPayload searchUserTaxServices(String searchTerm, String serviceId, String status, String pageNumber, String pageSize, String rawToken) throws GeneralAppException {
        List<UserTaxService> allSearchResults = new ArrayList<UserTaxService>();
        
        String requestorUsername = verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyJwt(rawToken).getSubject();
        String requestorRole = "";
        if (requestorUsername != null) {
            User user = userManager.get(requestorUsername);
            requestorRole = user.getRole();
        }
        
        List<String> usernames = new ArrayList<String>();
        
        if (searchTerm != null && !searchTerm.isEmpty()) {
            List<String> userBasedSearchUsernames = getUsernamesThatMatchSearchTerm(searchTerm);
            List<String> legalEntityBasedSearchUsernames = getUsernamesForEntityNamesThatMatchSearchTerm(searchTerm);
            
            usernames.addAll(userBasedSearchUsernames);
            usernames.addAll(legalEntityBasedSearchUsernames);            
        }
        
        if (!usernames.isEmpty()) {
            for (String username : usernames) {
                List<UserTaxService> userTaxServices = userTaxServiceDataManager.searchUserTaxServices(username, serviceId, status);

                allSearchResults.addAll(userTaxServices);
            }
        } else {
            allSearchResults = userTaxServiceDataManager.searchUserTaxServices(null, serviceId, status);
        }
                
        if (requestorRole.equals(RoleType.USER.getDescription())) {
            List<UserTaxService> resultsForUserRoleRequestor = new ArrayList<UserTaxService>();
            for (UserTaxService userTaxService : allSearchResults) {
                if (userTaxService.getUsername().equals(requestorUsername)) {
                    resultsForUserRoleRequestor.add(userTaxService);
                }
            }
            allSearchResults.clear();
            allSearchResults.addAll(resultsForUserRoleRequestor);
        } else if (requestorRole.equals(RoleType.ADMIN.getDescription())) {
            List<UserTaxService> resultsForUserRoleRequestor = new ArrayList<UserTaxService>();
            for (UserTaxService userTaxService : allSearchResults) {
                if (userTaxService.getAssignedTo() != null && userTaxService.getAssignedTo().equals(requestorUsername)) {
                    resultsForUserRoleRequestor.add(userTaxService);
                }
            }
            allSearchResults.clear();
            allSearchResults.addAll(resultsForUserRoleRequestor);
        } 
        
        ServiceRequestSummaryPayload payload = new ServiceRequestSummaryPayload();
        payload = getAppServiceRequestSummaryTax(allSearchResults, payload);
        List<AppServiceRequestSummary> appServiceRequestSummaries = new ArrayList<AppServiceRequestSummary>();
        appServiceRequestSummaries = payload.acquireAllAppServiceRequestSummaries();        
        
        Collections.sort(appServiceRequestSummaries, new Comparator<AppServiceRequestSummary>() {
            public int compare(AppServiceRequestSummary o1, AppServiceRequestSummary o2) {
                if (o1.getDateRequestedDateObject() == null || o2.getDateRequestedDateObject() == null) {
                    return 0;
                }
                return o1.getDateRequestedDateObject().compareTo(o2.getDateRequestedDateObject());
            }
        });
        
        int totalSize = appServiceRequestSummaries.size();
        payload.setAppServiceRequestSummaries(appServiceRequestSummaries);

        payload.setPageNumber(Integer.parseInt(pageNumber));
        payload.setPageSize(Integer.parseInt(pageSize));
        payload.setTotalCount(totalSize);
        
        return payload;
    } 
    
    
    private List<String> getUsernamesThatMatchSearchTerm(String searchTerm) throws GeneralAppException {
        List<User> users = userManager.getByUsernameFirstNameLastNamePhoneNumber(searchTerm);
        List<String> usernames = new ArrayList<String>();
        
        for (User user : users) {
            usernames.add(user.getUsername());
            System.out.println("USER: " + user.getUsername());
        }
        
        
        return usernames;
    }
    
    private List<String> getUsernamesForEntityNamesThatMatchSearchTerm(String searchTerm) throws GeneralAppException {
        List<LegalEntity> legalEntities = legalEntityManager.getByEntityName(searchTerm);
        List<String> usernames = new ArrayList<String>();
        
        for (LegalEntity legalEntity : legalEntities) {
            List<UserLegalEntity> userLegalEntities = userLegalEntityManager.getLegalEntitiesByEntity(legalEntity.getId());
            if (!userLegalEntities.isEmpty()) {
                usernames.add(userLegalEntities.get(0).getUsername());
            }            
        }
      
        return usernames;
    }

    @Override
    public UserTaxService update(UserTaxService userTaxService) throws GeneralAppException {
        return userTaxServiceDataManager.update(userTaxService);
    }
    
    @Override
    public AppBoolean saveDocumentUrl(String id, String url, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(SAVE_DOCUMENT_URL_RESOURCE).verifyParams(id, url, rawToken);
        
        AppBoolean updated = new AppBoolean();
        
        UserTaxService userTaxService = userTaxServiceDataManager.get(id);
        if (userTaxService == null) {
            exceptionThrowerManager.throwUserTaxServiceNotFoundException(SAVE_DOCUMENT_URL_RESOURCE);
        }
        
        String exisitingDocumentUrls = userTaxService.getDocumentUrls();
        if (exisitingDocumentUrls == null || exisitingDocumentUrls.isEmpty()) {
            exisitingDocumentUrls = url;
        } else {
            exisitingDocumentUrls = exisitingDocumentUrls.concat("," + url);
        }
        
        userTaxService.setDocumentUrls(exisitingDocumentUrls);
        userTaxServiceDataManager.update(userTaxService);
        
        updated.setStatus(Boolean.TRUE);
        
        return updated;
    }

    @Override
    public AppUserTaxService applyDiscount(String id, String discountCode, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(TAX_SERVICE_DISCOUNT_RESOURCE).verifyParams(id, discountCode, rawToken);  
        
        UserTaxService userTaxService = userTaxServiceDataManager.get(id);
        DiscountCoupon discountCoupon = discountCouponDataManager.get(discountCode);
        
        if (discountCoupon == null) {
            exceptionThrowerManager.throwDiscountCouponDoesNotExistException(TAX_SERVICE_DISCOUNT_RESOURCE);
        }
        
        Double discountPercentage = Double.parseDouble(discountCoupon.getPercentage());
     
        ServiceCharge serviceCharge = serviceChargeManager.getServiceCharge(userTaxService.getId());
        
        if (serviceCharge.getDiscountCode() == null || serviceCharge.getDiscountCode().equals("")) {
            Double totalAmount = serviceCharge.getTotal();
            Double discountAmount = totalAmount * (discountPercentage / 100);
            totalAmount = totalAmount - discountAmount;
            serviceCharge.setTotal(round(totalAmount, 0));
            serviceCharge.setDiscountCode(discountCode);
            serviceCharge.setDiscountDate(new Date());
            serviceCharge.setDiscountAmount(round(discountAmount, 0));            
            serviceChargeManager.update(serviceCharge);
        }
                
        AppServiceCharge appServiceCharge = getAppServiceCharge(serviceCharge);
        
        AppUserTaxService appUserTaxService = getAppUserTaxService(userTaxService);
        appUserTaxService.setServiceCharge(appServiceCharge);
        
        return appUserTaxService;
    }
    
    private static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
}
