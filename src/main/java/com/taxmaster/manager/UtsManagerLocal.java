/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.UserTaxService;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppServiceRequestSummary;
import com.taxmaster.pojo.AppUserTaxService;
import com.taxmaster.pojo.ServiceRequestSummaryPayload;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author buls
 */
@Local
public interface UtsManagerLocal {
    
    void nothing() throws GeneralAppException;
    
    AppUserTaxService createUserTaxService(String entityId, String serviceId, 
            String paymentPlanId, String revenueRangeId, String year, String citFiledBeforeStatus, 
            String citVatFiledBeforeStatus, String vatFiledBeforeStatus, String tccExpiryYear, String lastVatFiledDate, String rawToken) throws GeneralAppException;

    AppBoolean updateStatus(String id, String status, String rawToken) throws GeneralAppException;
    
    UserTaxService update(UserTaxService userTaxService) throws GeneralAppException;

    void delete(UserTaxService userTaxService);
    
    List<UserTaxService> getUserTaxServicesByUsername(String username);           
    
    UserTaxService getUserTaxService(String id);           
    
    ServiceRequestSummaryPayload getServiceRequestSummaries(String pageNumber, String pageSize, String rawToken) throws GeneralAppException;

    ServiceRequestSummaryPayload getServiceRequestSummariesForUser(String pageNumber, String pageSize, String rawToken) throws GeneralAppException;
    
    AppServiceRequestSummary getServiceRequestDetails(String id, String rawToken) throws GeneralAppException;
    
    ServiceRequestSummaryPayload searchUserTaxServices(String searchTerm, String serviceId, String status, String pageNumber, String pageSize, String rawToken) throws GeneralAppException; 
    
    AppUserTaxService getAppUserTaxService(UserTaxService userTaxService) throws GeneralAppException;      
        
    AppBoolean saveDocumentUrl(String id, String url, String rawToken) throws GeneralAppException;
    
    AppUserTaxService applyDiscount(String id, String discountCode, String rawToken) throws GeneralAppException;

}
