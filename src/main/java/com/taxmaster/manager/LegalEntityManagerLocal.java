/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.IncorporationRequest;
import com.taxmaster.model.LegalEntity;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppLegalEntity;
import com.taxmaster.pojo.AppIncorporationRequest;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface LegalEntityManagerLocal {

    AppLegalEntity create(String rawToken, String name, String address, String typeId, String industryId, String incorporationDate,
            String tin, String commencementDate, String revenueRangeId, String stateId, String addressLine1, String addressLine2,
            String city) throws EJBTransactionRolledbackException, GeneralAppException;
    
    LegalEntity getLegalEntity(String businessId);
    
    AppLegalEntity getAppLegalEntity(LegalEntity legalEntity) throws GeneralAppException;
    
    AppIncorporationRequest getAppIncorporationRequest(IncorporationRequest incorporationRequest);
    
    AppIncorporationRequest createIncorporationRequest(String rawToken, String proposedCompanyNameOne, String proposedCompanyNameTwo, String proposedCompanyNameThree,
            String proposedCompanyNameFour, String companyAddress, String companyIndustry) throws EJBTransactionRolledbackException, GeneralAppException;
    
    AppBoolean hasLegalEntity(String rawToken) throws GeneralAppException;
    
    AppBoolean hasIncorporationRequest(String rawToken) throws GeneralAppException;        
    
    List<IncorporationRequest> getAllIncorporationRequestsExcludingCompletedStatus();
    
    List<IncorporationRequest> getIncorporationRequestsByUsername(String username);
    
    IncorporationRequest getIncorporationRequest(String id);
    
    AppBoolean updadeIncorporationRequestStatus(String id, String status, String rawToken);

    List<LegalEntity> getByEntityName(String searchTerm);

}
