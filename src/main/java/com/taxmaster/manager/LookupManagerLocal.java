/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.LegalEntityType;
import com.taxmaster.model.CompanyIncomeTaxFilingPeriod;
import com.taxmaster.model.ContactFormSubject;
import com.taxmaster.model.Industry;
import com.taxmaster.model.RevenueRange;
import com.taxmaster.model.Service;
import com.taxmaster.model.State;
import com.taxmaster.pojo.AppServiceStatusType;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.util.ServiceStatusType;
import java.util.List;
import javax.ejb.Local;

@Local
public interface LookupManagerLocal {

    List<Industry> getIndustries();
    
    List<AppUser> getUsersByRole(String role);
    
    Industry getIndustry(String id);
    
    List<LegalEntityType> getLegalEntityTypes();
    
    LegalEntityType getLegalEntityType(String id);
    
    List<RevenueRange> getRevenueRanges();
    
    RevenueRange getRevenueRange(String id);
    
    List<CompanyIncomeTaxFilingPeriod> getCompanyIncomeTaxFilingPeriod();
    
    List<State> getAllStates();
    
    State getState(String id);
    
    List<ContactFormSubject> getAllSubject();

    List<Service> getServices();

    List<AppServiceStatusType> getServiceStatuses();

}
