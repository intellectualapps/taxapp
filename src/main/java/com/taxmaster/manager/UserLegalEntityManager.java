/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.UserLegalEntityDataManagerLocal;
import com.taxmaster.model.IncorporationRequest;
import com.taxmaster.model.UserLegalEntity;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class UserLegalEntityManager implements UserLegalEntityManagerLocal {
    
    @EJB
    private UserLegalEntityDataManagerLocal userLegalEntityDataManager; 

    @Override
    public UserLegalEntity create(UserLegalEntity userLegalEntity) throws EJBTransactionRolledbackException {
        return userLegalEntityDataManager.create(userLegalEntity);
    }

    @Override
    public List<UserLegalEntity> getLegalEntitiesByUsername(String username) throws EJBTransactionRolledbackException {
        return userLegalEntityDataManager.getUserLegalEntitiesByUsername(username);
    }

    @Override
    public List<IncorporationRequest> getIncorporationRequestByUsername(String username) throws EJBTransactionRolledbackException {
        return userLegalEntityDataManager.getUserIncorporationRequestsByUsername(username);
    }

    @Override
    public List<UserLegalEntity> getLegalEntitiesByEntity(String entityId) throws EJBTransactionRolledbackException {
        return userLegalEntityDataManager.getUserLegalEntitiesByEntity(entityId);
    }

}

