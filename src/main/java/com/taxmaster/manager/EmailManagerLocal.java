/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.Otp;
import com.taxmaster.model.User;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.pojo.AppUserTaxService;
import com.taxmaster.util.MessageType;

public interface EmailManagerLocal {
    void sendVerificationEmail(User user, Otp otp, MessageType messageType);
    void sendTaxCompleteEmail(User user, MessageType messageType);
    void sendPaymentConfirmation(User user, MessageType messageType);
    void sendTaskAssignmentConfirmation(AppUser user, AppUserTaxService appUserTaxService, MessageType messageType);
    void sendNewOtpEmail(User user, String newOtp, MessageType messageType);
    void contactUs(String name, String email, String subject, String message);
}
