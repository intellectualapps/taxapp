/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.UserTaxServiceCitPaymentPlan;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.Local;

@Local
public interface UserTaxServiceCitPaymentPlanManagerLocal {
    
    UserTaxServiceCitPaymentPlan create(UserTaxServiceCitPaymentPlan userTaxServiceCitPaymentPlan) throws GeneralAppException;  
    
    UserTaxServiceCitPaymentPlan get(String userTaxServiceId) throws GeneralAppException;  
    
}
