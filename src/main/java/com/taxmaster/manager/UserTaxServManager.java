/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.CitPaymentPlanDataManagerLocal;
import com.taxmaster.data.manager.PaymentPlanDataManagerLocal;
import com.taxmaster.data.manager.UserDataManagerLocal;
import com.taxmaster.data.manager.UserTaxServiceDataManagerLocal;
import com.taxmaster.model.LegalEntity;
import com.taxmaster.model.CitPaymentPlan;
import com.taxmaster.model.IncorporationRequest;
import com.taxmaster.model.PaymentPlan;
import com.taxmaster.model.Service;
import com.taxmaster.model.User;
import com.taxmaster.model.UserTaxService;
import com.taxmaster.model.UserTaxServiceCitPaymentPlan;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppCitPaymentPlan;
import com.taxmaster.pojo.AppPaymentPlan;
import com.taxmaster.pojo.AppService;
import com.taxmaster.pojo.AppServiceRequestSummary;
import com.taxmaster.pojo.AppUserTaxService;
import com.taxmaster.pojo.IncorporationPaymentPlanPayload;
import com.taxmaster.pojo.ServiceRequestSummaryPayload;
import com.taxmaster.util.CodeGenerator;
import com.taxmaster.util.DateHandler;
import com.taxmaster.util.MessageType;
import com.taxmaster.util.ServiceCategoryType;
import com.taxmaster.util.ServiceStatusType;
import com.taxmaster.util.ServiceType;
import com.taxmaster.util.Verifier;
import com.taxmaster.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless (name="UserTaxServManager")
public class UserTaxServManager implements UserTaxServManagerLocal {

    @EJB
    private UserTaxServiceDataManagerLocal userTaxServiceDataManager;    
    
    @EJB
    private Verifier verifier;    
    
    @EJB
    private CodeGenerator codeGenerator;    
    
    @EJB
    private UserDataManagerLocal userManager;
    
    @EJB
    private EmailManagerLocal emailManager;
    
    @EJB
    private UserTaxServiceCitPaymentPlanManagerLocal userTaxServiceCitPaymentPlanManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionThrowerManager;  
    
    @EJB
    private ServiceManagerLocal serviceManager;
    
    @EJB
    private LegalEntityManagerLocal legalEntityManager;
    
    private String TAX_SERVICE_RESOURCE = "/service/tax";

    @Override
    public AppUserTaxService createUserTaxService(String entityId, String serviceId, 
            String paymentPlanId, String revenueRangeId, String year, String rawToken) throws GeneralAppException {
        if (serviceId.equals(ServiceType.COMPANY_INCOME_TAX.getDescription())) {
            verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(entityId, serviceId, 
                    revenueRangeId, year, rawToken);            
        } else {
            verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(entityId, serviceId, 
                    paymentPlanId, rawToken);
        }
        
        Claims claims = verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        User user = userManager.get(username);
        if (user == null) {
            exceptionThrowerManager.throwUserDoesNotExistException(TAX_SERVICE_RESOURCE);
        }
        
        UserTaxService userTaxService = new UserTaxService();
        userTaxService.setId(codeGenerator.getToken());
        userTaxService.setBusinessId(entityId);
        userTaxService.setServiceId(serviceId);
        userTaxService.setUsername(username);
        userTaxService.setStatus(ServiceStatusType.PENDING.getDescription());
        userTaxService.setDateRequested(DateHandler.getNow());
        userTaxService.setFromDate(DateHandler.getNow());
        userTaxService.setToDate(DateHandler.getNow());
        
        userTaxService = userTaxServiceDataManager.create(userTaxService);
        
        if (serviceId.equals(ServiceType.COMPANY_INCOME_TAX.getDescription())) {
            UserTaxServiceCitPaymentPlan userTaxServiceCitPaymentPlan = new UserTaxServiceCitPaymentPlan();
            userTaxServiceCitPaymentPlan.setRevenueRange(revenueRangeId);
            userTaxServiceCitPaymentPlan.setYear(year);
            userTaxServiceCitPaymentPlan.setUserTaxServiceId(userTaxService.getId());
            
            userTaxServiceCitPaymentPlanManager.create(userTaxServiceCitPaymentPlan);
        } else {
            userTaxService.setNonCitPaymentPlan(paymentPlanId);
        }
        
        AppUserTaxService appUserTaxService = getAppUserTaxService(userTaxService);
        
        return appUserTaxService;
    }

    @Override
    public AppBoolean updateStatus(String id, String status, String rawToken) throws GeneralAppException {
        AppBoolean appBoolean = new AppBoolean();
        UserTaxService userTaxService = userTaxServiceDataManager.get(id);
        if (status.equals(ServiceStatusType.COMPLETED.getDescription())) {
            userTaxService.setStatus(ServiceStatusType.COMPLETED.getDescription());
        }
        User user = userManager.get(userTaxService.getUsername());
        emailManager.sendTaxCompleteEmail(user, MessageType.TAX_COMPLETE);
        
        appBoolean.setStatus(Boolean.TRUE);
        return appBoolean;
    }

    @Override
    public void delete(UserTaxService userTaxService) {
        userTaxServiceDataManager.delete(userTaxService);
    }

    @Override
    public List<UserTaxService> getUserTaxServicesByUsername(String username) {                
        return userTaxServiceDataManager.getUserTaxServicesByUsername(username);
    }
    
    @Override
    public UserTaxService getUserTaxServicesById(String id) {
       return userTaxServiceDataManager.get(id);
    }
    
    @Override
    public UserTaxService update(UserTaxService userTaxService) {
        return userTaxServiceDataManager.update(userTaxService);
    }    
    
    private AppUserTaxService getAppUserTaxService(UserTaxService userTaxService) throws GeneralAppException {
        AppUserTaxService appUserTaxService = new AppUserTaxService();
        
        if (userTaxService == null) {
            return appUserTaxService;
        }
        Service service = serviceManager.getService(userTaxService.getServiceId()); 
        LegalEntity entity = legalEntityManager.getLegalEntity(userTaxService.getBusinessId());
        if (service != null && entity != null) {
            appUserTaxService.setId(userTaxService.getId());
            appUserTaxService.setUsername(userTaxService.getUsername());
            appUserTaxService.setBusinessId(userTaxService.getBusinessId());
            appUserTaxService.setEntityName(entity.getName());
            appUserTaxService.setServiceId(userTaxService.getServiceId());
            appUserTaxService.setServiceName(service.getDescription());
            String formattedDate = DateHandler.formatDateFull(userTaxService.getDateRequested());
            appUserTaxService.setDateRequested(formattedDate);
            appUserTaxService.setDateRequestedDateObject(userTaxService.getDateRequested());
            appUserTaxService.setNonCitPaymentPlan(userTaxService.getNonCitPaymentPlan());
            appUserTaxService.setStatus(userTaxService.getStatus());
        }
        
        return appUserTaxService;
    }
    
    private List<AppUserTaxService> getAppUserTaxServices(List<UserTaxService> userTaxServices) throws GeneralAppException {
        List<AppUserTaxService> appUserTaxServices = new ArrayList<AppUserTaxService>();
        for (UserTaxService userTaxService : userTaxServices) {                      
            AppUserTaxService appUserTaxService = getAppUserTaxService(userTaxService);
            appUserTaxServices.add(appUserTaxService);
        }
        
        return appUserTaxServices;
    }
    
    private ServiceRequestSummaryPayload getAppServiceRequestSummaryIncorporation(List<IncorporationRequest> incorporationRequests, ServiceRequestSummaryPayload payload) throws GeneralAppException {
        List<AppServiceRequestSummary> appServiceRequestSummaries = new ArrayList<AppServiceRequestSummary>();
        for (IncorporationRequest incorporationRequest : incorporationRequests) {
            AppServiceRequestSummary appServiceRequestSummary = new AppServiceRequestSummary();
            Service service = serviceManager.getService(incorporationRequest.getServiceId()); 
            
            if (service != null) {
                appServiceRequestSummary.setId(incorporationRequest.getId());
                appServiceRequestSummary.setUsername(incorporationRequest.getUsername());                
                appServiceRequestSummary.setClientName(incorporationRequest.getProposedCompanyNameOne());
                appServiceRequestSummary.setRequestedServiceId(incorporationRequest.getServiceId());
                appServiceRequestSummary.setRequestedServiceName(service.getDescription());
                String formattedDate = DateHandler.formatDateFull(incorporationRequest.getRequestDate());
                appServiceRequestSummary.setDateRequested(formattedDate);
                appServiceRequestSummary.setDateRequestedDateObject(incorporationRequest.getRequestDate());
                appServiceRequestSummary.setRequestStatus(incorporationRequest.getStatus());
                appServiceRequestSummaries.add(appServiceRequestSummary);
                
            }         
        }
        
        payload.addAppServiceRequestSummaries(appServiceRequestSummaries);
        
        return payload;
    }
    
    private ServiceRequestSummaryPayload getAppServiceRequestSummaryTax(List<UserTaxService> userTaxServices, ServiceRequestSummaryPayload payload) throws GeneralAppException {
        List<AppServiceRequestSummary> appServiceRequestSummaries = new ArrayList<AppServiceRequestSummary>();
        for (UserTaxService userTaxService : userTaxServices) {
            AppServiceRequestSummary appServiceRequestSummary = new AppServiceRequestSummary();
            Service service = serviceManager.getService(userTaxService.getServiceId()); 
            LegalEntity entity = legalEntityManager.getLegalEntity(userTaxService.getBusinessId());
            
            if (service != null) {
                appServiceRequestSummary.setId(userTaxService.getId());
                appServiceRequestSummary.setUsername(userTaxService.getUsername());                
                appServiceRequestSummary.setClientName(entity.getName());
                appServiceRequestSummary.setRequestedServiceId(userTaxService.getServiceId());
                appServiceRequestSummary.setRequestedServiceName(service.getDescription());
                String formattedDate = DateHandler.formatDateFull(userTaxService.getDateRequested());
                appServiceRequestSummary.setDateRequested(formattedDate);
                appServiceRequestSummary.setDateRequestedDateObject(userTaxService.getDateRequested());
                appServiceRequestSummary.setRequestStatus(userTaxService.getStatus());
                appServiceRequestSummaries.add(appServiceRequestSummary);
                
            }         
        }
        
        payload.addAppServiceRequestSummaries(appServiceRequestSummaries);               
        
        return payload;
    }

    @Override
    public ServiceRequestSummaryPayload getServiceRequestSummaries(String pageNumber, String pageSize, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(pageNumber, pageSize, rawToken);
        
        Claims claims = verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        User user = userManager.get(username);
        if (user == null) {
            exceptionThrowerManager.throwUserDoesNotExistException(TAX_SERVICE_RESOURCE);
        }
        
        ServiceRequestSummaryPayload payload = new ServiceRequestSummaryPayload();
        //if (user.getRole().equals(RoleType.SUPER_ADMIN.getDescription())) {
            List<UserTaxService> userTaxServices = userTaxServiceDataManager.getAllTaxServicesExcludingCompletedStatus();
            List<IncorporationRequest> incorporationRequests = legalEntityManager.getAllIncorporationRequestsExcludingCompletedStatus();
            payload = getAppServiceRequestSummaryIncorporation(incorporationRequests, payload);
            payload = getAppServiceRequestSummaryTax(userTaxServices, payload);
            
            int totalSize = userTaxServices.size() + incorporationRequests.size();
            
            
            
            payload.setPageNumber(Integer.parseInt(pageNumber));
            payload.setPageSize(Integer.parseInt(pageSize));
            payload.setTotalCount(totalSize);
            
            payload.getAppServiceRequestSummaries().sort(Comparator.comparing(o -> o.getDateRequestedDateObject()));
            

            return payload;
       // }
        
        //return payload;
    }
    
    @Override
    public ServiceRequestSummaryPayload getServiceRequestSummariesForUser(String pageNumber, String pageSize, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyParams(pageNumber, pageSize, rawToken);
        
        Claims claims = verifier.setResourceUrl(TAX_SERVICE_RESOURCE).verifyJwt(rawToken);
        String username = claims.getSubject();
        
        User user = userManager.get(username);
        if (user == null) {
            exceptionThrowerManager.throwUserDoesNotExistException(TAX_SERVICE_RESOURCE);
        }
        
        ServiceRequestSummaryPayload payload = new ServiceRequestSummaryPayload();
        //if (user.getRole().equals(RoleType.SUPER_ADMIN.getDescription())) {
            List<UserTaxService> userTaxServices = userTaxServiceDataManager.getUserTaxServicesByUsername(username);
            List<IncorporationRequest> incorporationRequests = legalEntityManager.getIncorporationRequestsByUsername(username);
            payload = getAppServiceRequestSummaryIncorporation(incorporationRequests, payload);
            payload = getAppServiceRequestSummaryTax(userTaxServices, payload);
            
            int totalSize = userTaxServices.size() + incorporationRequests.size();
            
            payload.getAppServiceRequestSummaries().sort(Comparator.comparing(o -> o.getDateRequestedDateObject()));
            
            payload.setPageNumber(Integer.parseInt(pageNumber));
            payload.setPageSize(Integer.parseInt(pageSize));
            payload.setTotalCount(totalSize);
            

            return payload;
       // }
        
        //return payload;
    }
    
}

