/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppUserTaxService;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.Local;

@Local
public interface AdminManagerLocal {    
    AppBoolean UpdateRequest (String id, String status, String rawToken) throws GeneralAppException; 
    
    AppBoolean ConfirmPayment (String id, String status, String rawToken) throws GeneralAppException; 
    
    List<AppUserTaxService> getUserTaxServices (String rawToken) throws GeneralAppException; 

    AppBoolean assignTaskToUser(String taskId, String username, String rawToken) throws GeneralAppException;
}