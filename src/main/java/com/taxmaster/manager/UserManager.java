/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.UserDataManagerLocal;
import com.taxmaster.model.LegalEntity;
import com.taxmaster.model.Otp;
import com.taxmaster.model.User;
import com.taxmaster.model.UserLegalEntity;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppLegalEntity;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.pojo.UserPayload;
import com.taxmaster.util.CodeGenerator;
import com.taxmaster.util.DateHandler;
import com.taxmaster.util.JWT;
import com.taxmaster.util.MD5;
import com.taxmaster.util.MessageType;
import com.taxmaster.util.OnboardingStatusType;
import com.taxmaster.util.SocialPlatformType;
import com.taxmaster.util.Verifier;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.taxmaster.util.exception.GeneralAppException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author Lateefah
 */
@Stateless
public class UserManager implements UserManagerLocal {

    @EJB
    private UserDataManagerLocal userDataManager;

    @EJB
    private EmailManagerLocal emailManager;
    
    @EJB
    private UserLegalEntityManagerLocal userLegalEntityManager;
    
    @EJB
    private LegalEntityManagerLocal legalEntityManager;

    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private CodeGenerator codeGenerator;

    @EJB
    private OtpManagerLocal otpManager;

    @EJB
    private Verifier verifier;

    private final String USER_LINK = "/user";
    private final String AUTHENTICATE_USER_LINK = "/user/authenticate";
    private final String VERIFY_USERNAME_LINK = "/user/verify";
    private final String CREATE_USERNAME_LINK = "/user/username";
    private final String LINK_SOCIAL_ACCOUNT = "/user/link-social";
    private final String UNLINK_SOCIAL_ACCOUNT = "/user/unlink-social";
    private final String NOTIFY_OFFLINE_USER_LINK = "/message/notify";
    private final String DEVICE_TOKEN_LINK = "/user/device-token";
    private final String LOGIN_USER_LINK = "/login";
    private final long TOKEN_LIFETIME = 31556952000l;
    private final String DEFAULT_USER_AVATAR = "https://res.cloudinary.com/pentor/image/upload/v1535145904/zakatify_square_h4x55c.jpg";
    private final String THIS_CLASS = UserManager.class.getName();

    @Override
    public AppUser register(String email, String fullName, String phoneNumber, String birthdate) throws GeneralAppException {

        verifier.setResourceUrl(USER_LINK)
                .verifyParams(email, fullName, phoneNumber);

        if (!isValidEmailAddress(email)) {
            exceptionManager.throwInvalidEmailFormatException(USER_LINK);
        }

        if (userDataManager.get(email) != null) {
            exceptionManager.throwUserAlreadyExistException(USER_LINK);
        }

        if (!userDataManager.getByEmail(email).isEmpty()) {
            exceptionManager.throwEmailAlreadyExistException(email);
        }

        User user = new User();

        user.setUsername(email);
        user.setEmail(email);
        user.setCreationDate(Calendar.getInstance().getTime());
        Date birthday = DateHandler.getDateDayMonthYear(birthdate);
        user.setBirthday(birthday);
        String otp = codeGenerator.getCode();
        user.setPassword(MD5.hash(otp));

        String userHash = codeGenerator.getToken().substring(0, 10);
        user.setHash(userHash);

        fullName = fullName.trim();
        if (fullName.contains(" ")) {
            String firstName = fullName.split(" ")[0];
            String lastName = fullName.split(" ")[1];
            user.setFirstName(firstName);
            user.setLastName(lastName);
        } else {
            user.setFirstName(fullName);
        }

        user.setRole("USER");
        
        user = userDataManager.create(user);
        AppUser appUser = getAppUser(user);
        appUser = getAppUserWithToken(appUser);

        Otp userOtp = new Otp();

        userOtp.setUsername(user.getUsername());
        userOtp.setOtp(otp);
        userOtp.setDateCreated(new Date());

        emailManager.sendVerificationEmail(user, userOtp, MessageType.WELCOME_EMAIL);

        return appUser;
    }

    @Override
    public AppUser updateUserDetails(String username, String email, String firstName,
            String lastName, String phoneNumber, String photoUrl, String location,
            String facebookEmail, String twitterEmail, String rawToken) throws GeneralAppException {

        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username, rawToken);

        verifier.setResourceUrl(USER_LINK)
                .verifyJwt(rawToken);

        User user = userDataManager.get(username);

        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }

        if (email != null && !email.isEmpty()) {
            user.setEmail(email);
        }
        if (firstName != null && !firstName.isEmpty()) {
            user.setFirstName(firstName);
        }
        if (lastName != null && !lastName.isEmpty()) {
            user.setLastName(lastName);
        }
        if (location != null && !location.isEmpty()) {
            user.setLocation(location);
        }
        if (phoneNumber != null) {
            user.setPhoneNumber(phoneNumber);
        }
        if (photoUrl != null && !photoUrl.isEmpty()) {
            user.setPhotoUrl(photoUrl);
        } else {
            user.setPhotoUrl(DEFAULT_USER_AVATAR);
        }

        user.setFacebookEmail(facebookEmail);
        user.setTwitterEmail(twitterEmail);
        String userHash = codeGenerator.getToken().substring(0, 10);
        user.setHash(userHash);
        if (user.getFacebookEmail() != null && !user.getFacebookEmail().isEmpty()) {
            user.setVerified(1);
        }
        if (user.getTwitterEmail() != null && !user.getTwitterEmail().isEmpty()) {
            user.setVerified(1);
        }

        return getAppUser(userDataManager.update(user));
    }

    @Override
    public AppBoolean deleteUser(String username) throws GeneralAppException {

        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username);

        User user = userDataManager.get(username);
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }

        //catch database errors here
        userDataManager.delete(user);

        return getAppBoolean(true);
    }

    @Override
    public AppUser getUserDetails(String username, String rawToken) throws GeneralAppException {

        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username, rawToken);

        verifier.setResourceUrl(USER_LINK)
                .verifyJwt(rawToken);

        AppUser appUser = null;

        try {
            User user = userDataManager.get(username);
            if (user != null) {
                appUser = getAppUser(user);
            }
        } catch (Exception e) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }

        return appUser;
    }

    @Override
    public AppUser authenticate(String id, String password, String type) throws GeneralAppException {

        AppUser appUser = null;
        if (type.equals(SocialPlatformType.EMAIL.getDescription())) {
            verifier.setResourceUrl(AUTHENTICATE_USER_LINK)
                    .verifyParams(id, password, type);
        } else {
            verifier.setResourceUrl(AUTHENTICATE_USER_LINK)
                    .verifyParams(id, type);
        }

        if (isValidEmailAddress(id)) {
            if (type.equals(SocialPlatformType.FACEBOOK.getDescription())) {
                List<User> facebookUser = userDataManager.getByFacebookEmail(id);

                if (facebookUser.isEmpty()) {
                    facebookUser = userDataManager.getByEmail(id);
                    if (facebookUser.isEmpty()) {
                        User user = new User();
                        user.setUsername("autogen_" + codeGenerator.getToken());
                        user.setFacebookEmail(id);
                        user.setCreationDate(Calendar.getInstance().getTime());
                        String userHash = codeGenerator.getToken().substring(0, 10);
                        user.setHash(userHash);
                        user.setVerified(1);
                        userDataManager.create(user);
                        facebookUser.add(user);
                    } else {
                        User existingUser = facebookUser.get(0);
                        existingUser.setFacebookEmail(id);
                        existingUser = userDataManager.update(existingUser);
                        appUser = getAppUserWithToken(getAppUser(existingUser));
                        appUser = getOnboardingStatus(appUser, SocialPlatformType.FACEBOOK);
                    }
                } else {
                    appUser = getAppUserWithToken(getAppUser(facebookUser.get(0)));
                    appUser = getOnboardingStatus(appUser, SocialPlatformType.FACEBOOK);
                }

                appUser = getAppUserWithToken(getAppUser(facebookUser.get(0)));
                appUser = getOnboardingStatus(appUser, SocialPlatformType.FACEBOOK);
            } else if (type.equals(SocialPlatformType.TWITTER.getDescription())) {
                List<User> twitterUser = userDataManager.getByTwitterEmail(id);

                if (twitterUser.isEmpty()) {
                    twitterUser = userDataManager.getByEmail(id);
                    if (twitterUser.isEmpty()) {
                        User user = new User();
                        user.setUsername("autogen_" + codeGenerator.getToken());
                        user.setTwitterEmail(id);
                        user.setCreationDate(Calendar.getInstance().getTime());
                        String userHash = codeGenerator.getToken().substring(0, 10);
                        user.setHash(userHash);
                        user.setVerified(1);
                        userDataManager.create(user);
                        twitterUser.add(user);
                    } else {
                        User existingUser = twitterUser.get(0);
                        existingUser.setTwitterEmail(id);
                        existingUser = userDataManager.update(existingUser);
                        appUser = getAppUserWithToken(getAppUser(existingUser));
                        appUser = getOnboardingStatus(appUser, SocialPlatformType.TWITTER);
                    }
                } else {
                    appUser = getAppUserWithToken(getAppUser(twitterUser.get(0)));
                    appUser = getOnboardingStatus(appUser, SocialPlatformType.TWITTER);
                }

                appUser = getAppUserWithToken(getAppUser(twitterUser.get(0)));
                appUser = getOnboardingStatus(appUser, SocialPlatformType.TWITTER);
            } else if (type.equals(SocialPlatformType.EMAIL.getDescription())) {

                User emailUser = userDataManager.getByEmail(id).isEmpty() ? null : userDataManager.getByEmail(id).get(0);
                if (emailUser == null) {
                    exceptionManager.throwUserDoesNotExistException(AUTHENTICATE_USER_LINK);
                } else {
                    if (!(emailUser.getPassword().equals(MD5.hash(password)))) {
                        exceptionManager.throwInvalidLoginCredentialsException(AUTHENTICATE_USER_LINK);
                    }
                    appUser = getAppUserWithToken(getAppUser(emailUser));
                    appUser = getOnboardingStatus(appUser, SocialPlatformType.EMAIL);
                }
            }
        } else {
            User usernameUser = userDataManager.get(id);

            if (usernameUser == null) {
                exceptionManager.throwUserDoesNotExistException(AUTHENTICATE_USER_LINK);
            }

            if (usernameUser.getPassword() == null || usernameUser.getPassword().isEmpty()) {
                exceptionManager.throwNoEmailLoginAllowedException(AUTHENTICATE_USER_LINK);
            }

            if (!(usernameUser != null && usernameUser.getPassword().equals(MD5.hash(password)))) {
                exceptionManager.throwInvalidLoginCredentialsException(AUTHENTICATE_USER_LINK);
            }
            appUser = getAppUserWithToken(getAppUser(usernameUser));
            appUser = getOnboardingStatus(appUser, SocialPlatformType.EMAIL);
        }

        return appUser;
    }

    @Override
    public AppBoolean verifyUsername(String username) throws GeneralAppException {
        verifier.setResourceUrl(VERIFY_USERNAME_LINK)
                .verifyParams(username);

        User user = userDataManager.get(username);
        if (user == null) {
            return getAppBoolean(true);
        }
        return getAppBoolean(false);
    }

    @Override
    public AppUser createUsername(String username, String email, String tempKey) throws GeneralAppException {

        verifier.setResourceUrl(CREATE_USERNAME_LINK)
                .verifyParams(username, email, tempKey);

        if (!isValidEmailAddress(email)) {
            exceptionManager.throwInvalidEmailFormatException(CREATE_USERNAME_LINK);
        }

        if (userDataManager.get(username) != null) {
            exceptionManager.throwUserAlreadyExistException(CREATE_USERNAME_LINK);
        }

        User existingUser = userDataManager.get(tempKey);

        if (existingUser == null) {
            exceptionManager.throwUserDoesNotExistException(CREATE_USERNAME_LINK);
        }

        User user = new User();

        user.setFacebookEmail(existingUser.getFacebookEmail());
        user.setTwitterEmail(existingUser.getTwitterEmail());
        user.setCreationDate(existingUser.getCreationDate());
        String userHash = codeGenerator.getToken().substring(0, 10);
        user.setHash(existingUser.getHash());
        user.setVerified(existingUser.getVerified());

        username = username.replace("@", "");
        username = username.replace("[^A-Za-z0-9]", "");
        user.setUsername(username);

        System.out.println("EU H: " + existingUser.getHash() + " EU V: " + existingUser.getVerified());
        System.out.println("U H: " + user.getHash() + " U V: " + user.getVerified());

        user = userDataManager.create(user);
        AppUser appUser = getAppUser(user);
        appUser = getAppUserWithToken(appUser);

        userDataManager.delete(existingUser);

        return appUser;

    }

    @Override
    public AppUser linkSocialAcount(String username, String platform, String email, String rawToken) throws GeneralAppException {

        User user = null;

        verifier.setResourceUrl(LINK_SOCIAL_ACCOUNT)
                .verifyParams(username, platform, email, rawToken);

        verifier.setResourceUrl(LINK_SOCIAL_ACCOUNT)
                .verifyJwt(rawToken);

        user = userDataManager.get(username);

        if (user != null) {
            if (platform.equals(SocialPlatformType.FACEBOOK.getDescription())) {
                user.setFacebookEmail(email);
            } else if (platform.equals(SocialPlatformType.TWITTER.getDescription())) {
                user.setTwitterEmail(email);
            } else {
                exceptionManager.throwInvalidSocialPlatform(LINK_SOCIAL_ACCOUNT);
            }

        } else {
            exceptionManager.throwUserDoesNotExistException(LINK_SOCIAL_ACCOUNT);
        }
        return getAppUser(userDataManager.update(user));

    }

    @Override
    public AppUser unlinkSocialAcount(String username, String platform, String rawToken) throws GeneralAppException {

        User user = null;

        verifier.setResourceUrl(UNLINK_SOCIAL_ACCOUNT)
                .verifyParams(username, platform, rawToken);

        verifier.setResourceUrl(UNLINK_SOCIAL_ACCOUNT)
                .verifyJwt(rawToken);

        user = userDataManager.get(username);

        if (user != null) {
            if (platform.equals(SocialPlatformType.FACEBOOK.getDescription())) {
                user.setFacebookEmail(null);
            } else if (platform.equals(SocialPlatformType.TWITTER.getDescription())) {
                user.setTwitterEmail(null);
            } else {
                exceptionManager.throwInvalidSocialPlatform(UNLINK_SOCIAL_ACCOUNT);
            }

        } else {
            exceptionManager.throwUserDoesNotExistException(UNLINK_SOCIAL_ACCOUNT);
        }
        return getAppUser(userDataManager.update(user));

    }

    @Override
    public List<AppUser> getAppUsers(List<User> users) {
        List<AppUser> appUsers = new ArrayList<AppUser>();
        for (User user : users) {
            AppUser appUser = getAppUser(user);
            appUsers.add(appUser);
        }

        return appUsers;
    }

    private AppUser getAppUser(User user) {
        AppUser appUser = new AppUser();

        //set a temp key if username auto generated.        
        appUser.setTempKey(user.getUsername().startsWith("autogen_") ? user.getUsername() : null);

        //if username autogenerated do not set as username
        String username = user.getUsername().startsWith("autogen_") ? null : user.getUsername();
        appUser.setUsername(username);
        appUser.setEmail(user.getEmail() == null ? "" : user.getEmail());
        appUser.setFirstName(user.getFirstName() == null ? "" : user.getFirstName());
        appUser.setLastName(user.getLastName() == null ? "" : user.getLastName());
        appUser.setPhoneNumber(user.getPhoneNumber() == null ? "" : user.getPhoneNumber());
        appUser.setCreationDate(user.getCreationDate());
        appUser.setTwitterEmail(user.getTwitterEmail() == null ? "" : user.getTwitterEmail());
        appUser.setFacebookEmail(user.getFacebookEmail() == null ? "" : user.getFacebookEmail());
        appUser.setLocation(user.getLocation() == null ? "" : user.getLocation());
        appUser.setPhotoUrl(user.getPhotoUrl() == null ? "" : user.getPhotoUrl());
        appUser.setVerified(user.getValidated() == null ? 0 : user.getValidated()); // this is a hack, mobile apps are already integrated to use the "verified" field to show the green tick, but the actual field that controls the display of the green tick is "validated". Permanent fix is to return the "validated" field to the app and make the apps use "validated" to determine the green tick.
        appUser.setHash(user.getHash() == null ? "" : user.getHash());
        appUser.setRole(user.getRole());
        appUser.setBirthday(user.getBirthday());
        appUser.setBusinesses(getUserBusinesses(username));

        return appUser;
    }
    
    private String getUserBusinessIds(String username) {
        StringBuilder userBusinessIds = new StringBuilder();
        List<UserLegalEntity> userLegalEntities = userLegalEntityManager.getLegalEntitiesByUsername(username);        
        int count = 0;
        for (UserLegalEntity userLegalEntity : userLegalEntities) {
            count++;
            userBusinessIds.append(userLegalEntity.getLegalEntityId());
            if (count != userLegalEntities.size()) {
                userBusinessIds.append(",");
            }
        }
        return userBusinessIds.toString();
    }
    
    private List<AppLegalEntity> getUserBusinesses(String username) {
        List<AppLegalEntity> legalEntities = new ArrayList<AppLegalEntity>();
        if (username != null) {
            List<UserLegalEntity> userLegalEntities = userLegalEntityManager.getLegalEntitiesByUsername(username);
            try {
                for (UserLegalEntity userLegalEntity : userLegalEntities) {
                    LegalEntity legalEntity = legalEntityManager.getLegalEntity(userLegalEntity.getLegalEntityId());
                    AppLegalEntity appLegalEntity = legalEntityManager.getAppLegalEntity(legalEntity);
                    legalEntities.add(appLegalEntity);
                }
            } catch (GeneralAppException gae) {
                Logger.getLogger(THIS_CLASS).info(gae.getDeveloperMessage());
            }
        }
        return legalEntities;
    }

    private AppUser getAppUserWithToken(AppUser appUser) {
        JWT token = new JWT();
        appUser.setAuthToken(token.createJWT(appUser, TOKEN_LIFETIME));
        return appUser;
    }

    private AppBoolean getAppBoolean(Boolean status) {
        AppBoolean appBoolean = new AppBoolean();

        appBoolean.setStatus(status);

        return appBoolean;
    }

    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    private AppUser getOnboardingStatus(AppUser appUser, SocialPlatformType socialPlatform) {
        User user = null;
        appUser.setOnboardingStatus(OnboardingStatusType.STATUS_UNKNOWN.getDescription());
        if (socialPlatform.getDescription().equals(SocialPlatformType.FACEBOOK.getDescription())) {
            if (appUser.getFacebookEmail() != null && !appUser.getFacebookEmail().equals("")) {
                user = userDataManager.getByFacebookEmail(appUser.getFacebookEmail()).get(0);
            }
        } else if (socialPlatform.getDescription().equals(SocialPlatformType.TWITTER.getDescription())) {
            if (appUser.getTwitterEmail() != null && !appUser.getTwitterEmail().equals("")) {
                user = userDataManager.getByTwitterEmail(appUser.getFacebookEmail()).get(0);
            }
        } else if (socialPlatform.getDescription().equals(SocialPlatformType.EMAIL.getDescription())) {
            if (appUser.getEmail() != null && !appUser.getEmail().equals("")) {
                user = userDataManager.getByEmail(appUser.getEmail()).get(0);
            } else if (appUser.getUsername() != null && !appUser.getUsername().equals("")) {
                user = userDataManager.get(appUser.getUsername());
            }
        }

        //check username
        if (user != null && user.getUsername().startsWith("autogen_")) {
            appUser.setOnboardingStatus(OnboardingStatusType.CREATE_USERNAME.getDescription());
            return appUser;
        }
        //check profile
        if (user.getFirstName() == null || user.getFirstName().equals("")) {
            appUser.setOnboardingStatus(OnboardingStatusType.UPDATE_PROFILE.getDescription());
        }
        return appUser;
    }

    @Override
    public UserPayload getListOfUsers(String usernames, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(USER_LINK).verifyParams(usernames, rawToken);
        verifier.setResourceUrl(USER_LINK).verifyJwt(rawToken);

        List<AppUser> usersDetails = new ArrayList<AppUser>();
        System.out.println("USERNAMES: " + usernames);
        String[] listOfUsernames = usernames.split("\\s*,\\s*");
        for (int i = 0; i < listOfUsernames.length; i++) {
            String username = listOfUsernames[i];
            if (username != null && !username.isEmpty()) {
                AppUser appUser = getUserDetails(username, rawToken);
                if (appUser != null) {
                    System.out.println("ADDING USERNAME: " + appUser.getUsername());
                    usersDetails.add(appUser);
                }
            }
        }

        UserPayload users = new UserPayload();
        users.setPageNumber(1);
        users.setPageSize(listOfUsernames.length);
        users.setUsers(usersDetails);

        return users;
    }

    @Override
    public void notifyOfflineUser(String offlineUsername, String onlineUsername,
            String connectionId, String rawToken, String notificationPlatform) throws GeneralAppException {
        verifier.setResourceUrl(NOTIFY_OFFLINE_USER_LINK)
                .verifyParams(offlineUsername, onlineUsername, connectionId, rawToken);
        verifier.verifyJwt(rawToken);

        User onlineUser = userDataManager.get(onlineUsername);
        if (onlineUser == null) {
            exceptionManager.throwUserDoesNotExistException(DEVICE_TOKEN_LINK);
        }

    }

    @Override
    public UserPayload searchUsers(String searchTerm, String pageNumber, String pageSize, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(USER_LINK).verifyParams(searchTerm, rawToken);
        verifier.setResourceUrl(USER_LINK).verifyJwt(rawToken);

        List<User> users = userDataManager.getByUsernameFirstNameAndLastName(searchTerm);

        List<AppUser> appUsers = getAppUsers(users);

        UserPayload usersPayload = new UserPayload();
        usersPayload.setUsers(appUsers);
        usersPayload.setPageNumber(Integer.parseInt(pageNumber));
        usersPayload.setPageSize(Integer.parseInt(pageSize));
        usersPayload.setTotalCount(appUsers.size());

        return usersPayload;
    }

    @Override
    public AppUser LogUserInFromWelcomeEmail(String userHash) throws GeneralAppException {
        verifier.setResourceUrl(LOGIN_USER_LINK).verifyParams(userHash);

        User user = userDataManager.getByUserHash(userHash).get(0);
        AppUser appUser = new AppUser();

        if (user != null) {
            appUser = getAppUserWithToken(appUser);
        } else {
            exceptionManager.throwUserDoesNotExistException(LOGIN_USER_LINK);
            return appUser;
        }

        return appUser;
    }

    @Override
    public List<User> searchUsers(String searchTerm) throws GeneralAppException {
        return userDataManager.getByUsernameFirstNameLastNamePhoneNumber(searchTerm);
    }

    @Override
    public void resetOtp(String emailAddress) {
        User user = userDataManager.getByEmail(emailAddress).get(0);
        
        if (user != null) {
            String newOtp = codeGenerator.getCode();
            user.setPassword(MD5.hash(newOtp));
            userDataManager.update(user);
            
            emailManager.sendNewOtpEmail(user, newOtp, MessageType.NEW_OTP);            
        }
        
    }

    @Override
    public AppBoolean contactUs(String name, String email, String subject, String message) {
        emailManager.contactUs(name, email, subject, message);
        Logger.getLogger("RANDOM").fine(name + " " + email + " " + subject + " " + message);
        AppBoolean appBoolean = new AppBoolean();
        appBoolean.setStatus(Boolean.TRUE);
        
        return appBoolean;
    }

}
