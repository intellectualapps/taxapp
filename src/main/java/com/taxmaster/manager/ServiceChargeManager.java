/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.ServiceChargeDataManagerLocal;
import com.taxmaster.model.ServiceCharge;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class ServiceChargeManager implements ServiceChargeManagerLocal {

    @EJB
    private ServiceChargeDataManagerLocal serviceChargeDataManager;    

    @Override
    public ServiceCharge createServiceCharge(ServiceCharge serviceCharge) throws EJBTransactionRolledbackException {
        return serviceChargeDataManager.create(serviceCharge);
    }

    @Override
    public ServiceCharge getServiceCharge(String id) {
        return serviceChargeDataManager.get(id);
    }

    @Override
    public ServiceCharge update(ServiceCharge serviceCharge) {
        return serviceChargeDataManager.update(serviceCharge);
    }
     
}

