/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.data.manager.OtpDataManagerLocal;
import com.taxmaster.model.Otp;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class OtpManager implements OtpManagerLocal {

    @EJB
    private OtpDataManagerLocal otpDataManager;    

    @Override
    public Otp create(Otp otp) throws EJBTransactionRolledbackException {
        return otpDataManager.create(otp);
    }
   
    @Override
    public Otp get(String username) {
        return otpDataManager.get(username);
    }

    @Override
    public void delete(Otp otp) {
        otpDataManager.delete(otp);
    }
     
}

