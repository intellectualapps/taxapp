/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.taxmaster.data.manager.MessageDataManagerLocal;
import com.taxmaster.model.Message;

@Stateless
public class MessageManager implements MessageManagerLocal {

    @EJB
    MessageDataManagerLocal messageDatamanager;
    
    @Override
    public Message getMessage(String messageId) {
        return messageDatamanager.get(messageId);
    }
    
}
