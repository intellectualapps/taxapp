/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import com.taxmaster.model.User;
import com.taxmaster.pojo.AppBoolean;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.pojo.UserPayload;
import com.taxmaster.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.Local;

@Local
public interface UserManagerLocal {
 
    
    AppUser getUserDetails (String username, String rawToken) throws GeneralAppException; 
    
    AppUser register(String id, String fullName, String phoneNumber, String birthday) throws GeneralAppException;
    
    AppUser updateUserDetails(String username, String email, String firstName, String lastName,
                                String phoneNumber, String photoUrl, String location, String facebookEmail,
                                String twitterEmail, String rawToken) throws GeneralAppException;
    
    AppBoolean deleteUser(String username) throws GeneralAppException;
    
    AppUser authenticate (String id, String password, String type) throws GeneralAppException;
    
    AppBoolean verifyUsername(String username) throws GeneralAppException;
    
    List<AppUser> getAppUsers(List<User> users);
            
    AppUser createUsername(String username, String email, String tempKey) throws GeneralAppException;
    
    AppUser linkSocialAcount(String username, String platform, String email, String rawToken) throws GeneralAppException;
    
    AppUser unlinkSocialAcount(String username, String platform, String rawToken) throws GeneralAppException;
    
    UserPayload getListOfUsers (String usernames, String rawToken) throws GeneralAppException;
        
    void notifyOfflineUser(String offlineUsername, String onlineUsername, String connectionId, 
            String rawToken, String notificationPlatform) throws GeneralAppException;

    UserPayload searchUsers(String searchTerm, String pageNumber, String pageSize, String rawToken) throws GeneralAppException;
    
    List<User> searchUsers(String searchTerm) throws GeneralAppException;

    AppUser LogUserInFromWelcomeEmail(String userHash) throws GeneralAppException;
    
    void resetOtp(String emailAddress);

    AppBoolean contactUs(String name, String email, String subject, String message);
}