/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.manager;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import com.taxmaster.util.exception.GeneralAppException;

/**
 *
 * @author buls
 */

@Stateless
public class ExceptionThrowerManager implements ExceptionThrowerManagerLocal {
    
    public final String INVALID_EMAIL_FORMAT_ERROR = "Invalid email";
    public final String INVALID_EMAIL_FORMAT_ERROR_DETAILS = "Invalid email format"; 
    
    public final String USERNAME_ALREADY_EXISTS_ERROR = "User already exists";
    public final String USERNAME_ALREADY_EXISTS_ERROR_DETAILS = "A user already exists with the username supplied";
    
    public final String EMAIL_ALREADY_EXISTS_ERROR = "Email already exists";
    public final String EMAIL_ALREADY_EXISTS_ERROR_DETAILS = "A user with that email already exists";    
    
    private final String USER_NOT_FOUND_ERROR = "User not found";
    private final String USER_NOT_FOUND_ERROR_DETAILS = "The user with the supplied details id does not exist";
    
    private final String LEGAL_ENTITY_NOT_FOUND_ERROR = "Business not found";
    private final String LEGAL_ENTITY_NOT_FOUND_ERROR_DETAILS = "The business/legal entity with the supplied id does not exist";
    
    private final String DISCOUNT_COUPON_NOT_FOUND_ERROR = "Discount coupon not found";
    private final String DISCOUNT_COUPON_NOT_FOUND_ERROR_DETAILS = "The Discount coupon with the supplied id does not exist";
    
    private final String INCOMPLETE_DATA_ERROR = "Incomplete data";
    private final String INCOMPLETE_DATA_ERROR_DETAILS = "Incomplete data has been provided";
    
    private final String INVALID_LOGIN_CREDENTIALS_ERROR = "Invalid login credentials";
    private final String INVALID_LOGIN_CREDENTIALS_ERROR_DETAILS = "Invalid Username/Password combination"; 

    private final String INVALID_LOGIN_TYPE_ERROR = "Invalid login type";
    private final String INVALID_LOGIN_TYPE_ERROR_DETAILS = "invalid login type supplied"; 

    private final String INVALID_EMAIL_ERROR = "Email not found";
    private final String INVALID_EMAIL_ERROR_DETAILS = "Email does not exist in database"; 
    
    private final String INVALID_SOCIAL_PLATFORM_ERROR = "Invalid social platform";
    private final String INVALID_SOCIAL_PLATFORM_ERROR_DETAILS = "Invalid social platform specified";
    
    private final String INVALID_TOKEN_ERROR = "Invalid or expired token supplied";
    private final String INVALID_TOKEN_ERROR_DETAILS = "The auth token supplied is invalid or expired";
    
    private final String NO_EMAIL_LOGIN_ALLOWED_ERROR = "No email account found. Please try social login";
    private final String NO_EMAIL_LOGIN_ALLOWED_ERROR_DETAILS = "User created account via social media. Cannot login with email.";
    
    private final String USER_TAX_SERVICE_NOT_FOUND_ERROR = "Tax service not found";
    private final String USER_TAX_SERVICE_NOT_FOUND_ERROR_DETAILS = "The tax service with the supplied details id does not exist";
    
    private final String INVALID_INTEGER_ERROR = "Invalid number supplied";
    private final String INVALID_INTEGER_ERROR_DETAILS = "Cannot convert parameter from into an Integer";
    
    @Override
    public void throwNullUserAttributesException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INCOMPLETE_DATA_ERROR, INCOMPLETE_DATA_ERROR_DETAILS, link);
    }
    
    @Override
    public void throwUserAlreadyExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, USERNAME_ALREADY_EXISTS_ERROR, USERNAME_ALREADY_EXISTS_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwEmailAlreadyExistException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, EMAIL_ALREADY_EXISTS_ERROR, EMAIL_ALREADY_EXISTS_ERROR_DETAILS,
                link);
    } 
    
    @Override
    public void throwInvalidEmailFormatException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_EMAIL_FORMAT_ERROR, INVALID_EMAIL_FORMAT_ERROR_DETAILS,
                link);
    }        
    
    @Override
    public void throwUserDoesNotExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, USER_NOT_FOUND_ERROR, USER_NOT_FOUND_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwLegaEntityDoesNotExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, LEGAL_ENTITY_NOT_FOUND_ERROR, LEGAL_ENTITY_NOT_FOUND_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwDiscountCouponDoesNotExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, DISCOUNT_COUPON_NOT_FOUND_ERROR, DISCOUNT_COUPON_NOT_FOUND_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidLoginCredentialsException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_LOGIN_CREDENTIALS_ERROR, INVALID_LOGIN_CREDENTIALS_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidLoginType(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_LOGIN_TYPE_ERROR, INVALID_LOGIN_TYPE_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidEmail(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_EMAIL_ERROR, INVALID_EMAIL_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidSocialPlatform(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_SOCIAL_PLATFORM_ERROR, INVALID_SOCIAL_PLATFORM_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwInvalidTokenException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_TOKEN_ERROR, INVALID_TOKEN_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwNoEmailLoginAllowedException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, NO_EMAIL_LOGIN_ALLOWED_ERROR, NO_EMAIL_LOGIN_ALLOWED_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwUserTaxServiceNotFoundException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, USER_TAX_SERVICE_NOT_FOUND_ERROR, USER_TAX_SERVICE_NOT_FOUND_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwInvaidIntegerException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_INTEGER_ERROR, INVALID_INTEGER_ERROR_DETAILS,
                link);
    }
    
}
