/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.util;

/**
 *
 * @author RAYNOLD
 */
public enum ServiceType {
    COMPANY_INCORPORATION("ci"), COMPANY_INCOME_TAX("cit"),
    PERSONAL_INCOME_TAX("pit"), PERSONAL_INCOME_TAX_PAYE("paye"),
    OBTAIN_TIN("tin"), VALUED_ADDED_TAX("vat");
    
    String description;

    private ServiceType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
