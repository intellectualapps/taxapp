/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.util;

/**
 *
 * @author buls
 */
public enum LegalEntityCategory {
    BUSINESS_NAME("bsn"), LIMITED_LIABILITY("ltd"),
    COMPANY_LIMITED_BY_GUARANTEE("ngo"), INDIVIDUAL("ind");
    
    String description;

    private LegalEntityCategory(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
