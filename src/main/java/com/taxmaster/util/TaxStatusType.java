/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.util;

/**
 *
 * @author buls
 */

public enum TaxStatusType {
    NEVER("NEVER"), OTHER("OTHER"),
    TAX_MASTER("TAX_MASTER"), NA("NA");
    
    String description;

    private TaxStatusType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
