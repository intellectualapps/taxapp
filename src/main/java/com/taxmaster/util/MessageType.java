/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.util;

/**
 *
 * @author buls
 */
public enum MessageType {

    WELCOME_EMAIL("WELCOME_EMAIL"), TAX_COMPLETE("TAX_COMPLETE"),
    PAYMENT_CONFIRMATION("PAYMENT_CONFIRMATION"), 
    TASK_ASSIGNMENT_CONFIRMATION("TASK_ASSIGNMENT_CONFIRMATION"),
    NEW_OTP("NEW_OTP");
    
    String description;

    MessageType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

    
}
