/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taxmaster.util;

import com.taxmaster.manager.ExceptionThrowerManagerLocal;
import com.taxmaster.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class Verifier {

    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
       
    private String resourceUrl;   
    
    public Verifier setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
        return this;
    }
    
    public Verifier verifyParams(String... params) throws GeneralAppException {
        for (String param : params) {
            if (param == null || param.isEmpty()) {
                throwNullUserAttributeException(resourceUrl);
            }
        }
        return this;
    }        
    
    public Claims verifyJwt(String rawToken) 
            throws GeneralAppException {
        try {
            String authToken = rawToken.substring(7);
            JWT  token = new JWT();  
            return token.parseJWT(authToken);
        }  catch (Exception e) {
            exceptionManager.throwInvalidTokenException(resourceUrl);
        }
        return null;
    }
    
    private void throwNullUserAttributeException(String link) 
            throws GeneralAppException {
        exceptionManager.throwNullUserAttributesException(link);
    }
    
    
    
}
