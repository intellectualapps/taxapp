package com.taxapp.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/cit-pit-revenue-range")
public class AppCitPitRevenueRange extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {                
        request.getRequestDispatcher("WEB-INF/app/cit-pit-revenue-range.html").forward(request, response);        
    }

    @Override
    public String getServletInfo() {
        return "Servlet to manage cit-pit-revenue-range.html";
    }

}