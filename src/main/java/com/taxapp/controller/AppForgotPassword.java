package com.taxapp.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/forgot-password")
public class AppForgotPassword extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {                
        request.getRequestDispatcher("WEB-INF/app/forgot-password.html").forward(request, response);        
    }

    @Override
    public String getServletInfo() {
        return "Servlet to manage app forgot-password.html";
    }

}