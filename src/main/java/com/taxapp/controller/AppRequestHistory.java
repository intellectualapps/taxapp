package com.taxapp.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/request-history")
public class AppRequestHistory extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {                
        request.getRequestDispatcher("WEB-INF/app/request-history.html").forward(request, response);        
    }

    @Override
    public String getServletInfo() {
        return "Servlet to manage the app request-history.html";
    }

}