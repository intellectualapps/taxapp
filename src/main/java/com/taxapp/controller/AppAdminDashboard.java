package com.taxapp.controller;

import com.taxmaster.manager.UserManager;
import com.taxmaster.manager.UserManagerLocal;
import com.taxmaster.pojo.AppUser;
import com.taxmaster.util.RoleType;
import com.taxmaster.util.Verifier;
import com.taxmaster.util.exception.GeneralAppException;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/admin")
public class AppAdminDashboard extends HttpServlet {
    @EJB
    private Verifier verifier;
    
    @EJB
    private UserManagerLocal userManager;    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
        String rawToken = request.getParameter("t");
        try {
            rawToken = "Bearer " + rawToken;
            verifier.setResourceUrl("/admin").verifyParams(rawToken);
            String username = verifier.setResourceUrl("/admin").verifyJwt(rawToken).getSubject();
            AppUser user = userManager.getUserDetails(username, rawToken);
            if (user.getRole().equals(RoleType.SUPER_ADMIN.getDescription())) {
                request.getRequestDispatcher("WEB-INF/app/admin/super-admin-dashboard.html").forward(request, response);
            } else if (user.getRole().equals(RoleType.ADMIN.getDescription())) {
                request.getRequestDispatcher("WEB-INF/app/admin/super-admin-dashboard.html").forward(request, response);
            } else {
                
            }
        } catch (GeneralAppException gae) {
            System.out.println("/ADMIN ISSUE: " + gae.getDeveloperMessage());
            request.getRequestDispatcher("login").forward(request, response);
        }
                
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
        String rawToken = request.getParameter("t");
        try {
            verifier.setResourceUrl("/admin").verifyParams(rawToken);
            String username = verifier.setResourceUrl("/admin").verifyJwt(rawToken).getSubject();
            AppUser user = userManager.getUserDetails(username, rawToken);
            if (user.getRole().equals(RoleType.SUPER_ADMIN.getDescription())) {
                request.getRequestDispatcher("WEB-INF/app/admin/super-admin-dashboard.html").forward(request, response);
            } else if (user.getRole().equals(RoleType.ADMIN.getDescription())) {
                request.getRequestDispatcher("WEB-INF/app/admin/admin-dashboard.html").forward(request, response);
            } else {
                
            }
        } catch (GeneralAppException gae) {
            request.getRequestDispatcher("login").forward(request, response);
        }
                
    }

    @Override
    public String getServletInfo() {
        return "Servlet to manage the app admin-dashboard.html";
    }

}