package com.taxapp.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/bus-account-creation-confirmation")
public class AppAccountCreationConfirmation extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {                
        request.getRequestDispatcher("WEB-INF/app/bus-account-creation-confirmation.html").forward(request, response);        
    }

    @Override
    public String getServletInfo() {
        return "Servlet to manage the app bus-account-creation-confirmation.html";
    }

}