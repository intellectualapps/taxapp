package com.taxapp.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns = {"/user-history/details", "/details"})
public class AppAdminDashboardDetails extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {   
        String requestPath = request.getRequestURI();
        //if (requestPath.contains("user-history")) {
        String id = request.getParameter("id");
            System.out.println("user-history call " + id);
            //request.getRequestDispatcher("WEB-INF/app/admin/admin-dashboard-details.html").forward(request, response);    
          //  request.getRequestDispatcher("WEB-INF/app/admin/user-dashboard-details.html").forward(request, response);
        //} else {
          //  System.out.println("show dashboard details page call");
            request.getRequestDispatcher("WEB-INF/app/admin/admin-dashboard-details.html").forward(request, response);        
        //}
        
    }

    @Override
    public String getServletInfo() {
        return "Servlet to manage the app login.html";
    }

}